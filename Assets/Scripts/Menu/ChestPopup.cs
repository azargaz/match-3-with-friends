﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestPopup : MonoBehaviour
{
    [SerializeField]
    Button[] chestButtons;
    [SerializeField]
    Text header;
    [SerializeField]
    Button closeButton;

    [HideInInspector]
    public int lastButtonIndex = 0;

	void Start ()
    {
        closeButton.onClick.AddListener(delegate { ClosePopup(gameObject); });
        for (int i = 0; i < chestButtons.Length; i++)
        {
            int j = i;
            chestButtons[i].onClick.AddListener(delegate { CloseChestButton(chestButtons[j].gameObject); });
        }
	}
    	
	void ClosePopup(GameObject target)
    {
        Destroy(target);
    }

    void CloseChestButton(GameObject target)
    {
        Destroy(target);
    }

    public void SetHeaderText(string txt)
    {
        header.text = txt;
    }

    public void EnableChestButtons(int amount)
    {
        for (int i = 0; i < chestButtons.Length; i++)
        {
            if(i < amount)
            {
                chestButtons[i].gameObject.SetActive(true);
            }
        }
    }

    public Button GetNextButton()
    {
        if (lastButtonIndex >= chestButtons.Length)
            return null;
        return chestButtons[lastButtonIndex++];
    }
}
