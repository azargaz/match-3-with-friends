﻿using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class DataLoader : MonoBehaviour
{
    [Header("Data paths")]
    public string cuisineDataPath;
    public string materialDataPath;
    public string opponentDataPath;
    public string spellDataPath;
    public string[] levelDataPaths;

    [Serializable]
    public struct cuisineDataArray
    {
        public cuisineData[] cuisineData;
    }
    [Serializable]
    public struct materialDataArray
    {
        public materialData[] foodMaterials;
    }
    [Serializable]
    public struct opponentDataArray
    {
        public opponentData[] opponentData;
    }
    [Serializable]
    public struct spellDataArray
    {
        public spellData[] spellData;
    }

    [Header("Loaded Data")]
    public cuisineData[] cuisines;
    public materialData[] materials;
    public opponentData[] opponents;
    public spellData[] spells;
    public levelData[] levels;

    [HideInInspector]
    public opponentData chosenOpponent;
    [HideInInspector]
    public levelData chosenLevel;
    [HideInInspector]
    public List<materialData> chosenMaterials = new List<materialData>();
    public bool AddChosenMaterial(Storage.StorageMaterial storageMaterial)
    {
        if (storageMaterial.itemName == "none")
            return true;

        int sameMaterialUsed = 0;
        for (int i = 0; i < chosenMaterials.Count; i++)
        {
            if(chosenMaterials[i].name == storageMaterial.data.name)
            {
                sameMaterialUsed++;
            }

            if(sameMaterialUsed >= storageMaterial.quantity)
            {
                return false;
            }
        }

        chosenMaterials.Add(storageMaterial.data);
        return true;
    }

    [HideInInspector]
    public rewardData rewardsForBattle;
    [HideInInspector]
    public bool rewardsProcessed = true;
    public void SetRewards(int score)
    {
        float scoreToTargetRatio = score / chosenLevel.targetScore;
        int successLevel = 0;
        if (scoreToTargetRatio >= 1.5f)
        {
            if (chosenMaterials.Count >= 3)
                successLevel = 3;
            else
                successLevel = 2;
        }
        else if (scoreToTargetRatio >= 1f)
            successLevel = 1;

        // 3 STARS
        if(successLevel > 0)
        {
            Debug.Log(successLevel + " Stars");
            ProcessRewards(chosenLevel.rewardData.rewardList, chosenMaterials.Count > 0 ? chosenMaterials[0] : null, successLevel);
        }
        // FAIL
        else
        {
            Debug.Log("Fail");
        }
    }
    void ProcessRewards(rewardData.reward[] rewards, materialData chosenMaterial, int stars)
    {
        #region Rewards
        if(stars >= 3)
        {
            int randomCrystalsQuantity = UnityEngine.Random.Range(1, 3);
            PlayerStats.instance.AddCrystals(randomCrystalsQuantity);
        }

        for (int i = 0; i < rewards.Length; i++)
        {
            float r = UnityEngine.Random.Range(0f, 1f);
            if(r <= rewards[i].foodChance)
            {
                int randomQuantity = 0;
                if(stars >= 2)
                    randomQuantity = UnityEngine.Random.Range(0, rewards[i].quantityRandom);
                if (stars >= 3)
                    randomQuantity *= 3;             

                materialData randomFood = materials[rewards[i].foodAN];
                Storage.instance.CreateAndAddStorageItem(randomFood.name, rewards[i].quantityBase + randomQuantity, null, null, randomFood);
            }
        }

        #endregion

        #region Chosen material cuisine
        if(chosenMaterial != null)
        {
            int sumOfChances = 0;
            for (int i = 0; i < chosenMaterial.cuisineList.Length; i++)
            {
                sumOfChances += chosenMaterial.cuisineList[i].cuisineChance;
            }
            int currentChances = 0;
            int random = UnityEngine.Random.Range(0, sumOfChances);
            int index = 0;
            for (int i = 0; i < chosenMaterial.cuisineList.Length; i++)
            {
                if (random >= currentChances && random < chosenMaterial.cuisineList[i].cuisineChance + currentChances)
                {
                    index = chosenMaterial.cuisineList[i].cuisineAN;
                    break;
                }
                currentChances += chosenMaterial.cuisineList[i].cuisineChance;
            }

            cuisineData chosenCuisine = cuisines[index];
            Storage.instance.CreateAndAddStorageItem(chosenCuisine.name, 1, null, null, chosenCuisine);
        }
        #endregion
    }

    [HideInInspector]
    public bool dataLoaded = false;
    public static DataLoader instance;

    void Awake()
    {
        if (dataLoaded)
            instance = this;
    }

    public void LoadData()
    {
        instance = this;

        if(!string.IsNullOrEmpty(cuisineDataPath))
        {
            cuisines = LoadDataFromJson<cuisineDataArray>(cuisineDataPath).cuisineData;
        }
        if(!string.IsNullOrEmpty(materialDataPath))
        {
            materials = LoadDataFromJson<materialDataArray>(materialDataPath).foodMaterials;
        }
        if(!string.IsNullOrEmpty(opponentDataPath))
        {
            opponents = LoadDataFromJson<opponentDataArray>(opponentDataPath).opponentData;
        }
        if(!string.IsNullOrEmpty(spellDataPath))
        {
            spells = LoadDataFromJson<spellDataArray>(spellDataPath).spellData;
        }

        levels = new levelData[levelDataPaths.Length];
        for (int i = 0; i < levelDataPaths.Length; i++)
        {
            if(!string.IsNullOrEmpty(levelDataPaths[i]))
            {
                levels[i] = LoadDataFromJson<levelData>(levelDataPaths[i]);
            }
        }

        dataLoaded = true;
    }

    public void ClearData()
    {
        cuisines = null;
        materials = null;
        opponents = null;
        spells = null;
        levels = null;
        instance = null;
        dataLoaded = false;
    }

    T LoadDataFromJson<T>(string dataPath)
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, dataPath);
        string dataAsJson = File.ReadAllText(filePath);
        T data = JsonUtility.FromJson<T>(dataAsJson);
        return data;
    }
}

#region Data classes

[Serializable]
public class spellData
{
    public string name;
    public int arrayNum;
    public string flavor;
    public int energyRequire;
    [Serializable]
    public class effect
    {
        public string target;
        public string action;
        public string type;
        public string _object;
        public int number;
        public string color;
        public int layer;
    }
    public effect[] effects;
}

[Serializable]
public class opponentData
{
    public string name;
    public int arrayNum;
    public bool competitive;
    public int scoreRate;
    public int energyRate;
    public float raiseRate;
    public int spellCost;

    [Serializable]
    public class spell
    {
        public string name;
        public int spellAN;
        public int spellChance;
    }
    public spell[] spellList;
}

[Serializable]
public class levelData
{
    public string opponentName;
    public int opponentAN;
    public bool competitive;
    public int targetScore;
    public string flavorRequired;
    public int randomEventInterval;

    [Serializable]
    public class spell
    {
        public string spellName;
        public int spellAN;
        public int spellChance;
    }
    public spell[] randomEventList;
    public rewardData rewardData;
    public levelMapData levelMapData;
}

[Serializable]
public class rewardData
{
    [Serializable]
    public class reward
    {
        public string foodName;
        public int foodAN;
        public int foodChance;
        public int quantityBase;
        public int quantityRandom;
    }
    public reward[] rewardList;
}

[Serializable]
public class levelMapData
{
    [Serializable]
    public class element
    {
        public string name;
        public string elementType;
        public string boostType;
        [Serializable]
        public class position
        {
            public int xPos;
            public int yPos;
            public string color;
            public int layer;
        }
        public position[] posList;
    }
    [Serializable]
    public class randomElement
    {
        public string name;
        public string elementType;
        public int quantity;
        public string color;
        public string boostType;
        public int layers;
    }
    public element[] positionedElements;
    public randomElement[] randomElements;
}

[Serializable]
public class materialData
{
    public string name;
    public int arrayNum;
    public string flavor;
    public string rarity;
    public int copperPrice;
    public int crystalPrice;
    public string spellName;
    public int spellAN;
    [Serializable]
    public class cuisine
    {
        public string name;
        public int cuisineAN;
        public int cuisineChance;
    }
    public cuisine[] cuisineList;
}

[Serializable]
public class cuisineData
{
    public string name;
    public int arrayNum;
    public string flavor;
    public string rarity;
    [Serializable]
    public class material
    {
        public string materialName;
        public int materialAN;
    }
    public material[] materialList;
    [Serializable]
    public class successData
    {
        public int level;
        public int stamina;
        public string texture;
        public int copperPrice;
        public int crystalPrice;
    }
    public successData[] successList;
}

#endregion
