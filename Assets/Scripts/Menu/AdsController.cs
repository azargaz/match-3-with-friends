﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using Barebones.MasterServer;
using System;

public class AdsController : MonoBehaviour
{
    public int coinsPerAd = 10;
    public int staminaPerAd = 3;

    public string iOSGameId;
    bool adsRemoved = false;
    IAPController iapController;

    void Start()
    {
        iapController = GetComponent<IAPController>();
        Msf.Client.Auth.LoggedIn += OnLogIn;
        
        Advertisement.Initialize(iOSGameId);
    }
    
    void OnLogIn()
    {
        if(!adsRemoved)
        {
            Debug.Log("Show Ad");
            Advertisement.Show();
        }
    }

    public void RemoveAds()
    {
        adsRemoved = true;
    }

    public void ShowRewardedAd()
    {
        var options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show("test", options);
    }

    private void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            // Reward your player here.
            iapController.GrantCoins(coinsPerAd);
            iapController.GrantStamina(staminaPerAd);
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}
