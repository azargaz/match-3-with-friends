﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuisineManager : MonoBehaviour
{
    public materialData mainFoodMaterial;
    public materialData[] supportFoodMaterial;
    public int maxSupportFoodMaterials = 2;
}
