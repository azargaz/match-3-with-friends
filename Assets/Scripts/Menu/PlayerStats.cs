﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Barebones.MasterServer;
using Barebones.Networking;

public class PlayerStats : MonoBehaviour
{
    public int level = 1;
    public int currentExp = 0;
    public int maxExp = 100;

    public int currentStamina = 10;
    public int maxStamina = 100;

    public int currentChests = 0;
    public int maxChests = 2;

    public int crystals = 0;
    public int coins = 0;

    [HideInInspector]
    public bool notEnoughCoins = false;
    [HideInInspector]
    public bool notEnoughCrystals = false;

    public int secondsToSpawnChest = 300;
    float timeLeftToSpawnChest = 0;
    public string chestName = "Bread Chest";
    public string chestTab = "Tab 2";
    [SerializeField]
    GameObject chestPrefab;

    bool statsRestored = false;

    [SerializeField]
    GameObject chestPopup;

    MSFClientController msfClientController;
    public static PlayerStats instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        msfClientController = FindObjectOfType<MSFClientController>();
        RestoreStatsFromProfile();
        Msf.Client.Auth.LoggedIn += OnLogIn;
        msfClientController.LoggingOut += OnLogOut;
        SetupServerLastLoginTimeHandler();

        timeLeftToSpawnChest = secondsToSpawnChest;
    }

    void OnLogIn()
    {
        RestoreStatsFromProfile();
    }

    void OnLogOut()
    {
        SaveStatsToProfile((successful, error) =>
        {
            if (!successful)
            {
                Debug.LogError(error);
                return;
            }
            else
            {
                Debug.Log("Succesfully saved stats to profile.");
                statsRestored = false;
                currentStamina = 0;
                currentChests = 0;
            }
        });
    }

    // Sends message to server to request last login tme
    void GetLastLoginTime()
    {
        if (!Msf.Client.Auth.IsLoggedIn)
            return;
        var msg = MessageHelper.Create((short)MasterServerFunctionsOpCodes.requestLastLoginTime, Msf.Client.Auth.AccountInfo.Username);
        Msf.Connection.Peer.SendMessage(msg);
    }

    // Updates last login time to current time
    void UpdateLastLoginTime()
    {
        if (!Msf.Client.Auth.IsLoggedIn)
            return;
        var msg = MessageHelper.Create((short)MasterServerFunctionsOpCodes.updateLastLoginTime, Msf.Client.Auth.AccountInfo.Username);
        Msf.Connection.Peer.SendMessage(msg);
    }

    void SetupServerLastLoginTimeHandler()
    {
        Msf.Connection.SetHandler((short)MasterServerFunctionsOpCodes.newLoginTime, HandleLastLoginTime);
    }

    // Handle server message that contains last login time and current time
    void HandleLastLoginTime(IIncommingMessage message)
    {
        var data = message.Deserialize(new LastLoginTimePacket());

        if (data.oldLastLoginTime != 0 && data.newLastLoginTime != 0)
        {
            long ticksBetweenLoginTimes = data.newLastLoginTime - data.oldLastLoginTime;
            TimeSpan timeSpanBetweenLogins = new TimeSpan(ticksBetweenLoginTimes);
            Debug.Log("Time between logins: " + timeSpanBetweenLogins);

            int chestsToSpawn = (int)timeSpanBetweenLogins.TotalSeconds / secondsToSpawnChest;
            float secondsLeft = (float)timeSpanBetweenLogins.TotalSeconds - chestsToSpawn * secondsToSpawnChest;
            timeLeftToSpawnChest -= secondsLeft;

            chestsToSpawn = Mathf.Clamp(chestsToSpawn, 0, maxChests);
            SpawnChests(chestsToSpawn);
            while (chestsToSpawn > 0)
            {
                AddChest();
                chestsToSpawn--;
            }

            UpdateLastLoginTime();
        }
        else
        {
            UpdateLastLoginTime();
        }
    }

    private void FixedUpdate()
    {
        if (currentChests >= maxChests)
            timeLeftToSpawnChest = secondsToSpawnChest;
        if (timeLeftToSpawnChest > 0)
            timeLeftToSpawnChest -= Time.deltaTime;
        if (timeLeftToSpawnChest <= 0)
        {
            timeLeftToSpawnChest = secondsToSpawnChest;
            AddChest();
            UpdateLastLoginTime();
        }

        // For Testing //////////////
        if (Input.GetKeyDown(KeyCode.P))
        {
            AddExp(100);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            SaveStatsToProfile();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestoreStatsFromProfile();
        }
        ///////////////////////////
    }

    void AddChest()
    {
        if (currentChests < maxChests)
            currentChests++;

        Storage.instance.CreateAndAddStorageItem(chestName, 1, chestTab, chestPrefab);
    }

    public void AddExp(int amount)
    {
        maxExp = ExperienceToNextLevelFormula(level);
        currentExp += amount;

        while (currentExp >= maxExp)
        {
            currentExp -= maxExp;
            level++;
            maxExp = ExperienceToNextLevelFormula(level);
        }
    }

    public bool AddCoins(int amount)
    {
        if (amount < 0 && coins < Mathf.Abs(amount))
        {
            notEnoughCoins = true;
            return false;
        }

        coins += amount;
        return true;
    }

    public bool AddCrystals(int amount)
    {
        Debug.Log(amount);
        Debug.Log(crystals);

        if (amount < 0 && crystals < Mathf.Abs(amount))
        {
            notEnoughCrystals = true;
            return false;
        }

        crystals += amount;
        return true;
    }

    public bool AddStamina(int amount)
    {
        if (amount < 0 && currentStamina < Mathf.Abs(amount))
        {
            return false;
        }

        currentStamina += amount;
        Mathf.Clamp(currentStamina, 0, maxStamina);
        return true;
    }

    void DefaultSaveStatsToProfileCallback(bool successful, string error)
    {
        if (!successful)
        {
            Debug.LogError(error);
            return;
        }
        else
            Debug.Log("Succesfully saved stats to profile.");
    }

    void SaveStatsToProfile()
    {
        SaveStatsToProfile(DefaultSaveStatsToProfileCallback);
    }

    void SaveStatsToProfile(SuccessCallback callback)
    {
        if (!Msf.Client.Auth.IsLoggedIn)
            return;

        if (level <= 0)
            level = 1;

        var profile = new ObservableServerProfile(Msf.Client.Auth.AccountInfo.Username)
        {
            new ObservableInt(MyProfileKeys.PlayerLevel, 1),
            new ObservableInt(MyProfileKeys.Experience, 0),
            new ObservableInt(MyProfileKeys.Stamina),
            new ObservableInt(MyProfileKeys.Gems),
            new ObservableInt(MyProfileKeys.Coins)
        };

        Msf.Server.Profiles.FillProfileValues(profile, (successful, error) =>
        {
            if (!successful)
            {
                Logs.Error(error);
                callback.Invoke(false, error);
                return;
            }

            profile.GetProperty<ObservableInt>(MyProfileKeys.PlayerLevel).Set(level);
            profile.GetProperty<ObservableInt>(MyProfileKeys.Experience).Set(currentExp);
            profile.GetProperty<ObservableInt>(MyProfileKeys.Stamina).Set(currentStamina);
            profile.GetProperty<ObservableInt>(MyProfileKeys.Gems).Set(crystals);
            profile.GetProperty<ObservableInt>(MyProfileKeys.Coins).Set(coins);

            callback.Invoke(true, "");
        });
    }

    void RestoreStatsFromProfile()
    {
        if (!Msf.Client.Auth.IsLoggedIn || statsRestored)
            return;

        statsRestored = true;

        GetLastLoginTime();

        // Construct the profile
        var profile = new ObservableProfile()
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore),
            new ObservableInt(MyProfileKeys.OverallScore),
            new ObservableInt(MyProfileKeys.PlayerLevel, 1),
            new ObservableInt(MyProfileKeys.Experience),
            new ObservableInt(MyProfileKeys.Stamina),
            new ObservableInt(MyProfileKeys.Gems),
            new ObservableInt(MyProfileKeys.Coins)
        };

        // Get profile from server
        Msf.Client.Profiles.GetProfileValues(profile, (successful, error) =>
        {
            if (!successful)
            {
                Logs.Error(error);
                return;
            }
            else
            {
                var _level = profile.GetProperty<ObservableInt>(MyProfileKeys.PlayerLevel);
                level = _level.Value;

                var _exp = profile.GetProperty<ObservableInt>(MyProfileKeys.Experience);
                currentExp = _exp.Value;

                if (level <= 0)
                    level = 1;

                maxExp = ExperienceToNextLevelFormula(level);

                var _stamina = profile.GetProperty<ObservableInt>(MyProfileKeys.Stamina);
                currentStamina = _stamina.Value;

                var _gems = profile.GetProperty<ObservableInt>(MyProfileKeys.Gems);
                crystals = _gems.Value;

                var _coins = profile.GetProperty<ObservableInt>(MyProfileKeys.Coins);
                coins = _coins.Value;
            }
        });
    }

    // This is formula that is used to calculate experience points needed for next player level
    int ExperienceToNextLevelFormula(int _level)
    {
        if (_level == 0)
        {
            return 1;
        }

        return (int)Mathf.Sqrt(_level * 100) * 10;
    }

    void SpawnChests(int amount)
    {
        if (amount <= 0)
            return;

        GameObject _chestPopup = Instantiate(chestPopup);
        ChestPopup chestPopupScript = _chestPopup.GetComponent<ChestPopup>();
        chestPopupScript.SetHeaderText("You got " + amount + " bread chests!");

        for (int i = 0; i < amount; i++)
        {
            Button openChestButton = chestPopupScript.GetNextButton();
            openChestButton.onClick.AddListener(OpenChest);
        }
        chestPopupScript.EnableChestButtons(amount);
    }

    void OpenChest()
    {
        if (currentChests <= 0)
            return;

        int staminaToRestore = UnityEngine.Random.Range(1, 4);
        if (currentChests == 1 && currentStamina < 10)
            staminaToRestore = UnityEngine.Random.Range(3, 7);
        currentStamina += staminaToRestore;
        currentChests--;
        Storage.instance.RemoveStorageItem(chestName, chestTab, 1);
    }

    private void OnDestroy()
    {
        Msf.Client.Auth.LoggedIn -= OnLogIn;
        if(msfClientController != null)
            msfClientController.LoggingOut -= OnLogOut;
    }
}
