﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : MonoBehaviour
{
    [System.Serializable]
    public class StorageUpdate
    {
        public string itemName;
        public string tabName;
        public int quantity;

        [HideInInspector]
        public GameObject itemGameObject;

        public enum Action { add, remove };
        public Action action;
    }

    [System.Serializable]
    public class StorageItem
    {
        public string itemName;
        public string tabName;
        public int quantity;

        [HideInInspector]
        public GameObject itemGameObject;
    }

    [System.Serializable]
    public class StorageMaterial : StorageItem
    {
        public materialData data;
    }

    [System.Serializable]
    public class StorageCuisine : StorageItem
    {
        public cuisineData data;
    }

    public string materialTab = "Tab 1";
    public string cuisineTab = "Tab 1";

    public List<StorageItem> storageItems;
    public List<StorageUpdate> queuedUpdates;

    [SerializeField]
    GameObject storageItem;

    public static Storage instance;

    void Awake()
    {
        queuedUpdates = new List<StorageUpdate>();
        instance = this;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            for (int i = 0; i < DataLoader.instance.materials.Length; i++)
            {
                materialData mat = DataLoader.instance.materials[i];
                CreateAndAddStorageItem(mat.name, 1, materialTab, storageItem, mat);
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            for (int i = 0; i < DataLoader.instance.materials.Length; i++)
            {
                materialData mat = DataLoader.instance.materials[i];
                RemoveStorageItem(mat.name, materialTab, 1);
            }
        }
    }

    #region Create and add storage item functions
    public void CreateAndAddStorageItem(string name, int quant, string tab, GameObject go)
    {
        StorageItem newItem = new StorageItem()
        {
            itemName = name,
            quantity = quant,
            tabName = string.IsNullOrEmpty(tab) ? "Tab 1" : tab,
            itemGameObject = go
        };
        CreateAndAddStorageItem(newItem);
    }

    public void CreateAndAddStorageItem(string name, int quant, string tab, GameObject go, materialData materialData)
    {
        StorageMaterial newItem = new StorageMaterial()
        {
            itemName = name,
            quantity = quant,
            tabName = string.IsNullOrEmpty(tab) ? materialTab : tab,
            itemGameObject = go,
            data = materialData
        };
        CreateAndAddStorageItem(newItem);
    }

    public void CreateAndAddStorageItem(string name, int quant, string tab, GameObject go, cuisineData cuisineData)
    {
        StorageCuisine newItem = new StorageCuisine()
        {
            itemName = name,
            quantity = quant,
            tabName = string.IsNullOrEmpty(tab) ? cuisineTab : tab,
            itemGameObject = go,
            data = cuisineData
        };
        CreateAndAddStorageItem(newItem);
    }

    void CreateAndAddStorageItem(StorageItem item)
    {
        if (item.itemGameObject == null)
            item.itemGameObject = storageItem;
        StartCoroutine(AddStorageItem(item));
    }
    #endregion

    IEnumerator AddStorageItem(StorageItem item)
    {
        for (int i = 0; i < storageItems.Count; i++)
        {
            if (storageItems[i].itemName == item.itemName)
            {
                storageItems[i].quantity += item.quantity;
                QueueUpdate(item, StorageUpdate.Action.add);
                yield break;
            }
        }

        storageItems.Add(item);
        QueueUpdate(item, StorageUpdate.Action.add);
    }

    public void RemoveStorageItem(string name, string tab, int amount)
    {
        StorageItem item = new StorageItem()
        {
            itemName = name,
            quantity = amount,
            tabName = tab
        };

        StartCoroutine(RemoveStorageItem(item));
    }

    IEnumerator RemoveStorageItem(StorageItem item)
    {
        for (int i = 0; i < storageItems.Count; i++)
        {
            if (storageItems[i].itemName == item.itemName)
            {
                storageItems[i].quantity -= item.quantity;
                if (storageItems[i].quantity <= 0)
                {
                    storageItems.RemoveAt(i);
                }
                QueueUpdate(item, StorageUpdate.Action.remove);
                yield break;
            }
        }
    }

    void QueueUpdate(StorageItem item, StorageUpdate.Action actionToPerform)
    {
        StorageUpdate itemToUpdate = new StorageUpdate()
        {
            itemName = item.itemName,
            quantity = item.quantity,
            tabName = item.tabName,
            action = actionToPerform,
            itemGameObject = item.itemGameObject
        };
        queuedUpdates.Add(itemToUpdate);
    }
}
