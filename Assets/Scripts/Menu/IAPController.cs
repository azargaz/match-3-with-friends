﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPController : MonoBehaviour
{
    AdsController adsController;

    private void Awake()
    {
        adsController = GetComponent<AdsController>();        
    }

    public void GrantCoins(int amount)
    {
        Debug.Log("Granted " + amount + " copper coins.");
        PlayerStats.instance.AddCoins(amount);
    }

    public void GrantStamina(int amount)
    {
        Debug.Log("Granted " + amount + " stamina.");
        PlayerStats.instance.AddStamina(amount);
    }

    public void RemoveAds()
    {
        Debug.Log("Ads removed");
        adsController.RemoveAds();
    }

    public void GrantCoinsForCrystals(string amountAndPrice)
    {
        string[] split = amountAndPrice.Split(':');
        int amount = int.Parse(split[0]);
        int flavorCrystalsPrice = int.Parse(split[1]);     

        if(PlayerStats.instance.AddCrystals(-flavorCrystalsPrice))
        {
            Debug.Log("Granted " + amount + " copper coins for " + flavorCrystalsPrice + " flavor crystals.");
            PlayerStats.instance.AddCoins(amount);
        }
        else
        {
            Debug.Log("Not enough crystals.");
        }
    }
}
