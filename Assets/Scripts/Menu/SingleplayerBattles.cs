﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Barebones.MasterServer;
using System;
using System.Collections.Generic;

public class SingleplayerBattles : MonoBehaviour
{
    public List<Battle> battles;
    [Serializable]
    public class Battle
    {
        public string name;
        public int staminaCost = 1;
        public int coinCost = 0;
        public levelData levelData;
        public Button chooseButton;
    }

    [SerializeField]
    GameObject button;
    [SerializeField]
    Transform buttonsParent;

    Battle chosenBattle;

    [SerializeField]
    Text targetText;

    [SerializeField]
    GameObject chooseFoodMaterialsPopup;

    [SerializeField]
    Button readyButton;
    [SerializeField]
    Button startButton;
    [SerializeField]
    Button closePopupButton;
    [SerializeField]
    SceneField singleplayerScene;

    MSFClientController msfClientController;
    [SerializeField]
    DataLoader dataLoader;

    void Start ()
    {
        msfClientController = FindObjectOfType<MSFClientController>();
        dataLoader = DataLoader.instance;
        
        readyButton.onClick.AddListener(SingleplayerGameReady);
        startButton.onClick.AddListener(StartSingleplayerGame);
        closePopupButton.onClick.AddListener(CloseFoodMaterialPopup);
        ChooseBattle(0);
    }
    
    void ChooseBattle(int index)
    {
        chosenBattle = battles[index];
        targetText.text = "Target score:\n" + chosenBattle.levelData.targetScore;
    }

    void SingleplayerGameReady()
    {        
        chooseFoodMaterialsPopup.SetActive(true);
    }

    void CloseFoodMaterialPopup()
    {
        chooseFoodMaterialsPopup.SetActive(false);
    }

    public void StartSingleplayerGame()
    {
        if(PlayerStats.instance.currentStamina >= chosenBattle.staminaCost && PlayerStats.instance.AddCoins(-chosenBattle.coinCost))
        {
            PlayerStats.instance.AddStamina(-chosenBattle.staminaCost);
            dataLoader.chosenLevel = chosenBattle.levelData;
            dataLoader.chosenOpponent = dataLoader.opponents[dataLoader.chosenLevel.opponentAN];
            msfClientController.SingleplayerGame();
            SceneManager.LoadScene(singleplayerScene);
        }
        else
        {
            Debug.LogWarning("Not enough stamina/coins to start battle!");
            CloseFoodMaterialPopup();
        }
    }

    public void SetupBattles()
    {
        battles.Clear();
        foreach (Transform child in buttonsParent)
        {
            DestroyImmediate(child.gameObject);
        }

        for (int i = 0; i < dataLoader.levels.Length; i++)
        {
            GameObject buttonGO = Instantiate(button, buttonsParent);

            levelData level = dataLoader.levels[i];
            string battleName = dataLoader.levelDataPaths[i].Substring(0, dataLoader.levelDataPaths[i].Length - 5);
            Battle battle = new Battle()
            {
                name = battleName,
                chooseButton = buttonGO.GetComponent<Button>(),
                levelData = level
            };
            battles.Add(battle);
        }

        for (int i = 0; i < battles.Count; i++)
        {
            int j = i;
            battles[i].chooseButton.onClick.AddListener(delegate { ChooseBattle(j); });
            battles[i].chooseButton.GetComponentInChildren<Text>().text = battles[i].name;
        }

        ChooseBattle(0);
    }
}
