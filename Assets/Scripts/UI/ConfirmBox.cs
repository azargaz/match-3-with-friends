﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ConfirmBox : MonoBehaviour
{
    [SerializeField]
    Button confirm;
    [SerializeField]
    Button cancel;
    [SerializeField]
    Text headerText;

    void Awake()
    {
        SetOnCancel();
    }

    public void SetOnConfirm(UnityAction onConfirm)
    {
        confirm.onClick.AddListener(onConfirm);
        confirm.onClick.AddListener(delegate { gameObject.SetActive(false); });
    }

    public void ClearOnConfirm()
    {
        confirm.onClick.RemoveAllListeners();
    }

    public void SetHeaderText(string txt)
    {
        headerText.text = txt;
    }

    void SetOnCancel()
    {
        cancel.onClick.AddListener(delegate { gameObject.SetActive(false); });
    }
}
