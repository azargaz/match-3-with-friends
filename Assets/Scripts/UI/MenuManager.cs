﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using UnityEngine.SceneManagement;
using Barebones.MasterServer;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    Button searchGameButton;
    [SerializeField]
    Button cancelSearchGameButton;

    MSFClientController msfClientController;

    public static MenuManager instance;

    public GameObject mainMenu;
    public GameObject allAuth;
    public GameObject startGameButtons;
    public GameObject cancelSearchGame;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        msfClientController = FindObjectOfType<MSFClientController>();

        AddListenerToButton(msfClientController.SearchForGame, searchGameButton);
        AddListenerToButton(OnSearchGame, searchGameButton);
        AddListenerToButton(msfClientController.CancelSearchForGame, cancelSearchGameButton);
        AddListenerToButton(OnCancelSearchGame, cancelSearchGameButton);

        Msf.Client.Auth.LoggedIn += OnLoggedIn;
        msfClientController.LoggingOut += OnLogOut;
        msfClientController.BetweenGames += BetweenGames;
        Msf.Client.Connection.Connected += OnConnected;
        Msf.Client.Connection.Disconnected += OnDisconnected;

        if (Msf.Client.Auth.IsLoggedIn)
            mainMenu.SetActive(true);
        else if (Msf.Client.Connection.IsConnected)
            allAuth.SetActive(true);

        OnDisconnected();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (Msf.Client.Auth.IsLoggedIn && Msf.Client.Connection.IsConnected)
            {
                msfClientController.LogOut();
            }
        }
    }

    void OnConnected()
    {
        allAuth.SetActive(true);
        mainMenu.SetActive(false);
    }

    void OnDisconnected()
    {
        allAuth.SetActive(false);
        mainMenu.SetActive(false);
    }

    void OnLoggedIn()
    {
        allAuth.SetActive(false);
        mainMenu.SetActive(true);
    }

    void BetweenGames()
    {
        allAuth.SetActive(false);
        mainMenu.SetActive(true);
        OnCancelSearchGame();
    }

    void OnLogOut()
    {
        allAuth.SetActive(false);
        mainMenu.SetActive(false);
        OnCancelSearchGame();
    }

    void OnCancelSearchGame()
    {
        startGameButtons.SetActive(true);
        cancelSearchGame.SetActive(false);
    }

    void OnSearchGame()
    {
        startGameButtons.SetActive(false);
        cancelSearchGame.SetActive(true);
    }
    
    public void AddListenerToButton(UnityAction function, Button button)
    {
        button.onClick.AddListener(function);
    }

    private void OnDestroy()
    {
        Msf.Client.Auth.LoggedIn -= OnLoggedIn;
        Msf.Client.Auth.LoggedOut -= OnLogOut;
        Msf.Client.Connection.Connected -= OnConnected;
        Msf.Client.Connection.Disconnected -= OnDisconnected;
        if (msfClientController != null)
        { 
            msfClientController.LoggingOut -= OnLogOut;
            msfClientController.BetweenGames -= BetweenGames;
        }
    }
}
