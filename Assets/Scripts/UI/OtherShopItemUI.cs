﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OtherShopItemUI : MonoBehaviour
{
    [SerializeField]
    Text titleText;
    [SerializeField]
    Text priceText;
    [SerializeField]
    Button button;

    public void SetButtonOnClick(UnityAction onClick)
    {
        button.onClick.AddListener(onClick);
    }

    public void SetTitleText(string txt)
    {
        titleText.text = txt;
    }

    public void SetPriceText(string txt)
    {
        priceText.text = txt;
    }
}
