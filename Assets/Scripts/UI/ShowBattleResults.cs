﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Barebones.MasterServer;

public class ShowBattleResults : MonoBehaviour
{
    [SerializeField]
    Text battleResultsText;
    [SerializeField]
    Button showBattleResults;
    [SerializeField]
    GameObject results;

    private void Start()
    {
        showBattleResults.onClick.AddListener(ShowProfileData);

        results.SetActive(false);

        Msf.Client.Auth.LoggedIn += UpdateProfile;
    }

    void UpdateProfile()
    {
        if (!Msf.Client.Auth.IsLoggedIn)
            return;

        // Construct the profile
        var profile = new ObservableProfile()
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore, 0),
            new ObservableInt(MyProfileKeys.OverallScore, 0)
        };
        
        // Get profile from server
        Msf.Client.Profiles.GetProfileValues(profile, (successful, error) =>
        {
            if (!successful)
            {
                Logs.Error(error);
                return;
            }  
            else
            {
                string title = "Battle results:\n";
                string battleResults = "";

                var dictionary = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.PiecesRemoved);
                foreach (KeyValuePair<string, int> item in dictionary.Pairs)
                {
                    battleResults += item.Key + " pieces removed: " + item.Value + "\n";
                }

                dictionary = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.BoostsUsed);
                foreach (KeyValuePair<string, int> item in dictionary.Pairs)
                {
                    battleResults += item.Key + " boosts used: " + item.Value + "\n";
                }

                dictionary = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.SpellsCast);
                foreach (KeyValuePair<string, int> item in dictionary.Pairs)
                {
                    battleResults += item.Key + " spells cast: " + item.Value + "\n";
                }

                var highscore = profile.GetProperty<ObservableInt>(MyProfileKeys.Highscore);
                if (highscore.Value != 0)
                    battleResults += "Highscore: " + highscore.Value + "\n";

                var overallScore = profile.GetProperty<ObservableInt>(MyProfileKeys.OverallScore);
                if (overallScore.Value != 0)
                    battleResults += "Overall score: " + overallScore.Value + "\n";

                if (battleResults == "")
                {
                    if (!Msf.Client.Auth.AccountInfo.IsGuest)
                        title = "You need to play at least one game to have any data recorded.";
                    else
                        title = "Data is not saved permanently on Guest account. All data will be wiped after restarting application.";
                }

                battleResultsText.text = title + battleResults;
            }
        });
    }

    void ShowProfileData()
    {
        if(results.activeInHierarchy)
        {
            results.SetActive(false);
            return;
        }

        if (!Msf.Client.Auth.IsLoggedIn)
            return;

        UpdateProfile();
        results.SetActive(true);
    }
}
