﻿using UnityEngine;
using UnityEngine.UI;

public class BottomButtonsUI : MonoBehaviour
{
    MSFClientController msfClientController;

    [System.Serializable]
    public class UIScreen
    {
        public string name;
        public Button openButton;
        public Button closeButton;
        public GameObject screen;

        public void Open()
        {
            screen.SetActive(true);
        }

        public void Close()
        {
            screen.SetActive(false);
        }
        
        public void Initialize()
        {
            if (openButton != null && screen != null)
                openButton.onClick.AddListener(Open);
            else
                Debug.LogError("Assign variables openButton and screen in inspector.");

            if (closeButton != null && screen != null)
                closeButton.onClick.AddListener(Close);
            else
                Debug.LogError("Assign variables closeButton and screen in inspector.");
        }
    }

    public UIScreen[] UIScreens;

    private void Start()
    {
        for (int i = 0; i < UIScreens.Length; i++)
        {
            UIScreens[i].Initialize();
            UIScreens[i].screen.SetActive(false);
        }

        msfClientController = FindObjectOfType<MSFClientController>();
        msfClientController.LoggingOut += OnLogOut;
    }

    void OnLogOut()
    {
        for (int i = 0; i < UIScreens.Length; i++)
        {
            UIScreens[i].Close();
        }
    }

    private void OnDestroy()
    {
        if(msfClientController != null)
            msfClientController.LoggingOut -= OnLogOut;
    }
}
