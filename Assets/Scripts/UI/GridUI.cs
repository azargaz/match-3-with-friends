﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridUI : MonoBehaviour
{
    [SerializeField]
    Button leftArrow;
    [SerializeField]
    Button rightArrow;

    public ItemTab[] itemTabs;
    public int currentTab = 0;

    [System.Serializable]
    public class ItemTab
    {
        public string name;
        public Button tabButton;
        public ItemGrid itemGrid;

        public void OpenItemGrid()
        {
            itemGrid.itemGridGO.SetActive(true);
        }

        public void CloseItemGrid()
        {
            itemGrid.itemGridGO.SetActive(false);
        }
    }

    [System.Serializable]
    public class ItemGrid
    {
        public List<ItemGridElement> itemGridElements;
        public int elementsPerPage = 20;
        public List<ItemGridPage> itemGridPages;

        int currentPage = 0;

        //[HideInInspector]
        public GameObject itemGridGO;

        public void Initialize()
        {
            for (int i = 0; i < itemGridPages.Count; i++)
            {
                if (i != currentPage)
                    itemGridPages[i].ClosePage();
            }
        }

        public void NextPage()
        {
            if (currentPage >= itemGridPages.Count - 1)
                return;

            itemGridPages[currentPage].ClosePage();
            currentPage++;
            itemGridPages[currentPage].OpenPage();
        }

        public void PreviousPage()
        {
            if (currentPage <= 0)
                return;

            itemGridPages[currentPage].ClosePage();
            currentPage--;
            itemGridPages[currentPage].OpenPage();
        }
    }
    
    //[System.Serializable]
    public class ItemGridPage
    {
        public GameObject itemGridPageGO;

        public ItemGridElement[] itemGridElements;

        public void OpenPage()
        {
            itemGridPageGO.SetActive(true);
        }

        public void ClosePage()
        {
            itemGridPageGO.SetActive(false);
        }
    }

    [System.Serializable]
    public class ItemGridElement
    {
        public string name;
        public int quantity;
        public Storage.StorageItem storageItem;

        //[HideInInspector]
        public GameObject itemGridElementGO;
    }

    [Header("Prefabs")]
    public GameObject itemTabPrefab;
    public GameObject itemGridPrefab;
    public GameObject itemPagePrefab;
    public GameObject itemGridElementPrefab;

    public List<Storage.StorageItem> storageItems;

    bool updatingGrid = false;

    void Awake()
    {
        CreateGrids();

        leftArrow.onClick.AddListener(OnClickLeftArrow);
        rightArrow.onClick.AddListener(OnClickRightArrow);
        storageItems = new List<Storage.StorageItem>();
    }

    void FixedUpdate()
    {
        if(Storage.instance.queuedUpdates.Count > 0 && !updatingGrid)
        {
            StartCoroutine(ProcessUpdate(0));
        }
    }

    IEnumerator ProcessUpdate(int index)
    {
        updatingGrid = true;
        Storage.StorageUpdate update = Storage.instance.queuedUpdates[index];
        UpdateItem(update);
        Storage.instance.queuedUpdates.RemoveAt(index);
        yield return new WaitForFixedUpdate();
        updatingGrid = false;
    }

    void CreateGrids()
    {
        for (int i = 0; i < itemTabs.Length; i++)
        {
            GameObject _itemTab = Instantiate(itemTabPrefab, transform);
            int j = i;
            itemTabs[i].tabButton.onClick.AddListener(delegate
            {
                SwitchTab(j);
            });

            ItemGrid itemGrid = itemTabs[i].itemGrid;
            itemTabs[i].itemGrid = itemGrid;
            GameObject _itemGrid = Instantiate(itemGridPrefab, _itemTab.transform);
            itemGrid.itemGridGO = _itemGrid;
            itemGrid.itemGridPages = new List<ItemGridPage>();
            int elements = itemGrid.itemGridElements.Count;
            int elementsInPage = 0;

            ItemGridPage currentPage = null;
            if (elements == 0)
            {
                elementsInPage = itemGrid.elementsPerPage;

                currentPage = new ItemGridPage();
                GameObject page = Instantiate(itemPagePrefab, _itemGrid.transform);
                currentPage.itemGridPageGO = page;
                currentPage.itemGridElements = new ItemGridElement[elementsInPage];
                itemGrid.itemGridPages.Add(currentPage);
            }

            // While there are elements to put into this tab
            while (elements > 0)
            {
                // If there are still elements to put and this page is full, create new page
                if (elementsInPage == 0)
                {
                    elementsInPage = itemGrid.elementsPerPage;

                    currentPage = new ItemGridPage();
                    GameObject page = Instantiate(itemPagePrefab, _itemGrid.transform);
                    currentPage.itemGridPageGO = page;
                    currentPage.itemGridElements = new ItemGridElement[elementsInPage];
                    itemGrid.itemGridPages.Add(currentPage);
                }

                elementsInPage--;
                elements--;

                GameObject _element = Instantiate(itemGridElementPrefab, currentPage.itemGridPageGO.transform.GetChild(0));
                ItemGridElement element = itemGrid.itemGridElements[elements];
                element.itemGridElementGO = _element;
                currentPage.itemGridElements[elementsInPage] = element;
            }

            itemGrid.Initialize();
        }

        for (int i = 1; i < itemTabs.Length; i++)
        {
            itemTabs[i].CloseItemGrid();
        }
    }

    //void UpdateGrid(List<Storage.StorageItem> oldStorageItems, List<Storage.StorageItem> newStorageItems)
    //{
    //    // Update quantities of items
    //    for (int i = 0; i < itemTabs.Length; i++)
    //    {
    //        for (int j = 0; j < itemTabs[i].itemGrid.itemGridElements.Count; j++)
    //        {
    //            ItemGridElement itemGridElement = itemTabs[i].itemGrid.itemGridElements[j];
    //            StartCoroutine(UpdateItem(itemGridElement.storageItem));
    //        }
    //    }

    //    // Check for new items
    //    for (int i = 0; i < newStorageItems.Count; i++)
    //    {
    //        Storage.StorageItem newItem = newStorageItems[i];
    //        if (newItem != null && !ContainsStorageItem(oldStorageItems, newItem))
    //        {
    //            StartCoroutine(UpdateItem(newItem));
    //        }
    //    }

    //    // Check for removed items
    //    for (int i = 0; i < oldStorageItems.Count; i++)
    //    {
    //        Storage.StorageItem oldItem = oldStorageItems[i];
    //        if (oldItem != null && !ContainsStorageItem(newStorageItems, oldItem))
    //        {
    //            StartCoroutine(UpdateItem(oldItem));
    //        }
    //    }
    //}

    //bool ContainsStorageItem(List<Storage.StorageItem> _storageItems, Storage.StorageItem item)
    //{
    //    for (int i = 0; i < _storageItems.Count; i++)
    //    {
    //        if (_storageItems[i].itemName == item.itemName)
    //            return true;
    //    }
    //    return false;
    //}

    public void OnClickLeftArrow()
    {
        itemTabs[currentTab].itemGrid.PreviousPage();
    }

    public void OnClickRightArrow()
    {
        itemTabs[currentTab].itemGrid.NextPage();
    }

    public void SwitchTab(int newTab)
    {
        itemTabs[currentTab].CloseItemGrid();
        currentTab = newTab;
        itemTabs[currentTab].OpenItemGrid();
    }

    void UpdateItem(Storage.StorageUpdate update)
    {
        Debug.Log(update.itemName + ", " + update.quantity + ", " + update.action);

        // If this item is already in the grid just update quantity
        for (int i = 0; i < itemTabs.Length; i++)
        {
            if (itemTabs[i].name == update.tabName)
            {
                ItemGrid itemGrid = itemTabs[i].itemGrid;

                for (int j = 0; j < itemGrid.itemGridElements.Count; j++)
                {
                    ItemGridElement itemGridElement = itemGrid.itemGridElements[j];
                    if (itemGridElement.storageItem.itemName == update.itemName)
                    {
                        if(update.action == Storage.StorageUpdate.Action.add)
                            itemGridElement.quantity += update.quantity;

                        if (update.action == Storage.StorageUpdate.Action.remove)
                            itemGridElement.quantity -= update.quantity;
                        
                        itemGridElement.storageItem.quantity = itemGridElement.quantity;
                        if (itemGridElement.quantity <= 0)
                        {
                            Debug.Log(itemGridElement.itemGridElementGO);
                            Destroy(itemGridElement.itemGridElementGO);
                            itemGrid.itemGridElements.RemoveAt(j);
                            j--;
                        }
                        return;
                    }
                }
            }
        }

        GameObject _item = null;
        if (update.itemGameObject != null)
            _item = Instantiate(update.itemGameObject);

        // If this item is new in grid, create it
        for (int i = 0; i < itemTabs.Length; i++)
        {
            if (itemTabs[i].name == update.tabName)
            {
                ItemGrid currentGrid = itemTabs[i].itemGrid;
                Storage.StorageItem newStorageItem = new Storage.StorageItem()
                {
                    itemName = update.itemName,
                    quantity = update.quantity,
                    tabName = update.tabName,
                    itemGameObject = update.itemGameObject
                };
                ItemGridElement newItem = new ItemGridElement()
                {
                    name = newStorageItem.itemName,
                    quantity = newStorageItem.quantity,
                    storageItem = newStorageItem,
                    itemGridElementGO = _item
                };
                currentGrid.itemGridElements.Add(newItem);

                ItemGridPage currentPage = currentGrid.itemGridPages[currentGrid.itemGridPages.Count - 1];
                for (int j = 0; j < currentPage.itemGridElements.Length; j++)
                {
                    if (currentPage.itemGridElements[j] == null)
                    {
                        currentPage.itemGridElements[j] = newItem;
                        newItem.itemGridElementGO.transform.SetParent(currentPage.itemGridPageGO.transform.GetChild(0));
                        return;
                    }
                }

                ItemGridPage newPage = new ItemGridPage();
                GameObject page = Instantiate(itemPagePrefab, currentGrid.itemGridGO.transform);
                newPage.itemGridPageGO = page;
                newPage.itemGridElements = new ItemGridElement[currentGrid.elementsPerPage];
                currentGrid.itemGridPages.Add(newPage);
                newPage.itemGridElements[0] = newItem;
                currentGrid.Initialize();
                newItem.itemGridElementGO.transform.SetParent(newPage.itemGridPageGO.transform.GetChild(0));
                return;
            }
        }
    }
}
