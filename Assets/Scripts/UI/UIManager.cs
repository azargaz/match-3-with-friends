﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    Button returnToMainMenuButton;

    [SerializeField]
    GameObject quitGamePrompt;
    [SerializeField]
    Button confirmQuitGameButton;
    [SerializeField]
    Button cancelQuitGameButton;

    [SerializeField]
    Text startGameCounter;

    [SerializeField]
    Text yourScore;
    [SerializeField]
    Text enemyScore;
    [SerializeField]
    Text gameTimeLeft;

    [SerializeField]
    GameObject battleResultsObject;
    [SerializeField]
    Text resultTitleText;
    [SerializeField]
    Text resultScoreText;

    [SerializeField]
    Transform spellsContainer;

    [SerializeField]
    RectTransform energyBar;
    [SerializeField]
    RectTransform enemyEnergyBar;

    [SerializeField]
    RectTransform turnBar;
    [SerializeField]
    Button[] steps;
    [SerializeField]
    ColorBlock highlightedColor;

    PlayerInput playerInput;
    SpellController spellController;

    MSFClientController msfClientController;

    public static UIManager ui;
    bool buttonsSetup = false;

    private void Awake()
    {
        ui = this;
        msfClientController = FindObjectOfType<MSFClientController>();

        if (msfClientController != null)
        {
            AddListenerToButton(msfClientController.EndGame, returnToMainMenuButton);
            AddListenerToButton(msfClientController.EndGame, confirmQuitGameButton);
        }
        else
            Debug.LogError("MSF Client controller not found.");

        AddListenerToButton(ReturnToMainMenu, returnToMainMenuButton);
        AddListenerToButton(ReturnToMainMenu, confirmQuitGameButton);
        AddListenerToButton(CloseQuitPrompt, cancelQuitGameButton);

        for (int i = steps.Length-1; i >= 0; i--)
        {
            steps[i].Select();
            steps[i].colors = highlightedColor;            
        }
    }

    private void FixedUpdate()
    {
        if (BoardManager.instance == null)
            return;

        if (playerInput == null || spellController == null)
        {
            if(BoardManager.instance != null)
            {
                playerInput = BoardManager.instance.GetComponent<PlayerInput>();
                spellController = SpellController.instance;
            }
            return;
        }

        if (!buttonsSetup)
            SetupSpellButtons();

        if (startGameCounter.gameObject.activeInHierarchy)
        {
            int timeLeftToStart = (int)BoardManager.instance.timeLeftToStart;
            if (timeLeftToStart > 0)
                startGameCounter.text = "Game starts in: " + timeLeftToStart;
            else
                startGameCounter.text = "";
        }

        // Display scores, turn and time left in UI
        yourScore.text = "Score: " + DataManager.instance.playerScore;
        if(enemyScore != null)
            enemyScore.text = "Score: " + DataManager.instance.enemyScore;
        int gameTime = (int)BoardManager.instance.gameTimeLeft;
        int gameTimeMinutes = gameTime / 60;
        int gameTimeSeconds = gameTime % 60;
        gameTimeLeft.text = gameTimeMinutes + ":" + (gameTimeSeconds >= 10 ? "" : "0") + gameTimeSeconds;

        //UpdateEnergyBar(energyBar, spellController.maxEnergy, spellController.currentEnergy, spellController.energyColor);
        //if(enemyEnergyBar != null)
        //    UpdateEnergyBar(enemyEnergyBar, spellController.maxEnergy, spellController.enemyCurrentEnergy, spellController.enemyEnergyColor);

        UpdateBar(turnBar, playerInput.playersTurnDuration, playerInput.playersTurnStopTime);

        if((int)MatchController.instance.CurrentStep <= steps.Length)
        {
            steps[(int)MatchController.instance.CurrentStep-1].Select();
            steps[(int)MatchController.instance.CurrentStep-1].colors = highlightedColor;
        }
    }

    void ReturnToMainMenu()
    {
        if(BoardManager.instance.singleplayer)
        {
            SceneManager.LoadScene(0);
        }
    }

    void CloseQuitPrompt()
    {
        quitGamePrompt.SetActive(false);
    }

    void SetupSpellButtons()
    {
        if (spellController == null)
            return;

        int i = 0;
        foreach (RectTransform child in spellsContainer)
        {
            if (i < spellController.standardSpells.Length + spellController.spells.Length)
            {
                Button button = child.GetComponent<Button>();
                int j = i;
                AddListenerToButton(delegate { spellController.CastSpell(j); }, button);
                child.GetComponentInChildren<Text>().text = i >= spellController.standardSpells.Length ? spellController.spells[i - spellController.standardSpells.Length].data.name : spellController.standardSpells[i].data.name;
                i++;
            }
            else
                child.gameObject.SetActive(false);
        }
        buttonsSetup = true;
    }

    public void AddListenerToButton(UnityAction function, Button button)
    {
        if (function != null)
            button.onClick.AddListener(function);
        else
            Debug.LogError("Can't add null listener to button.");
    }

    public void DisplayBattleResults()
    {
        battleResultsObject.SetActive(true);

        resultTitleText.text = (BoardManager.instance.draw ? "Draw" : (BoardManager.instance.isWinner ? "Win" : "Lose"));
        resultScoreText.text = "Score: " + DataManager.instance.playerScore;
    }

    public void UpdateBar(RectTransform bar, float maxValue, float currentValue)
    {
        if(maxValue != 0 && currentValue >= 0)
            bar.localScale = new Vector3(bar.localScale.x, currentValue / maxValue, bar.localScale.z);
    }

    #region For testing purposes

    public void UpdateEnergyBar(RectTransform bar, int maxEnergy, int currentEnergy, Piece.PieceColor energyColor)
    {
        if (maxEnergy != 0)
            bar.localScale = new Vector3((float)currentEnergy / maxEnergy, bar.localScale.y, bar.localScale.z);
        bar.GetComponent<Image>().color = pieceColors[(int)energyColor];
    }

    Color[] pieceColors =
    {
        Color.red,                //red
        Color.red + Color.yellow, //orange
        Color.yellow,             //yellow
        Color.green,              //green
        Color.red + Color.blue,   //purple
        Color.blue                //blue
    };

    #endregion

    private void OnDestroy()
    {
        ui = null;
    }
}
