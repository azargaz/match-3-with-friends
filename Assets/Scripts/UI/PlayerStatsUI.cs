﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUI : MonoBehaviour
{
    [SerializeField]
    PlayerStats stats;

    [SerializeField]
    RectTransform expBar;
    [SerializeField]
    RectTransform staminaBar;

    [SerializeField]
    Text gemsCounter;
    [SerializeField]
    Text coinsCounter;

    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        expBar.localScale = new Vector3((float)stats.currentExp / stats.maxExp, expBar.localScale.y, expBar.localScale.z);
        staminaBar.localScale = new Vector3((float)stats.currentStamina / stats.maxStamina, staminaBar.localScale.y, staminaBar.localScale.z);
        gemsCounter.text = "Crystals:\n" + stats.crystals;
        coinsCounter.text = "Coins:\n" + stats.coins;

        if(stats.notEnoughCoins)
        {
            stats.notEnoughCoins = false;
            anim.SetTrigger("FlashCoins");
        }

        if(stats.notEnoughCrystals)
        {
            stats.notEnoughCrystals = false;
            anim.SetTrigger("FlashCrystals");
        }
    }
}
