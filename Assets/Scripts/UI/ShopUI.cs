﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class ShopUI : MonoBehaviour
{
    public IAPItem[] iAPItems;
    public OtherShopItem[] otherShopItems;

    [System.Serializable]
    public class IAPItem
    {        
        [HideInInspector]
        public string productId;
        public IAPButton.OnPurchaseCompletedEvent onPurchaseComplete;
    }

    [System.Serializable]
    public class OtherShopItem
    {
        public string name;
        public bool confirmBox = true;
        public int quantity;
        public string itemName;
        public int price;
        public string currency;
        public string titleText;
        public string customConfirmationText;
        public UnityEvent onClick;
    }

    [SerializeField]
    GameObject confirmBox;
    [SerializeField]
    ConfirmBox confirmBoxScript;
    public GameObject iAPItemsPrefab;
    public GameObject otherShopItemsPrefab;

    void Awake ()
    {
        for (int i = 0; i < iAPItems.Length; i++)
        {
            GameObject iAPItem = Instantiate(iAPItemsPrefab, transform);
            var iAPButton = iAPItem.GetComponent<IAPButton>();
            iAPButton.productId = iAPItems[i].productId;
            iAPButton.onPurchaseComplete = iAPItems[i].onPurchaseComplete;
            iAPItem.SetActive(true);
        }

        for (int i = 0; i < otherShopItems.Length; i++)
        {
            GameObject otherShopItem = Instantiate(otherShopItemsPrefab, transform);
            var otherShopItemUI = otherShopItem.GetComponent<OtherShopItemUI>();
            UnityEvent onClick = otherShopItems[i].onClick;

            if (!otherShopItems[i].confirmBox)
                otherShopItemUI.SetButtonOnClick(delegate { onClick.Invoke(); });
            else
            {
                int j = i;
                otherShopItemUI.SetButtonOnClick(delegate {
                    ShowConfirmBox(onClick, otherShopItems[j].itemName, otherShopItems[j].quantity.ToString(),
                        otherShopItems[j].price.ToString(), otherShopItems[j].currency, otherShopItems[j].customConfirmationText);
                });
            }

            string title = string.Format(otherShopItems[i].titleText, otherShopItems[i].quantity, otherShopItems[i].price);
            otherShopItemUI.SetTitleText(title);
            if(otherShopItems[i].price == 0)
                otherShopItemUI.SetPriceText("FREE");
            else
                otherShopItemUI.SetPriceText(otherShopItems[i].price + " " + otherShopItems[i].currency);
            otherShopItem.SetActive(true);
        }
	}

    void ShowConfirmBox(UnityEvent onConfirm, string item, string quantity, string price, string currency, string customText)
    {
        confirmBox.SetActive(true);
        string confirmText = "";
        if(string.IsNullOrEmpty(customText))
        {
            confirmText = "Do you want to buy " + quantity + " of " + item + " for " + price + " " + currency + "?";
        }
        else
        {
            confirmText = customText;
        }

        confirmBoxScript.SetHeaderText(confirmText);
        confirmBoxScript.ClearOnConfirm();
        confirmBoxScript.SetOnConfirm(delegate { onConfirm.Invoke(); });
    }
}
