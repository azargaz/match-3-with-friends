﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitPrompt : MonoBehaviour
{
    [SerializeField]
    GameObject quitGamePrompt;

    [SerializeField]
    float timeToOpenQuitPrompt = 3f;
    float clickTimeOfOpenQuitPrompt = 0f;
    bool quitButtonClicked = false;

    public void OnPointerDown()
    {
        quitButtonClicked = true;
    }

    public void OnPointerUp()
    {
        quitButtonClicked = false;
        clickTimeOfOpenQuitPrompt = 0;
    }

    void Update ()
    {
        if (quitButtonClicked && clickTimeOfOpenQuitPrompt < timeToOpenQuitPrompt)
            clickTimeOfOpenQuitPrompt += Time.deltaTime;

        if(clickTimeOfOpenQuitPrompt >= timeToOpenQuitPrompt)
        {
            quitButtonClicked = false;
            quitGamePrompt.SetActive(true);
            clickTimeOfOpenQuitPrompt = 0;
        }
	}
}
