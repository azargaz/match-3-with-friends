﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class FoodMaterialUI : MonoBehaviour
{
    Dropdown dropdown;

    List<Storage.StorageMaterial> materials = new List<Storage.StorageMaterial>();
    int chosenValue = 0;

    private void OnEnable()
    {
        if(dropdown == null)
        {
            dropdown = GetComponent<Dropdown>();
            dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        UpdateDropdownList();
        OnValueChanged(0);
    }

    void UpdateDropdownList()
    {
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();

        materials.Clear();

        Storage.StorageMaterial nullMaterial = new Storage.StorageMaterial() {
            itemName = "none",
            quantity = 999,
            data = null
        };
        materials.Add(nullMaterial);
        Dropdown.OptionData nullMaterialOption = new Dropdown.OptionData()
        {
            text = nullMaterial.itemName
        };
        options.Add(nullMaterialOption);

        for (int i = 0; i < Storage.instance.storageItems.Count; i++)
        {
            Storage.StorageMaterial material = (Storage.StorageMaterial)Storage.instance.storageItems[i];
            if (material.data != null)
            {
                Dropdown.OptionData optionData = new Dropdown.OptionData() {
                    text = material.itemName + " (x" + material.quantity + ")"
                };
                options.Add(optionData);
                materials.Add(material);
            }
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(options);
    }

    void OnValueChanged(int i)
    {
        DataLoader.instance.chosenMaterials.Remove(materials[chosenValue].data);
        if(DataLoader.instance.AddChosenMaterial(materials[i]))
            chosenValue = i;
        else
        {
            if(materials.Count > 0 && materials[chosenValue].itemName != "none")
                DataLoader.instance.AddChosenMaterial(materials[chosenValue]);
            dropdown.value = chosenValue;
        }
    }

}
