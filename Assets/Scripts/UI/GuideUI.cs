﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuideUI : MonoBehaviour
{
    [SerializeField]
    Button leftArrowButton;
    [SerializeField]
    Button rightArrowButton;
    
    [SerializeField]
    RectTransform middlePosition;
    [SerializeField]
    RectTransform rightPosition;
    [SerializeField]
    RectTransform leftPostion;

    public GameObject[] pages;
    public int curentPage = 0;
    public float movePageAnimationDuration;
    
	void Start ()
    {
        leftArrowButton.onClick.AddListener(PreviousPage);
        rightArrowButton.onClick.AddListener(NextPage);
	}

    void NextPage()
    {
        if(curentPage < pages.Length-1)
        {
            StartCoroutine(MoveAnimation(pages[curentPage].transform, pages[curentPage].transform.position, leftPostion.position,
                   0, movePageAnimationDuration));
            curentPage++;
            StartCoroutine(MoveAnimation(pages[curentPage].transform, pages[curentPage].transform.position, middlePosition.position,
                 0, movePageAnimationDuration));
        }
    }

    void PreviousPage()
    {
        if (curentPage > 0)
        {
            StartCoroutine(MoveAnimation(pages[curentPage].transform, pages[curentPage].transform.position, rightPosition.position, 
                0, movePageAnimationDuration));
            curentPage--;
            StartCoroutine(MoveAnimation(pages[curentPage].transform, pages[curentPage].transform.position, middlePosition.position,
                 0, movePageAnimationDuration));
        }
    }

    // Coroutine that swaps positions of two objects in duration seconds
    IEnumerator MoveAnimation(Transform objectToSwap, Vector2 startPosition, Vector2 target, float time, float duration)
    {
        float t = time;
        t += Time.deltaTime / duration;
        objectToSwap.position = Vector2.Lerp(startPosition, target, t);
        yield return new WaitForEndOfFrame();
        if (objectToSwap == null)
            yield break;
        else if (Vector2.Distance(objectToSwap.transform.position, target) > 0.1f)
            StartCoroutine(MoveAnimation(objectToSwap, startPosition, target, t, duration));
        else
        {
            objectToSwap.transform.position = target;
            yield return null;
        }
    }
}
