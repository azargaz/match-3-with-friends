﻿using UnityEngine;
using System;
using System.Collections;

public class DestroyIfExists : MonoBehaviour
{
    [SerializeField]
    Component objectTypeToDestroy;
    public bool dontDestroyOnLoad = false;
    public int maxCopiesOfObject = 1;
    bool destroy = true;

    void Awake()
    {
        if (dontDestroyOnLoad)
            DontDestroyOnLoad(gameObject);

        Type type = objectTypeToDestroy.GetType();
        _DestroyIfExists(type);
    }

    void _DestroyIfExists(Type type)
    {
        UnityEngine.Object[] objects = FindObjectsOfType(type);
        if (objects.Length > maxCopiesOfObject && destroy)
            Destroy(gameObject);
        else
            destroy = false;
    }
}
