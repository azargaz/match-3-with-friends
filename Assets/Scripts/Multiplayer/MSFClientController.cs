﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Barebones.MasterServer;
using Barebones.Networking;
using Barebones.Logging;

public class MSFClientController : MonoBehaviour
{
    public event Action<GameServerMatchDetailsPacket> ConnectToGameServer;
    public event Action GameStarted;
    public event Action GameEnded;

    //Added
    public event Action LoggingOut;
    public event Action BetweenGames;

    public string ServerIp = "127.0.0.1";
    public int ServerPort = 5000;
    public ClientController controller;

    // timeouts and waits
    private float ConnectToMasterTimeout = 10f;
    //private float LogInTimeout = 5f;

    private float WaitingToConnectToMaster = .5f;

    private float RequestedGameTimeout = 60f;
    private float WaitingBetweenGames = 1f;

    public bool useWs = true;

    public float ConnectToGameServerTimeout = 30f;

    private IClientSocket gameServerSocket;

    public LogLevel LogLevel = LogLevel.Info;
    public BmLogger Logger = Msf.Create.Logger(typeof(MSFClientController).Name);

    void Start()
    {
        UnityEngine.Random.InitState((int)(DateTime.Now.Ticks % 10000));

        Logger.LogLevel = LogLevel;

        Msf.Connection.Connected += Connected;
        Msf.Connection.Disconnected += Disconnected;

        //Added
        Msf.Client.Auth.LoggedIn += LoggedIn;
        Msf.Client.Auth.LoggedOut += LoggedOut;

        GoToState_Initial();
    }
    
    //Added
    public void SingleplayerGame()
    {
        StopBackgroundEnumerator();
        backgroundEnumerator = CoroutineUtils.StartWaiting(WaitingBetweenGames,
                () => {
                    GoToState_GameStarted();
                },
                1f,
                (time) => { controller.Status = string.Format("Waiting {0}s", time); }); StartCoroutine(backgroundEnumerator);
    }

    //Added
    public void SearchForGame()
    {
        StopBackgroundEnumerator();
        backgroundEnumerator = CoroutineUtils.StartWaiting(WaitingBetweenGames,
                () => {
                    GoToState_RequestedGame();
                },
                1f,
                (time) => { controller.Status = string.Format("Waiting {0}s", time); }); StartCoroutine(backgroundEnumerator);
    }

    //Added
    public void CancelSearchForGame()
    {
        StopBackgroundEnumerator();

        var msg = MessageHelper.Create((short)CustomOpCodes.cancelRequestForGame, "Please");
        Msf.Connection.Peer.SendMessage(msg);

        GoToState_BetweenGames();
    }

    //Added
    public void LogOut()
    {
        CancelSearchForGame();
        LoggingOut.Invoke();
        StartCoroutine(CoroutineUtils.StartWaiting(1,
             () => { Msf.Client.Auth.LogOut();
             },
             1f,
             (time) => { controller.Status = string.Format("Waiting {0}s", time); }, false));
    }
    
    //Added
    public void EndGame()
    {
        GoToState_GameEnded();
    }

    //Added
    private void LoggedIn()
    {
        GoToState_LoggedIn(Msf.Client.Auth.AccountInfo.Username);
    }

    //Added
    private void LoggedOut()
    {
        StopBackgroundEnumerator();
    }
    
    private void Connected()
    {
        GoToState_ConnectedToMaster();
    }

    private void Disconnected()
    {
        StopBackgroundEnumerator();
        if(Msf.Client.Auth.IsLoggedIn)
            LogOut(); 
        else
            LoggingOut.Invoke();

        GoToState_DisconnectedFromMaster();
    }

    private void GoToState_Initial()
    {
        controller.clientState = ClientState.Initial;
        controller.Status = string.Empty;

        StartCoroutine(CoroutineUtils.StartWaiting(WaitingToConnectToMaster,
            () => { GoToState_ConnectingToMaster(); },
            1f,
            (time) => { controller.Status = string.Format("Waiting {0}s", time); }, false));
    }

    private IEnumerator backgroundEnumerator;

    private void StopBackgroundEnumerator()
    {
        if (backgroundEnumerator != null)
        {
            StopCoroutine(backgroundEnumerator);
            backgroundEnumerator = null;
        }
    }

    private void GoToState_ConnectingToMaster()
    {
        controller.clientState = ClientState.ConnectingToMaster;

        backgroundEnumerator = CoroutineUtils.StartWaiting(ConnectToMasterTimeout,
                () => { GoToState_FailedToConnectToMaster("Timed out"); },
                1f,
                (time) => { controller.Status = string.Format("\"{0}:{1}\" {2}s", ServerIp, ServerPort, time); }); StartCoroutine(backgroundEnumerator);

        if (Msf.Connection.IsConnected)
            GoToState_ConnectedToMaster();
        else
            Msf.Connection.Connect(ServerIp, ServerPort);
    }

    private void OnConnected()
    {
        GoToState_ConnectedToMaster();
    }

    private void GoToState_FailedToConnectToMaster(string error)
    {
        controller.clientState = ClientState.FailedToConnectToMaster;
        controller.Status = error;
        StartCoroutine(CoroutineUtils.StartWaiting(1f, () => { GoToState_Initial(); }));
    }

    private void GoToState_ConnectedToMaster()
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.ConnectedToMaster;
        controller.Status = string.Format("at \"{0}:{1}\"", ServerIp, ServerPort);

        StartCoroutine(CoroutineUtils.StartWaiting(1f,
            () => { GoToState_LoggingIn(); }));
    }

    private void GoToState_LoggingIn()
    {
        if (Msf.Client.Auth.IsLoggedIn)
            GoToState_LoggedIn(Msf.Client.Auth.AccountInfo.Username);

        controller.clientState = ClientState.LoggingIn;
        controller.Status = string.Empty;

        //backgroundEnumerator = CoroutineUtils.StartWaiting(LogInTimeout,
        //    () => { GoToState_FailedToLogIn("Timed out"); },
        //    1f,
        //    (time) => {
        //        controller.Status =
        //            string.Format("Trying to log in {0}s", time);
        //    });
        //StartCoroutine(backgroundEnumerator);

        //Msf.Client.Auth.LogInAsGuest((successful, loginError) =>
        //{
        //    if (successful == null)
        //    {
        //        GoToState_FailedToLogIn(loginError);
        //    }
        //    else
        //    {
        //        GoToState_LoggedIn(Msf.Client.Auth.AccountInfo.Username);
        //    }
        //});        
    }

    private void GoToState_LoggedIn(string username)
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.LoggedIn;
        controller.Status = string.Format("Logged in: {0}", username);

        Msf.Client.Connection.SetHandler((short)CustomOpCodes.gameServerMatchDetails, OnGameServerMatchDetails);

        //Added
        //GoToState_BetweenGames();

        StartCoroutine(CoroutineUtils.StartWaiting(1f,
                () => { GoToState_BetweenGames(); }));
    }

    private void GoToState_FailedToLogIn(string loginError)
    {
        controller.clientState = ClientState.FailedToLogIn;
        controller.Status = loginError;

        StartCoroutine(CoroutineUtils.StartWaiting(3f,
            () => { GoToState_LoggingIn(); }));
    }

    private void GoToState_BetweenGames()
    {
        //Added
        if(BetweenGames != null)
            BetweenGames.Invoke();

        controller.clientState = ClientState.BetweenGames;
        controller.Status = string.Empty;

        //backgroundEnumerator = CoroutineUtils.StartWaiting(WaitingBetweenGames,
        //    () => { GoToState_RequestedGame(); },
        //    1f,
        //    (time) => { controller.Status = string.Format("Waiting {0}s", time); }, false);
        //StartCoroutine(backgroundEnumerator);
    }

    private void GoToState_RequestedGame()
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.RequestedGame;
        controller.Status = string.Empty;

        var msg = MessageHelper.Create((short)CustomOpCodes.requestStartGame, "Please");
        Msf.Connection.Peer.SendMessage(msg);

        backgroundEnumerator = CoroutineUtils.StartWaiting(RequestedGameTimeout,
            () => { GoToState_FailedToGetGame("Timed out"); },
            1f,
            (time) => { controller.Status = string.Format("Waiting for game {0}s", time); });
        StartCoroutine(backgroundEnumerator);
    }

    private void GoToState_FailedToGetGame(string message)
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.FailedToGetGame;
        controller.Status = message;

        controller.GamesAborted = controller.GamesAborted + 1;

        StartCoroutine(CoroutineUtils.StartWaiting(3f, () => { GoToState_BetweenGames(); }));
    }

    private void OnGameServerMatchDetails(IIncommingMessage message)
    {
        GameServerMatchDetailsPacket details = message.Deserialize(new GameServerMatchDetailsPacket());
        GoToState_AssignedGame(details);
    }

    private void GoToState_AssignedGame(GameServerMatchDetailsPacket details)
    {
        StopBackgroundEnumerator();

        string s = string.Format("Game Server Details  SpawnId {0}  SpawnCode {1}  MachineId {2}  AssignedPort {3}  GameSecondaryPort {4}",
            details.SpawnId,
            details.SpawnCode,
            details.MachineId,
            details.AssignedPort,
            details.GamePort);
        Logger.Info(s);

        controller.clientState = ClientState.AssignedGame;
        controller.Status = string.Empty;

        GoToState_ConnectingToGameServer(details);
    }

    private void GoToState_ConnectingToGameServer(GameServerMatchDetailsPacket details)
    {
        controller.clientState = ClientState.ConnectingToGameServer;
        controller.Status = string.Empty;

        SetupGameServerSocket();

        Logger.Info(string.Format("Connect to game server on {0}:{1}", details.MachineId, details.GamePort));

        gameServerSocket.Connect(details.MachineId, details.AssignedPort);

        backgroundEnumerator = CoroutineUtils.StartWaiting(ConnectToGameServerTimeout,
            () => { GoToState_FailedToConnectToGameServer(); },
            1f,
            (time) => { controller.Status = string.Format("Waiting for game {0}s", time); });
        StartCoroutine(backgroundEnumerator);

        if (ConnectToGameServer != null)
            ConnectToGameServer.Invoke(details);
    }

    private void GoToState_DisconnectedFromMaster()
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.DisconnectedFromMaster;
        controller.Status = string.Empty;
        
        //Added
        GoToState_Stop();
        
        //StartCoroutine(CoroutineUtils.StartWaiting(2f, () => { GoToState_Stop(); }));
    }

    private void GoToState_Stop()
    {
        controller.clientState = ClientState.Stop;
        controller.Status = string.Empty;

        StartCoroutine(CoroutineUtils.StartWaiting(WaitingToConnectToMaster,
            () => { GoToState_ConnectingToMaster(); },
            1f,
           (time) => { controller.Status = string.Format("Waiting {0}s", time); }, false));
    }

    private void SetupGameServerSocket()
    {
        if (useWs)
            gameServerSocket = new ClientSocketWs();
        else gameServerSocket = new ClientSocketUnet();

        gameServerSocket.Connected += GoToState_ConnectedToGameServer;
        gameServerSocket.Disconnected += GoToState_DisconnectedFromGameServer;
        gameServerSocket.SetHandler(new PacketHandler(0, HandleGameServerMessage));
    }

    private void HandleGameServerMessage(IIncommingMessage msg)
    {
        string s = msg.ToString();
        controller.Status = s;
        if (s.CompareTo("Start Game") == 0)
        {
            GoToState_GameStarted();
            return;
        }
        if (s.CompareTo("End Game") == 0)
        {
            GoToState_GameEnded();
            return;
        }
    }

    private void GoToState_GameStarted()
    {
        controller.clientState = ClientState.GameStarted;
        controller.Status = string.Empty;

        if (GameStarted != null)
            GameStarted.Invoke();
    }

    private void GoToState_GameEnded()
    {
        controller.clientState = ClientState.GameEnded;
        controller.Status = string.Empty;

        if (gameServerSocket != null)
        {
            gameServerSocket.Disconnected -= GoToState_DisconnectedFromGameServer;
            gameServerSocket.Disconnect();
            gameServerSocket = null;
            controller.GamesPlayed = controller.GamesPlayed + 1;
        }

        if (GameEnded != null)
            GameEnded.Invoke();

        //GoToState_BetweenGames();

        //Added
        backgroundEnumerator = CoroutineUtils.StartWaiting(1f,
            () => { GoToState_BetweenGames(); },
            1f,
            (time) => { controller.Status = string.Format("Waiting {0}s", time); }, false);
        StartCoroutine(backgroundEnumerator);
    }

    private void GoToState_ConnectedToGameServer()
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.ConnectedToGameServer;
        controller.Status = string.Empty;
    }

    private void GoToState_DisconnectedFromGameServer()
    {
        StopBackgroundEnumerator();

        if (controller.clientState != ClientState.GameEnded)
        {
            controller.GamesAborted = controller.GamesAborted + 1;
            Logger.Debug("aborted " + controller.clientState);
        }

        controller.clientState = ClientState.DisconnectedFromGameServer;
        controller.Status = string.Empty;

        if(gameServerSocket != null)
        {
            gameServerSocket.Connected -= GoToState_ConnectedToGameServer;
            gameServerSocket.Disconnected -= GoToState_DisconnectedFromGameServer;
            gameServerSocket = null;
        }

        if (!Msf.Connection.IsConnected)
            GoToState_DisconnectedFromMaster();
    }

    private void GoToState_FailedToConnectToGameServer()
    {
        StopBackgroundEnumerator();

        controller.clientState = ClientState.FailedToConnectToGameServer;
        controller.Status = string.Empty;

        controller.GamesAborted = controller.GamesAborted + 1;

        gameServerSocket = null;
    }

    //Added
    private void OnDestroy()
    {
        Msf.Connection.Connected -= Connected;
        Msf.Connection.Disconnected -= Disconnected;
        Msf.Client.Auth.LoggedIn -= LoggedIn;
        Msf.Client.Auth.LoggedOut -= LoggedOut;
    }
}
