﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum ClientState
{
    Initial,                        // -> ConnectingToMaster
    ConnectingToMaster,             // if success -> ConnectedToMaster, if failure -> FailedToConnectToMaster
    ConnectedToMaster,              // -> LoggingIn
    FailedToConnectToMaster,        // Stop
    LoggingIn,                      // if success -> LoggedIn, if failure -> FailedToLogIn
    LoggedIn,                       // -> BetweenGames
    FailedToLogIn,                  // Stop
    BetweenGames,                   // RequestingGame
    RequestedGame,                  // if success -> ConnectingToGameServer, if failure -> FailedToGetGame
    AssignedGame,                   // -> ConnectingToGameServer
    FailedToGetGame,                // Stop
    ConnectingToGameServer,         // if success -> ConnectedToGameSever, if failure -> FailedToConnectToGameServer
    ConnectedToGameServer,          // if get GameStarted -> GameStarted
    FailedToConnectToGameServer,    // Stop
    GameStarted,                    // if get GameEnded -> GameEnded
    GameEnded,                      // -> DisconnectFromGameServer
    DisconnectedFromGameServer,     // -> BetweenGames
    DisconnectedFromMaster,         // Disconnected from master server
    Stop
};

public class ClientController : MonoBehaviour
{
    private ClientState _clientState;
    public ClientState clientState
    {
        get { return _clientState; }
        set
        {
            _clientState = value;
            ShowClientState();
        }
    }

    private string _status;
    public string Status
    {
        set
        {
            _status = value;
        }
        get { return _status; }
    }

    ColorBlock highlightedColorBlock;

    public Button[] Buttons;

    private void ShowClientState()
    {

    }

    public int GamesPlayed { get; set; }
    public int GamesAborted { get; set; }

    void Start()
    {
        Random.InitState((int)(System.DateTime.Now.Ticks % 10000));
        GamesPlayed = 0;
        GamesAborted = 0;

        highlightedColorBlock = ColorBlock.defaultColorBlock;
        highlightedColorBlock.highlightedColor = new Color(135 / 255f, 206 / 255f, 250 / 255f);

        clientState = ClientState.Initial;
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        Rect rect = new Rect(2, 120, Screen.width, style.fontSize);
        style.fontSize = 18;
        style.alignment = TextAnchor.UpperLeft;
        style.normal.textColor = Color.white;
        string text = string.Format("[played {0} | aborted {1} | {2}] \n{3}",
            GamesPlayed, GamesAborted, clientState, Status);
        GUI.Label(rect, text, style);
    }
}
