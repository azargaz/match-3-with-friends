﻿using Barebones.MasterServer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkFunctions : NetworkBehaviour
{
    public static NetworkFunctions instance;

    private void Start()
    {
        if (isLocalPlayer || BoardManager.instance.singleplayer)
        {
            instance = this;
        }
    }

    [Command]
    public void CmdPlayerReady()
    {
        lock (BoardManager.instance)
        {
            BoardManager.instance.playersReady++;
        }
    }

    [ClientRpc]
    public void RpcSetupFinished()
    {
        BoardManager.instance.gameReady = true;
    }
    
    [ClientRpc]
    public void RpcStartGame()
    {
        BoardManager.instance.gameStarted = true;
    }

    [ClientRpc]
    public void RpcFinishGame()
    {
        BoardManager.instance.gameFinished = true;
        BoardManager.instance.CheckWinningConditions();
    }

    [Command]
    public void CmdGenerateSeed(int id)
    {
        int _seed = (int)System.DateTime.Now.Ticks + Random.Range(0, 1000000);
        RpcGenerateSeed(id, _seed);
    }
    [ClientRpc]
    void RpcGenerateSeed(int id, int _seed)
    {
        if (BoardManager.instance != null && id == BoardManager.instance.myId)
        {
            BoardManager.instance.seed = _seed;
            BoardManager.instance.SetupRandomBoard();
        }
    }

    [Command]
    public void CmdSetWinner(int id, bool draw)
    {
        RpcSetWinner(id, draw);
    }
    [ClientRpc]
    void RpcSetWinner(int id, bool draw)
    {
        if (BoardManager.instance != null && DataManager.instance != null)
        {
            if (id != 0)
            {
                if (id == BoardManager.instance.myId)
                    BoardManager.instance.isWinner = true;
                else
                    BoardManager.instance.isWinner = false;
            }

            BoardManager.instance.draw = draw;
            BoardManager.instance.gameFinished = true;
            UIManager.ui.DisplayBattleResults();
            DataManager.instance.SaveBattleResults();
        }
    }

    //public void UpdateEnemyEnergy(int id, int energyLevel, int color)
    //{
    //    CmdUpdateEnemyEnergy(id, energyLevel, color);
    //}
    //[Command]
    //void CmdUpdateEnemyEnergy(int id, int energyLevel, int color)
    //{
    //    RpcUpdateEnemyEnergy(id, energyLevel, color);
    //}
    //[ClientRpc]
    //void RpcUpdateEnemyEnergy(int id, int energyLevel, int color)
    //{
    //    if (SpellController.instance != null)
    //    {
    //        SpellController.instance.currentEnergies[id - 1] = energyLevel;
    //        SpellController.instance.energyColors[id - 1] = (Piece.PieceColor)color;
    //        SpellController.instance.UpdateEnemyEnergy();
    //    }
    //}

    [Command]
    public void CmdUpdateScore(int index, int value)
    {
        RpcUpdateScore(index, value);
    }
    [ClientRpc]
    void RpcUpdateScore(int index, int value)
    {
        if (DataManager.instance != null && BoardManager.instance != null)
        {
            DataManager.instance.scores[index] = value;
            DataManager.instance.UpdateEnemyScore();
            BoardManager.instance.CheckWinningConditions();
        }
    }

    public void UpdateProfile(BattleResults GameBattleResults, int playerScore, string username)
    {
        CmdUpdateProfile(GameBattleResults, playerScore, username);
    }
    [Command]
    void CmdUpdateProfile(BattleResults GameBattleResults, int playerScore, string username)
    {
        //if (Msf.Client.Auth.AccountInfo.IsGuest)
        //    return;

        // Construct the profile
        var profile = new ObservableServerProfile(username)
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore),
            new ObservableInt(MyProfileKeys.OverallScore)
        };

        // Fill profile values
        Msf.Server.Profiles.FillProfileValues(profile, (successful, error) =>
        {
            if (!successful)
            {
                Logs.Error(error);
                return;
            }

            // Modify profile properties (changes will automatically be sent to the master server, 
            // and then to client)
            ObservableDictStringInt piecesRemoved = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.PiecesRemoved);
            for (int i = 0; i < GameBattleResults.piecesOfColorRemoved.Length; i++)
            {
                PieceInfo pieces = GameBattleResults.piecesOfColorRemoved[i];
                int piecesToAdd = pieces.piecesRemoved + piecesRemoved.GetValue(pieces.color.ToString());
                piecesRemoved.SetValue(pieces.color.ToString(), piecesToAdd);
            }

            ObservableDictStringInt boostsUsed = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.BoostsUsed);
            for (int i = 0; i < GameBattleResults.boostsUsed.Length; i++)
            {
                BoostInfo boosts = GameBattleResults.boostsUsed[i];
                int boostsToAdd = boosts.boostsUsed + boostsUsed.GetValue(boosts.boostType.ToString());
                boostsUsed.SetValue(boosts.boostType.ToString(), boostsToAdd);
            }

            ObservableDictStringInt spellsCast = profile.GetProperty<ObservableDictStringInt>(MyProfileKeys.SpellsCast);
            for (int i = 0; i < GameBattleResults.spellsCast.Length; i++)
            {
                SpellInfo spells = GameBattleResults.spellsCast[i];
                int spellsToAdd = spells.spellsCast + spellsCast.GetValue(spells.spellType.ToString());
                spellsCast.SetValue(spells.spellType.ToString(), spellsToAdd);
            }

            ObservableInt highscore = profile.GetProperty<ObservableInt>(MyProfileKeys.Highscore);
            if (playerScore > highscore.Value)
                highscore.Set(playerScore);

            ObservableInt overallScore = profile.GetProperty<ObservableInt>(MyProfileKeys.OverallScore);
            overallScore.Add(playerScore);
        });
    }

    [ClientRpc]
    public void RpcTriggerRandomEvent()
    {
        StartCoroutine(BoardManager.instance.TriggerRandomEvent());
    }

    [Client]
    public void RpcUpdateTimeLeft(float gameTimeLeft, float timeLeftToStart)
    {
        BoardManager.instance.gameTimeLeft = gameTimeLeft;
        if(!BoardManager.instance.gameStarted)
            BoardManager.instance.timeLeftToStart = timeLeftToStart;
    }
}
