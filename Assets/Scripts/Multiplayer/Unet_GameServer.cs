﻿using Barebones.Logging;
using Barebones.MasterServer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Unet_GameServer : NetworkBehaviour
{
    public LogLevel LogLevel = LogLevel.All;
    public BmLogger Logger = Msf.Create.Logger(typeof(Unet_GameServer).Name);

    public GameServer gameServer;
    public Unet_NetworkManager unetNetworkManager;

    void Start()
    {
        Logger.LogLevel = LogLevel;

        gameServer.Registered += Registered;
        gameServer.GameStarted += GameStarted;
        gameServer.GameEnded += GameEnded;
    }

    private void GameStarted()
    {
        Logger.Info("GameStarted");
    }

    private void GameEnded()
    {
        Logger.Info("GameEnded");

        unetNetworkManager.HostDisconnect();
    }

    private void Registered(GameServerMatchDetailsPacket details)
    {
        Logger.Info(string.Format("Registered {0}", details.GamePort));
        unetNetworkManager.HostConnect(details.GamePort);
    }
}