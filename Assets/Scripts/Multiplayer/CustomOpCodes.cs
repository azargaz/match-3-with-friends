﻿public enum CustomOpCodes : short
{
    requestStartGame = 1,
    gameServerMatchDetails,
    gameServerMatchCompletion,
    cancelRequestForGame
}