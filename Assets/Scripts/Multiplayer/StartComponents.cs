﻿using UnityEngine;
using System.Collections.Generic;
using Barebones;
using Barebones.MasterServer;

public class StartComponents : MonoBehaviour
{
    public GameObject ClientController;
    public GameObject MasterServer;
    public GameObject GameServer;
    public GameObject Spawner;
    public GUIConsole guiConsole;

    public GameObject Unet;
    public GameObject UnetClientController;
    public GameObject UnetGameServer;

    public GameObject UICanvas;

    public bool client;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        bool anythingStarted = false;
        bool inEditor = false;

#if UNITY_EDITOR
        inEditor = true;
#endif
        if (inEditor)
        {
            //StartMasterServer();
            //StartSpawner();
            StartClient();
            anythingStarted = true;
        }

        if (Msf.Args.SpawnId != -1)
        {
            StartGameServer();
            anythingStarted = true;
            client = false;
        }

        if (Msf.Args.StartMaster)
        {
            StartMasterServer();
            anythingStarted = true;
            client = false;
        }

        if (Msf.Args.IsProvided(Msf.Args.Names.StartSpawner))
        {
            StartSpawner();
            anythingStarted = true;
            client = false;
        }

        if (!client)
            UICanvas.SetActive(false);

        if (Msf.Args.IsProvided("-client") || client)
        {
            StartClient();
            anythingStarted = true;
        }

        if (!anythingStarted)
        {
            Debug.Log(string.Format("supported args: -client {0} {1}", Msf.Args.Names.StartMaster, Msf.Args.Names.StartSpawner));
        }
    }

    private void StartClient()
    {
        guiConsole.Show = false;
        Unet.gameObject.SetActive(true);
        UnetClientController.gameObject.SetActive(true);
        ClientController.gameObject.SetActive(true);
    }

    private void StartGameServer()
    {
        Unet.gameObject.SetActive(true);
        UnetGameServer.gameObject.SetActive(true);
        GameServer.gameObject.SetActive(true);
        guiConsole.Show = false;
    }

    private void StartMasterServer()
    {
        MasterServer.gameObject.SetActive(true);
        MasterServerBehaviour master = MasterServer.GetComponent<MasterServerBehaviour>();
        master.StartServer();
    }

    private void StartSpawner()
    {
        guiConsole.Show = true;
        Msf.Connection.Connect(Msf.Args.MasterIp, Msf.Args.MasterPort);
        Spawner.gameObject.SetActive(true);
    }
}
