﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barebones.Logging;
using Barebones.MasterServer;
using Barebones.Networking;
using System;

public enum MasterServerFunctionsOpCodes : short
{
    requestLastLoginTime = 20,
    newLoginTime = 21,
    updateLastLoginTime = 22
}

public class LastLoginTimePacket : SerializablePacket
{
    public long oldLastLoginTime = 0;
    public long newLastLoginTime = 0;

    public override void FromBinaryReader(EndianBinaryReader reader)
    {
        oldLastLoginTime = reader.ReadInt64();
        newLastLoginTime = reader.ReadInt64();
    }

    public override void ToBinaryWriter(EndianBinaryWriter writer)
    {
        writer.Write(oldLastLoginTime);
        writer.Write(newLastLoginTime);
    }
}

public class MasterServerFunctions : ServerModuleBehaviour
{
    public LogLevel LogLevel = LogLevel.All;
    public BmLogger Logger = Msf.Create.Logger(typeof(GameServer).Name);

    public override void Initialize(IServer server)
    {
        base.Initialize(server);

        // Add handlers
        server.SetHandler((short)MasterServerFunctionsOpCodes.requestLastLoginTime, HandleLastLoginTimeRequest);
        server.SetHandler((short)MasterServerFunctionsOpCodes.updateLastLoginTime, HandleUpdateLastLoginTime);
    }
    
    void SendUpdatedLastLoginTime(IPeer sender, long newTicks, long oldTicks)
    {
        LastLoginTimePacket packet = new LastLoginTimePacket()
        {
            oldLastLoginTime = oldTicks,
            newLastLoginTime = newTicks
        };

        sender.SendMessage((short)MasterServerFunctionsOpCodes.newLoginTime, packet);
    }

    void HandleUpdateLastLoginTime(IIncommingMessage message)
    {
        var profile = new ObservableServerProfile(message.ToString())
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore),
            new ObservableInt(MyProfileKeys.OverallScore),
            new ObservableInt(MyProfileKeys.PlayerLevel),
            new ObservableInt(MyProfileKeys.Experience),
            new ObservableInt(MyProfileKeys.Stamina),
            new ObservableInt(MyProfileKeys.Gems),
            new ObservableInt(MyProfileKeys.Coins),
            new ObservableString(MyProfileKeys.LastLoginTime, "0")
        };

        Msf.Server.Profiles.FillProfileValues(profile, (successful, error) =>
        {
            long newTicks = DateTime.Now.Ticks;
            ObservableString lastLoginTime = profile.GetProperty<ObservableString>(MyProfileKeys.LastLoginTime);
            lastLoginTime.Set(newTicks.ToString());
        });
    }

    void HandleLastLoginTimeRequest(IIncommingMessage message)
    {
        var profile = new ObservableServerProfile(message.ToString())
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore),
            new ObservableInt(MyProfileKeys.OverallScore),
            new ObservableInt(MyProfileKeys.PlayerLevel),
            new ObservableInt(MyProfileKeys.Experience),
            new ObservableInt(MyProfileKeys.Stamina),
            new ObservableInt(MyProfileKeys.Gems),
            new ObservableInt(MyProfileKeys.Coins),
            new ObservableString(MyProfileKeys.LastLoginTime, "0")
        };

        Msf.Server.Profiles.FillProfileValues(profile, (successful, error) =>
        {
            long newTicks = DateTime.Now.Ticks;
            long oldTicks = 0;

            ObservableString lastLoginTime = profile.GetProperty<ObservableString>(MyProfileKeys.LastLoginTime);
            if(lastLoginTime.Value == "")            
                lastLoginTime.Set("0");

            oldTicks = long.Parse(lastLoginTime.Value);

            SendUpdatedLastLoginTime(message.Peer, newTicks, oldTicks);
        });
    }
}
