﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class SpellController : NetworkBehaviour
{
    public Spell[] standardSpells;
    public Spell[] spells;
    [System.Serializable]
    public class Spell
    {
        [HideInInspector]
        public int currentEnergy;
        public spellData data;
    }

    public static SpellController instance;

    [SerializeField]
    PlayerInput playerInput;

    void Awake()
    {
        if (isLocalPlayer || BoardManager.instance.singleplayer)
            instance = this;        
    }

    public void AddEnergy(Piece.PieceColor color)
    {
        for (int i = 0; i < spells.Length; i++)
        {
            spellData data = spells[i].data;
            if(color.ToString() == data.flavor && spells[i].currentEnergy < data.energyRequire)
            {
                spells[i].currentEnergy++;
            }
        }
    }

    public void AddEnergy(string _color, int amount)
    {
        Piece.PieceColor color = Piece.StringToEnum<Piece.PieceColor>(_color);
        for (int i = 0; i < spells.Length; i++)
        {
            spellData data = spells[i].data;
            if (color.ToString() == data.flavor && spells[i].currentEnergy < data.energyRequire)
            {
                spells[i].currentEnergy += amount;
                spells[i].currentEnergy = Mathf.Clamp(spells[i].currentEnergy, 0, data.energyRequire);
            }
        }
    }
    
    public void RemoveEnergy(int amount)
    {
        for (int i = 0; i < spells.Length; i++)
        {
            spells[i].currentEnergy -= amount;
            if (spells[i].currentEnergy < 0)
                spells[i].currentEnergy = 0;
        }
    }

    /// <summary>
    /// Cast spell, casted by player
    /// </summary>
    /// <param name="index">index of spell in players spell list</param>
    public void CastSpell(int index)
    {
        if (playerInput.busy)
            return;

        Spell castedSpell = null;
        if(index < standardSpells.Length)
        {
            castedSpell = standardSpells[index];
        }
        else
        {
            index -= (standardSpells.Length);
            castedSpell = spells[index];
        }

        castedSpell.currentEnergy = 0;
        DataManager.instance.playerScore -= castedSpell.data.energyRequire;
        CastSpell(castedSpell.data, true);
    }

    /// <summary>
    /// Cast spell, casted by third party or opponent, not by player
    /// </summary>
    /// <param name="spell">spellData of spell being cast</param>
    public void CastSpell(spellData spell, bool castByPlayer)
    {
        for (int i = 0; i < spell.effects.Length; i++)
        {
            spellData.effect effect = spell.effects[i];
            playerInput.CastSpell(effect, castByPlayer);
        }
    }

    public void OpponentCastSpell(spellData spell)
    {
        CastSpell(spell, false);
    }

    public void RandomEventCastSpell(spellData spell)
    {
        CastSpell(spell, false);
    }

    //[SerializeField]
    //PlayerInput playerInput;

    //public enum SpellType
    //{
    //    automatch,
    //    reshuffle,
    //    removeRandom,
    //    sendBlocks
    //}

    //public int maxEnergy;
    //[HideInInspector]
    //public int currentEnergy;
    //[HideInInspector]
    //public int enemyCurrentEnergy;
    //[HideInInspector]
    //public int additionalEnergy;
    //[HideInInspector]
    //public Piece.PieceColor enemyEnergyColor = Piece.PieceColor.red;
    //public Piece.PieceColor energyColor = Piece.PieceColor.red;

    //[HideInInspector]
    //public int[] currentEnergies = new int[2];
    //[HideInInspector]
    //public Piece.PieceColor[] energyColors = new Piece.PieceColor[2];

    //[System.Serializable]
    //public class Spell
    //{
    //    public SpellType type;
    //    public int scoreCost;
    //    public bool hasEnergyCost;

    //    // Initialize spell
    //    public Spell(SpellType _type, int _cost, bool _hasEnergyCost)
    //    {
    //        hasEnergyCost = _hasEnergyCost;
    //    }
    //}

    //public Spell[] spells;
    //HashSet<Piece.PieceColor> availableColors;
    //List<Piece.PieceColor> allColors;

    //public static SpellController instance;

    //private void Start()
    //{
    //    if (isLocalPlayer || BoardManager.instance.singleplayer)
    //        instance = this;

    //    availableColors = new HashSet<Piece.PieceColor>();
    //    availableColors.UnionWith(BoardManager.instance.spawnablePieces);
    //    allColors = new List<Piece.PieceColor>();
    //    allColors.AddRange(BoardManager.instance.spawnablePieces);

    //    ChangeEnergyColor(ref availableColors, allColors);
    //}

    //// This function is called by a button that casts certain spell
    //public void CastSpell(int _spell)
    //{
    //    if (BoardManager.instance == null || playerInput.busy || !BoardManager.instance.gameStarted)
    //        return;

    //    // Get spell properties in form of Spell class
    //    SpellType spellType = (SpellType)_spell;
    //    Spell spell = GetSpell(spellType);

    //    // If energy bar is full or spell has no energy bar - cast it
    //    if ((spell.hasEnergyCost && currentEnergy >= maxEnergy) || !spell.hasEnergyCost)
    //    {
    //        Debug.Log("Cast spell " + spellType);
    //        // Switch that calls function for each type of spell
    //        switch (spellType)
    //        {
    //            case SpellType.automatch:
    //                {
    //                    playerInput.AutoSwap();
    //                    break;
    //                }
    //            case SpellType.reshuffle:
    //                {
    //                    playerInput.Reshuffle();
    //                    break;
    //                }
    //            case SpellType.removeRandom:
    //                {
    //                    playerInput.RemoveRandomPiece();
    //                    break;
    //                }
    //            case SpellType.sendBlocks:
    //                {
    //                    playerInput.SendBlocks();
    //                    break;
    //                }
    //        }
    //        DataManager.instance.UpdateScore(-spell.scoreCost);
    //        DataManager.instance.gameBattleResults.UpdateResults(spell.type);

    //        if (spell.hasEnergyCost)
    //        {
    //            DecreaseEnergy();
    //        }

    //        playerInput.playerMadeViableMove = true;
    //        playerInput.swapHolder = null;
    //    }
    //}

    //Spell GetSpell(SpellType spell)
    //{
    //    for (int i = 0; i < spells.Length; i++)
    //    {
    //        if (spells[i].type == spell)
    //            return spells[i];
    //    }

    //    return null;
    //}

    //public void IncreaseEnergy()
    //{
    //    if (currentEnergy < maxEnergy)
    //    {
    //        currentEnergy++;
    //        SendToEnemyEnergyData();
    //    }
    //    else
    //        additionalEnergy++;
    //}

    //void DecreaseEnergy()
    //{
    //    ChangeEnergyColor(ref availableColors, allColors);
    //    currentEnergy = 0;
    //    additionalEnergy = 0;
    //    SendToEnemyEnergyData();
    //}

    //void SendToEnemyEnergyData()
    //{
    //    if (BoardManager.instance.singleplayer)
    //        return;

    //    NetworkFunctions.instance.UpdateEnemyEnergy(BoardManager.instance.myId, currentEnergy, (int)energyColor);
    //}

    //public void UpdateEnemyEnergy()
    //{
    //    if (BoardManager.instance.myId == 1)
    //    {
    //        enemyCurrentEnergy = currentEnergies[1];
    //        enemyEnergyColor = energyColors[1];
    //    }
    //    else
    //    {
    //        enemyCurrentEnergy = currentEnergies[0];
    //        enemyEnergyColor = energyColors[0];
    //    }
    //}

    //// Change energy color to new one, that is not used
    //void ChangeEnergyColor(ref HashSet<Piece.PieceColor> availableColors, List<Piece.PieceColor> allColors)
    //{
    //    availableColors.Remove(energyColor);
    //    availableColors.Remove(enemyEnergyColor);
    //    Piece.PieceColor newColor = allColors[Random.Range(0, allColors.Count)];
    //    while (!availableColors.Contains(newColor))
    //        newColor = allColors[Random.Range(0, allColors.Count)];
    //    if (!availableColors.Contains(energyColor))
    //        availableColors.Add(energyColor);
    //    energyColor = newColor;
    //    availableColors.Remove(energyColor);
    //}
}
