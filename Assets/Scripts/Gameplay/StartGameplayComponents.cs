﻿using UnityEngine;
using System;
using System.Collections;

public class StartGameplayComponents : MonoBehaviour
{
    public Action GameSetup;

    public GameObject[] objectsToEnable;

    MSFClientController ClientController;

    bool gameIsSetup = false;

    void Start()
    {
        ClientController = FindObjectOfType<MSFClientController>();
        GameSetup = null;

        if(ClientController != null)
        {
            ClientController.GameStarted += SetupGame;
        }
    }

    void SetupGame()
    {
        if(!gameIsSetup)
        {
            gameIsSetup = true;

            for (int i = 0; i < objectsToEnable.Length; i++)
            {
                objectsToEnable[i].SetActive(true);
            }

            if(GameSetup != null)
                GameSetup.Invoke();
        }
    }

    private void OnDestroy()
    {
        ClientController.GameStarted -= SetupGame;
    }
}