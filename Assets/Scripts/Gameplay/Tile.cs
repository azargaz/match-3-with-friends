﻿using UnityEngine;

public class Tile : MonoBehaviour {

    public int pollutionLayers = 0;

    public int score;
    public float bonusRate = 0.2f;
    public Vector2Int indexInPieceBoard;
}
