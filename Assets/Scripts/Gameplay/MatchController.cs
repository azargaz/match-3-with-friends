﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

#region Global variables

public enum Step
{
    PlayerAction = 1,
    ResolveMatches = 2,
    SpellsAndAutoBoardAction = 3,
    ResolveChainMatches = 4
};

#endregion

public class MatchController : NetworkBehaviour
{
    #region Variables

    [SerializeField]
    private Step currentStep = Step.PlayerAction;
    public Step CurrentStep
    {
        get
        {
            return currentStep;
        }

        set
        {
            currentStep = value;
            OnSwitchStep(currentStep);
        }
    }

    // Assign this in the inspector
    [SerializeField]
    PlayerInput playerInput;
    // Assign this in the inspector
    [SerializeField]
    SpellController spellController;

    // for testing, shows where you can make a swap
    public bool highlightAvalableMove = false;

    // List of pieces marked for removal
    List<Piece> markedToRemove = new List<Piece>();
    // List of "hit"s - pieces that can create boost
    List<Piece> hitList = new List<Piece>();
    // HashSet of used position for forming a boost 
    // HashSet performs better than list if want to check if something is inside list and don't care about indexing
    HashSet<Vector2Int> usedHitList = new HashSet<Vector2Int>();
    // List of triggered boosts
    List<Boost> boostTrigger = new List<Boost>();
    // List of formed boosts that will be spawned right before RefillBoard()
    List<Boost> boostsToSpawn = new List<Boost>();
    // List of square matches, used to determine if game should create one tile boost
    List<Piece> squareMatches = new List<Piece>();

    public int blocksDefaultLayers = 1;
    public int blocksToSpawnQueue = 0;

    // Scores
    int pieceScore;
    int spellScore;
    int piecesRemoved = 0;
    int breakScore;
    int chainBonusScore = 30;
    int turn = 0;

    int subSpellScore;
    int subPieceScore;
    int objectScore;

    // This represents how much pieces you need to remove at once to get this much score per piece
    [System.Serializable]
    public class PieceBonus
    {
        public int minimumPieces;
        public float pieceBonus;

        public PieceBonus(int _minimumPieces, float _pieceBonus)
        {
            minimumPieces = _minimumPieces;
            pieceBonus = _pieceBonus;
        }
    }
    [SerializeField]
    PieceBonus[] pieceBonuses =
    {
        new PieceBonus(0, 1),
        new PieceBonus(4, 1.5f),
        new PieceBonus(9, 1.8f),
        new PieceBonus(15, 2.1f),
        new PieceBonus(49, 2.3f)
    };

    // Boost class, created to easier pass information about boosts that will be spawned
    class Boost
    {
        public Boost(Vector2Int _index, Piece.BoostType _boostType, Piece.PieceColor _color)
        {
            indexInPieceBoard = _index;
            boostType = _boostType;
            color = _color;
        }

        public Vector2Int indexInPieceBoard;
        public Piece.BoostType boostType;
        public Piece.PieceColor color;
    }

    // Duration of pieces moving animation
    [Range(0f, 1f)]
    public float delayBeforeStep3 = 0.5f;
    [Range(0f, 1f)]
    public float moveAnimationDuration = 0.02f;
    [Range(0f, 1f)]
    public float reshuffleAnimationDuration = 0.02f;

    [HideInInspector]
    public bool movingPieces = false;

    public static MatchController instance;

    private void Start()
    {
        if (isLocalPlayer || BoardManager.instance.singleplayer)
        {
            instance = this;
        }
    }

    #endregion

    // Called when server switches step for this client
    void OnSwitchStep(Step step)
    {
        switch (step)
        {
            case Step.PlayerAction:
                {
                    turn++;
                    playerInput.StartPlayersTurn();
                    break;
                }
            case Step.ResolveMatches:
                {
                    StartCoroutine(CheckMatchBoard(false));
                    break;
                }
            case Step.SpellsAndAutoBoardAction:
                {
                    StartCoroutine(AutoBoardAction());
                    break;
                }
            case Step.ResolveChainMatches:
                {
                    StartCoroutine(ChainMatches());
                    break;
                }
        }
    }

    #region Step 2: Resolve matches

    // Check if there is match, if there is none go back to PlayerInput
    public bool ResolveMatches(Piece firstPiece, Piece secondPiece)
    {
        #region Check for match-3

        int firstPieceMatch = 0;
        int secondPieceMatch = 0;

        // Loop that goes thorugh horizontal positions to check if there is match
        for (int x = -2; x < 3; x++)
        {
            if (x == 0)
                continue;

            Vector2Int indexToCheck = firstPiece.indexInPieceBoard + new Vector2Int(x, 0);

            // Checking matches for first piece
            if (BoardManager.IsInBoard(indexToCheck))
            {
                Piece checkedPiece = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];
                if (checkedPiece != null)
                {
                    if (checkedPiece.color == firstPiece.color
                        && checkedPiece.color != Piece.PieceColor.rainbow && checkedPiece.color != Piece.PieceColor.inert)
                        firstPieceMatch++;
                    else
                        firstPieceMatch = 0;
                }
                else
                    firstPieceMatch = 0;
            }
            else
                firstPieceMatch = 0;

            // Checking matches for second piece
            indexToCheck = secondPiece.indexInPieceBoard + new Vector2Int(x, 0);
            if (BoardManager.IsInBoard(indexToCheck))
            {
                Piece checkedPiece = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];
                if (checkedPiece != null)
                {
                    if (checkedPiece.color == secondPiece.color
                        && checkedPiece.color != Piece.PieceColor.rainbow && checkedPiece.color != Piece.PieceColor.inert)
                        secondPieceMatch++;
                    else
                        secondPieceMatch = 0;
                }
                else
                    secondPieceMatch = 0;
            }
            else
                secondPieceMatch = 0;

            if (firstPieceMatch >= 2 || secondPieceMatch >= 2)
                return true;
        }

        // Reset counters
        firstPieceMatch = 0;
        secondPieceMatch = 0;

        // Loop that goes thorugh vertical positions to check if there is match
        for (int y = -2; y < 3; y++)
        {
            if (y == 0)
                continue;

            Vector2Int indexToCheck = firstPiece.indexInPieceBoard + new Vector2Int(0, y);

            if (BoardManager.IsInBoard(indexToCheck))
            {
                Piece checkedPiece = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];
                if (checkedPiece != null)
                {
                    if (checkedPiece.color == firstPiece.color
                        && checkedPiece.color != Piece.PieceColor.rainbow && checkedPiece.color != Piece.PieceColor.inert)
                        firstPieceMatch++;
                    else
                        firstPieceMatch = 0;
                }
                else
                    firstPieceMatch = 0;
            }
            else
                firstPieceMatch = 0;

            indexToCheck = secondPiece.indexInPieceBoard + new Vector2Int(0, y);
            if (BoardManager.IsInBoard(indexToCheck))
            {
                Piece checkedPiece = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];
                if (checkedPiece != null)
                {
                    if (checkedPiece.color == secondPiece.color
                        && checkedPiece.color != Piece.PieceColor.rainbow && checkedPiece.color != Piece.PieceColor.inert)
                        secondPieceMatch++;
                    else
                        secondPieceMatch = 0;
                }
                else
                    secondPieceMatch = 0;
            }
            else
                secondPieceMatch = 0;

            if (firstPieceMatch >= 2 || secondPieceMatch >= 2)
                return true;
        }

        #endregion

        #region Check for square match

        int squareMatch = 0;

        // Check if two pieces are horizontal to each other, depending on this set which squares we need to search

        bool horizontalPieces = (firstPiece.indexInPieceBoard - secondPiece.indexInPieceBoard).y == 0;
        int max_x = horizontalPieces ? 3 : 2;
        int max_y = horizontalPieces ? 2 : 3;

        // Two loops that go through 6 squares using left-lower corner as reference to check if all 4 pieces in square are same color
        for (int x = -1; x < max_x; x++)
        {
            for (int y = -1; y < max_y; y++)
            {
                Vector2Int referenceIndex = new Vector2Int(x + firstPiece.indexInPieceBoard.x, y + firstPiece.indexInPieceBoard.y);
                // Check if referene piece is even inside board
                if (BoardManager.IsInBoard(referenceIndex))
                {
                    // Reference piece
                    Piece referencePiece = BoardManager.pieceBoard[referenceIndex.x, referenceIndex.y];

                    // Other pieces we need to check to make sure there is square match
                    Vector2Int[] otherSqaureIndexes = {
                        new Vector2Int(referenceIndex.x, referenceIndex.y + 1),
                        new Vector2Int(referenceIndex.x + 1, referenceIndex.y + 1),
                        new Vector2Int(referenceIndex.x + 1, referenceIndex.y)
                    };

                    // Loop that goes through square
                    for (int i = 0; i < otherSqaureIndexes.Length; i++)
                    {
                        // Check if part of square is inside board, else break loop as there can't be square match
                        if (BoardManager.IsInBoard(otherSqaureIndexes[i]))
                        {
                            Piece otherPiece = BoardManager.pieceBoard[otherSqaureIndexes[i].x, otherSqaureIndexes[i].y];
                            if (otherPiece == null || referencePiece == null)
                                break;
                            else
                            {
                                // Check if pieces are same color, else break the loop as there can't be square match
                                if (otherPiece.color == referencePiece.color
                                    && referencePiece.color != Piece.PieceColor.rainbow && referencePiece.color != Piece.PieceColor.inert)
                                    squareMatch++;
                                else
                                    break;
                            }
                        }
                        else
                            break;
                    }

                    if (squareMatch >= 3)
                    {
                        return true;
                    }
                }
                squareMatch = 0;
            }
        }

        #endregion

        // If no matches return false
        return false;
    }

    // Check for special match like rainbow piece + normal piece, or two boosts
    public bool CheckForSpecialMatch(Piece firstPiece, Piece secondPiece)
    {
        playerInput.busy = true;

        bool specialMatch = false;

        if (firstPiece.color == Piece.PieceColor.rainbow && secondPiece.color == Piece.PieceColor.rainbow)
        {
            // Two rainbow pieces
            specialMatch = true;
        }
        else if (firstPiece.color == Piece.PieceColor.rainbow || secondPiece.color == Piece.PieceColor.rainbow)
        {
            if (firstPiece.Boost != Piece.BoostType.none)
            {
                // Boost piece + rainbow piece
                specialMatch = true;
            }
            else if (secondPiece.Boost != Piece.BoostType.none)
            {
                // Rainbow piece + boost piece
                specialMatch = true;
            }
            else
            {
                // Rainbow piece + normal piece
                specialMatch = true;
            }
        }
        else if (firstPiece.Boost != Piece.BoostType.none && secondPiece.Boost != Piece.BoostType.none)
        {
            // Two boost pieces
            specialMatch = true;
        }

        if (specialMatch)
        {
            // Normal piece + rainbow piece
            if (firstPiece.Boost != Piece.BoostType.none && secondPiece.Boost == Piece.BoostType.none)
            {
                Boost firstBoost = new Boost(firstPiece.indexInPieceBoard, firstPiece.Boost, secondPiece.color);
                ProcessBoost(firstBoost, null);
            }
            else if (secondPiece.Boost != Piece.BoostType.none && firstPiece.Boost == Piece.BoostType.none)
            {
                Boost secondBoost = new Boost(secondPiece.indexInPieceBoard, secondPiece.Boost, firstPiece.color);
                ProcessBoost(secondBoost, null);
            }
            // Two boost pieces
            else
            {
                Boost firstBoost = new Boost(firstPiece.indexInPieceBoard, firstPiece.Boost, (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow));
                Boost secondBoost = new Boost(secondPiece.indexInPieceBoard, secondPiece.Boost, (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow));

                // If one of boosts is rainbow piece
                if (firstBoost.boostType == Piece.BoostType.rainbow || secondBoost.boostType == Piece.BoostType.rainbow)
                {
                    // if both are rainbow, it clears whole board, so no need to process both boosts
                    if (firstBoost.boostType == Piece.BoostType.rainbow && secondBoost.boostType == Piece.BoostType.rainbow)
                        ProcessBoost(firstBoost, secondBoost);
                    // else do normal rainbow boost + spawn new boosts in places of removed pieces and trigger them
                    else if (firstBoost.boostType == Piece.BoostType.rainbow)
                    {
                        ProcessBoost(firstBoost, secondBoost);
                        secondPiece.Boost = Piece.BoostType.none;
                    }
                    else
                    {
                        ProcessBoost(secondBoost, firstBoost);
                        firstPiece.Boost = Piece.BoostType.none;
                    }
                }
                else if (firstBoost.boostType == Piece.BoostType.oneTile)
                {
                    ProcessBoost(firstBoost, secondBoost);
                    secondPiece.Boost = Piece.BoostType.none;
                }
                else if (secondBoost.boostType == Piece.BoostType.oneTile)
                {
                    firstPiece.Boost = Piece.BoostType.none;
                    ProcessBoost(secondBoost, firstBoost);
                }
                else
                {
                    ProcessBoost(firstBoost, secondBoost);
                }
            }

            return true;
        }

        return false;
    }

    // Check gameboard for matches and mark them for removal and add them to hitList, then process hitList
    IEnumerator CheckMatchBoard(bool chain)
    {
        playerInput.busy = true;

        // Mark pieces for removal and add hits to hitList
        #region Match-3

        for (int y = 0; y < BoardManager.height; y++)
        {
            for (int x = 0; x < BoardManager.width - 2; x++)
            {
                Piece piece = BoardManager.pieceBoard[x, y];
                Piece nextPiece = BoardManager.pieceBoard[x + 1, y];
                Piece nextNextPiece = BoardManager.pieceBoard[x + 2, y];
                Block web = BoardManager.blockBoard[x, y];
                Block nextWeb = BoardManager.blockBoard[x + 1, y];
                Block nextNextWeb = BoardManager.blockBoard[x + 2, y];

                if (piece != null && nextPiece != null && nextNextPiece != null)
                {
                    if (piece.color != Piece.PieceColor.inert && piece.color != Piece.PieceColor.rainbow && piece.color == nextPiece.color && piece.color == nextNextPiece.color)
                    {
                        if (markedToRemove.Contains(piece))
                            hitList.Add(piece);
                        else if (markedToRemove.Contains(nextPiece))
                            hitList.Add(nextPiece);
                        else if (markedToRemove.Contains(nextNextPiece))
                            hitList.Add(nextNextPiece);

                        if (web == null || web.GetType() != typeof(Web))
                            MarkPieceForRemoval(piece);
                        else
                            web.blockLayers--;

                        if (nextWeb == null || nextWeb.GetType() != typeof(Web))
                            MarkPieceForRemoval(nextPiece);
                        else
                            nextWeb.blockLayers--;

                        if (nextNextWeb == null || nextNextWeb.GetType() != typeof(Web))
                            MarkPieceForRemoval(nextNextPiece);
                        else
                            nextNextWeb.blockLayers--;
                    }
                }
            }
        }

        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height - 2; y++)
            {
                Piece piece = BoardManager.pieceBoard[x, y];
                Piece nextPiece = BoardManager.pieceBoard[x, y + 1];
                Piece nextNextPiece = BoardManager.pieceBoard[x, y + 2];
                Block web = BoardManager.blockBoard[x, y];
                Block nextWeb = BoardManager.blockBoard[x, y + 1];
                Block nextNextWeb = BoardManager.blockBoard[x, y + 2];

                if (piece != null && nextPiece != null && nextNextPiece != null)
                {
                    if (piece.color != Piece.PieceColor.inert && piece.color != Piece.PieceColor.rainbow && piece.color == nextPiece.color && piece.color == nextNextPiece.color)
                    {
                        if (markedToRemove.Contains(piece))
                            hitList.Add(piece);
                        else if (markedToRemove.Contains(nextPiece))
                            hitList.Add(nextPiece);
                        else if (markedToRemove.Contains(nextNextPiece))
                            hitList.Add(nextNextPiece);

                        if (web == null || web.GetType() != typeof(Web))
                            MarkPieceForRemoval(piece);
                        else
                            web.blockLayers--;

                        if (nextWeb == null || nextWeb.GetType() != typeof(Web))
                            MarkPieceForRemoval(nextPiece);
                        else
                            nextWeb.blockLayers--;

                        if (nextNextWeb == null || nextNextWeb.GetType() != typeof(Web))
                            MarkPieceForRemoval(nextNextPiece);
                        else
                            nextNextWeb.blockLayers--;
                    }
                }
            }
        }
        #endregion

        #region Square matches

        int squareMatch = 0;

        // Two loops that go through 6 squares using left-lower corner as reference to check if all 4 pieces in square are same color

        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Vector2Int referenceIndex = new Vector2Int(x, y);
                // Check if referene piece is even inside board
                if (BoardManager.IsInBoard(referenceIndex) && !BoardManager.IsBlocked(referenceIndex))
                {
                    // Reference piece
                    Piece referencePiece = BoardManager.pieceBoard[referenceIndex.x, referenceIndex.y];

                    // Other pieces we need to check to make sure there is square match
                    Vector2Int[] otherSqaureIndexes = {
                        new Vector2Int(referenceIndex.x, referenceIndex.y + 1),
                        new Vector2Int(referenceIndex.x + 1, referenceIndex.y + 1),
                        new Vector2Int(referenceIndex.x + 1, referenceIndex.y)
                    };

                    // Loop that goes through square
                    for (int i = 0; i < otherSqaureIndexes.Length; i++)
                    {
                        // Check if part of square is inside board, else break loop as there can't be square match
                        if (BoardManager.IsInBoard(otherSqaureIndexes[i]) && !BoardManager.IsBlocked(otherSqaureIndexes[i]))
                        {
                            Piece otherPiece = BoardManager.pieceBoard[otherSqaureIndexes[i].x, otherSqaureIndexes[i].y];
                            if (otherPiece == null || referencePiece == null)
                                break;
                            else
                            {
                                // Check if pieces are same color, else break the loop as there can't be square match
                                if (otherPiece.color == referencePiece.color
                                    && referencePiece.color != Piece.PieceColor.rainbow && referencePiece.color != Piece.PieceColor.inert)
                                    squareMatch++;
                                else
                                    break;
                            }
                        }
                        else
                            break;
                    }

                    if (squareMatch >= 3)
                    {
                        for (int i = 0; i < otherSqaureIndexes.Length; i++)
                        {
                            Piece otherPiece = BoardManager.pieceBoard[otherSqaureIndexes[i].x, otherSqaureIndexes[i].y];
                            if (otherPiece != null)
                            {
                                MarkPieceForRemoval(otherPiece);
                            }
                        }
                        MarkPieceForRemoval(referencePiece);
                        squareMatches.Add(referencePiece);
                        x++;
                        y++;
                    }
                }
                squareMatch = 0;
            }
        }
        #endregion

        #region Process hitList

        // Go through hitList and determine each shape and what type of boost it will create
        for (int i = 0; i < hitList.Count; i++)
        {
            // If this is blocked hit or marked as used, ignore it
            if (BoardManager.IsBlockedOrWebbed(hitList[i].indexInPieceBoard) || usedHitList.Contains(hitList[i].indexInPieceBoard))
                continue;

            int leftPattern = DetermineBoostPattern(hitList[i], new Vector2Int(-1, 0));
            int rightPattern = DetermineBoostPattern(hitList[i], new Vector2Int(1, 0));
            int upPattern = DetermineBoostPattern(hitList[i], new Vector2Int(0, 1));
            int downPattern = DetermineBoostPattern(hitList[i], new Vector2Int(0, -1));

            int numberOfZeros = (leftPattern == 0 ? 1 : 0) + (rightPattern == 0 ? 1 : 0) + (upPattern == 0 ? 1 : 0) + (downPattern == 0 ? 1 : 0);

            // Rainbow            
            if (leftPattern + rightPattern >= 4 || upPattern + downPattern >= 4)
            {
                Boost boost = new Boost(hitList[i].indexInPieceBoard, Piece.BoostType.rainbow, Piece.PieceColor.rainbow);
                boostsToSpawn.Add(boost);
                MarkPiecesAsUsed(hitList[i]);
            }
            // line or L shape
            else if (numberOfZeros >= 2)
            {
                // L shape
                if (leftPattern + upPattern == 0 || upPattern + rightPattern == 0 || rightPattern + downPattern == 0 || downPattern + leftPattern == 0)
                {
                    Boost boost = new Boost(hitList[i].indexInPieceBoard, Piece.BoostType.cross, hitList[i].color);
                    boostsToSpawn.Add(boost);
                    MarkPiecesAsUsed(hitList[i]);
                }
                // line - 4 in a col/row
                else if (leftPattern + rightPattern == 0 || upPattern + downPattern == 0)
                {
                    Boost boost = new Boost(hitList[i].indexInPieceBoard, leftPattern + rightPattern == 0 ? Piece.BoostType.row : Piece.BoostType.column, hitList[i].color);
                    boostsToSpawn.Add(boost);
                    MarkPiecesAsUsed(hitList[i]);
                }
            }
            // + or T shape
            else if (numberOfZeros <= 1)
            {
                Boost boost = new Boost(hitList[i].indexInPieceBoard, Piece.BoostType.around, hitList[i].color);
                boostsToSpawn.Add(boost);
                MarkPiecesAsUsed(hitList[i]);
            }

        }

        // Add square boosts if there are any hits not used
        for (int k = 0; k < squareMatches.Count; k++)
        {
            Vector2Int referenceIndex = squareMatches[k].indexInPieceBoard;
            Vector2Int[] otherSqaureIndexes = {
                new Vector2Int(referenceIndex.x, referenceIndex.y + 1),
                new Vector2Int(referenceIndex.x + 1, referenceIndex.y + 1),
                new Vector2Int(referenceIndex.x + 1, referenceIndex.y)
            };

            int square = 0;
            // Check if all 4 pieces in a square are not used
            for (int j = 0; j < otherSqaureIndexes.Length; j++)
            {
                Piece otherPiece = BoardManager.pieceBoard[otherSqaureIndexes[j].x, otherSqaureIndexes[j].y];
                if (otherPiece != null && !usedHitList.Contains(otherSqaureIndexes[j]))
                {
                    square++;
                }
            }
            // If they are not used add to spawn one tile boost
            if (square >= 3)
            {
                Boost boost = new Boost(squareMatches[k].indexInPieceBoard, Piece.BoostType.oneTile, squareMatches[k].color);
                boostsToSpawn.Add(boost);
                MarkPiecesAsUsed(squareMatches[k]);
            }
        }

        hitList.Clear();
        usedHitList.Clear();
        squareMatches.Clear();

        #endregion

        if (!chain)
        {
            yield return new WaitForSeconds(delayBeforeStep3);
            playerInput.GoToNextStep(CurrentStep);
        }
    }

    #endregion

    // AutoBoard function, calls every neccessary function in Step 3
    IEnumerator AutoBoardAction()
    {
        #region Step 3: spells and auto action

        // Remove marked pieces, add boosts to boostTrigger list and if possible decrease pollution of tile under piece
        RemovePieces(false);

        #region Process boosts in boostTrigger list
        for (int i = 0; i < boostTrigger.Count; i++)
        {
            ProcessBoost(boostTrigger[i], null);
        }
        boostTrigger.Clear();
        #endregion

        UpdateScore();

        // Move pieces, refill board and wait while pieces are moving
        yield return new WaitWhile(IsMovingPieces);
        StartCoroutine(MovePiecesDown());
        yield return new WaitWhile(IsMovingPieces);
        StartCoroutine(RefillBoard());
        yield return new WaitWhile(IsMovingPieces);
        //StartCoroutine(SpawnRandomBlocks());
        //yield return new WaitWhile(IsMovingPieces);

        // Resolve chain matches
        playerInput.GoToNextStep(CurrentStep);

        #endregion
    }

    // Chain matches, calls every neccessary function in Step 4
    IEnumerator ChainMatches()
    {
        #region Step 4: resolve chain matches

        // Resolve chain matches 
        // ('true' as argument so that ChechMatchBoard() function won't call AutoBoardAction() again and won't make infinite loop
        StartCoroutine(CheckMatchBoard(true));

        // Remove marked pieces, add boosts to boostTrigger list and if possible decrease pollution of tile under piece
        RemovePieces(false);

        #region Process boosts in boostTrigger list
        for (int i = 0; i < boostTrigger.Count; i++)
        {
            ProcessBoost(boostTrigger[i], null);
        }
        boostTrigger.Clear();
        #endregion

        // Move pieces and wait before refilling board
        yield return new WaitWhile(IsMovingPieces);
        StartCoroutine(MovePiecesDown());
        yield return new WaitWhile(IsMovingPieces);
        StartCoroutine(RefillBoard());
        AddBoostPieces();
        yield return new WaitWhile(IsMovingPieces);

        // If no more moves are available - reshuffle the board
        if (!CheckForAvailableMoves())
        {
            StartCoroutine(ReshuffleBoard());
            yield return new WaitWhile(IsMovingPieces);
        }

        UpdateScore();

        // Start next turn
        playerInput.GoToNextStep(CurrentStep);

        #endregion
    }

    #region Score functions

    void UpdateScore()
    {
        #region Add Scores
        int resolutionScore = 0;
        float pieceBonus = 1f;
        for (int i = pieceBonuses.Length - 1; i >= 0; i--)
        {
            if (piecesRemoved >= pieceBonuses[i].minimumPieces)
            {
                pieceBonus = pieceBonuses[i].pieceBonus;
                break;
            }
        }

        resolutionScore = Mathf.RoundToInt(Mathf.RoundToInt((pieceScore * pieceBonus + breakScore * 0.8f) * 0.2f) / 0.2f);
        if (CurrentStep == Step.ResolveChainMatches && (pieceScore > 0 || breakScore > 0))
            resolutionScore += chainBonusScore;
        resolutionScore += spellScore;

        DataManager.instance.UpdateScore(resolutionScore);

        spellScore = 0;
        pieceScore = 0;
        breakScore = 0;
        piecesRemoved = 0;
        #endregion

        #region Subtract Scores
        int subScore = 0;

        subScore = (int)(Mathf.RoundToInt(subSpellScore + subPieceScore + objectScore * 0.2f) / 0.2f);
        DataManager.instance.UpdateScore(-subScore);

        subSpellScore = 0;
        subPieceScore = 0;
        objectScore = 0;
        #endregion
    }

    void AddBreakScore(int score, float bonusRate)
    {
        float timeBonus = 1 + turn * bonusRate;
        breakScore += (int)(Mathf.RoundToInt(score * timeBonus * 0.2f) / 0.2f);
    }

    #endregion

    #region Auto Board Action and Chain Matches functions

    // Add boosts to boostTrigger list, check tiles for pollution and decrease it, 
    // impact adjacent pieces and decrease layers of blocks and finally remove pieces
    void RemovePieces(bool removedByBoost)
    {
        for (int i = 0; i < markedToRemove.Count; i++)
        {
            if (markedToRemove[i] == null)
                continue;

            // Record piece removed
            DataManager.instance.gameBattleResults.UpdateResults(markedToRemove[i].color);

            spellController.AddEnergy(markedToRemove[i].color);

            #region Add boosts to BoostTrigger
            if (markedToRemove[i].Boost != Piece.BoostType.none)
            {
                Boost boost = new Boost(markedToRemove[i].indexInPieceBoard, markedToRemove[i].Boost, (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow));
                boostTrigger.Add(boost);
            }
            #endregion

            #region Check tile under and decrease it's pollution layer if possible
            Vector2Int pieceIndex = markedToRemove[i].indexInPieceBoard;
            Tile tileUnderPiece = BoardManager.tileBoard[pieceIndex.x, pieceIndex.y];
            if (tileUnderPiece != null)
            {
                if (tileUnderPiece.pollutionLayers > 0)
                {
                    AddBreakScore(tileUnderPiece.score, tileUnderPiece.bonusRate);
                    tileUnderPiece.pollutionLayers--;
                }
            }
            #endregion

            #region Decrease layers of blocks and remove inert pieces

            // Block which we are checking right now
            Block block = BoardManager.blockBoard[markedToRemove[i].indexInPieceBoard.x, markedToRemove[i].indexInPieceBoard.y];

            // Remove webs and blocks with 0 layers
            if (block != null)
            {
                // Destroy block
                if (block.GetType() == typeof(Web) || removedByBoost)
                {
                    DecreaseLayerOfBlock(block, 1);
                }
                continue;
            }

            // Array of adjacent positions which we check in loop below
            Vector2Int[] adjacentPositions = { new Vector2Int(-1, 0), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(0, 1) };

            // Check adjacent positions and impact them if they have blocks on them
            for (int j = 0; j < adjacentPositions.Length; j++)
            {
                Vector2Int checkedPosition = markedToRemove[i].indexInPieceBoard + adjacentPositions[j];

                if (BoardManager.IsInBoard(checkedPosition))
                {
                    block = BoardManager.blockBoard[checkedPosition.x, checkedPosition.y];
                    Piece inert = BoardManager.pieceBoard[checkedPosition.x, checkedPosition.y];
                    if (block != null)
                    {
                        DecreaseLayerOfBlock(block, 1);
                    }
                    // Destroy inert piece
                    if (inert != null && inert.color == Piece.PieceColor.inert)
                    {
                        AddBreakScore(inert.score, inert.bonusRate);
                        BoardManager.pieceBoard[checkedPosition.x, checkedPosition.y] = null;
                        Destroy(inert.gameObject);
                    }
                }
            }

            #endregion

            // Destroy piece
            BoardManager.pieceBoard[pieceIndex.x, pieceIndex.y] = null;
            Destroy(markedToRemove[i].gameObject);
        }

        if (breakScore > 0)
            turn = 0;
        markedToRemove.Clear();
    }

    // Process boost given as argument
    void ProcessBoost(Boost boost, Boost otherBoost)
    {
        if (otherBoost != null && boost == null)
        {
            boost = otherBoost;
            otherBoost = null;
        }
        string otherBoostType = "";
        if (otherBoost != null)
            otherBoostType = " + " + otherBoost.boostType;
        Debug.Log("Triggered boost: " + boost.boostType + otherBoostType);

        if (otherBoost != null
            && boost.boostType != Piece.BoostType.oneTile
            && boost.boostType != Piece.BoostType.rainbow
            && boost.boostType != Piece.BoostType.none)
        {
            // Shape (like this but for whole board)
            // 1 0 1 0 1
            // 0 1 1 1 0
            // 1 1 1 1 1
            // 0 1 1 1 0
            // 1 0 1 0 1
            int width = BoardManager.width;
            int height = BoardManager.height;
            for (int x = -width; x < width; x++)
            {
                for (int y = -height; y < height; y++)
                {
                    if (x == 0 || y == 0 || Mathf.Abs(x) == Mathf.Abs(y))
                    {
                        Vector2Int pos = new Vector2Int(x + boost.indexInPieceBoard.x, y + boost.indexInPieceBoard.y);

                        if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                        {
                            Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                            if (piece != null)
                            {
                                MarkPieceForRemoval(piece);
                            }
                        }
                        else if (BoardManager.IsInBoard(pos) && BoardManager.IsBlocked(pos))
                        {
                            Block block = BoardManager.blockBoard[pos.x, pos.y];
                            DecreaseLayerOfBlock(block, 1);
                        }
                    }
                }
            }
        }
        else
        {
            // Remove pieces depending on which type of boost it is
            switch (boost.boostType)
            {
                // Rainbow piece, remove every piece of given color
                case Piece.BoostType.rainbow:
                    {
                        for (int x = 0; x < BoardManager.width; x++)
                        {
                            for (int y = 0; y < BoardManager.height; y++)
                            {
                                Vector2Int pos = new Vector2Int(x, y);

                                if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                                {
                                    Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                                    if (piece != null && piece.color == boost.color)
                                    {
                                        MarkPieceForRemoval(piece);

                                        if (otherBoost != null && otherBoost.boostType != Piece.BoostType.rainbow)
                                        {
                                            Boost newBoost = otherBoost;
                                            newBoost.indexInPieceBoard = pos;
                                            boostTrigger.Add(newBoost);
                                        }
                                    }

                                    if (piece != null && otherBoost != null && otherBoost.boostType == Piece.BoostType.rainbow)
                                    {
                                        MarkPieceForRemoval(piece);
                                    }
                                }
                            }
                        }
                        break;
                    }
                // Around boost removes pieces around piece
                case Piece.BoostType.around:
                    {
                        // Shape:
                        // 1 1 1
                        // 1 0 1
                        // 1 1 1
                        for (int x = -1; x < 2; x++)
                        {
                            for (int y = -1; y < 2; y++)
                            {
                                if (x != 0 || y != 0)
                                {
                                    Vector2Int pos = new Vector2Int(x + boost.indexInPieceBoard.x, y + boost.indexInPieceBoard.y);

                                    if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                                    {
                                        Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                                        if (piece != null)
                                        {
                                            MarkPieceForRemoval(piece);
                                        }
                                    }
                                    else if (BoardManager.IsInBoard(pos) && BoardManager.IsBlocked(pos))
                                    {
                                        Block block = BoardManager.blockBoard[pos.x, pos.y];
                                        DecreaseLayerOfBlock(block, 1);
                                    }
                                }
                            }
                        }
                        break;
                    }
                // Around boost removes pieces around in a X shape
                case Piece.BoostType.cross:
                    {
                        // Shape:
                        // 1 0 0 0 1
                        // 0 1 0 1 0
                        // 0 0 1 0 0
                        // 0 1 0 1 0
                        // 1 0 0 0 1
                        for (int x = -2; x < 3; x++)
                        {
                            for (int y = -2; y < 3; y++)
                            {
                                if (Mathf.Abs(x) == Mathf.Abs(y) && x != 0 && y != 0)
                                {
                                    Vector2Int pos = new Vector2Int(x + boost.indexInPieceBoard.x, y + boost.indexInPieceBoard.y);

                                    if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                                    {
                                        Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                                        if (piece != null)
                                        {
                                            MarkPieceForRemoval(piece);
                                        }
                                    }
                                    else if (BoardManager.IsInBoard(pos) && BoardManager.IsBlocked(pos))
                                    {
                                        Block block = BoardManager.blockBoard[pos.x, pos.y];
                                        DecreaseLayerOfBlock(block, 1);
                                    }
                                }
                            }
                        }
                        break;
                    }
                // Row boost removes whole row
                case Piece.BoostType.row:
                    {
                        int y = boost.indexInPieceBoard.y;
                        for (int x = 0; x < BoardManager.width; x++)
                        {
                            Vector2Int pos = new Vector2Int(x, y);

                            if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                            {
                                Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                                if (piece != null)
                                {
                                    MarkPieceForRemoval(piece);
                                }
                            }
                            else if (BoardManager.IsInBoard(pos) && BoardManager.IsBlocked(pos))
                            {
                                Block block = BoardManager.blockBoard[pos.x, pos.y];
                                DecreaseLayerOfBlock(block, 1);
                            }
                        }
                        break;
                    }
                // Column boost removes whole column
                case Piece.BoostType.column:
                    {
                        int x = boost.indexInPieceBoard.x;
                        for (int y = 0; y < BoardManager.height; y++)
                        {
                            Vector2Int pos = new Vector2Int(x, y);

                            if (BoardManager.IsInBoard(pos) && !BoardManager.IsBlocked(pos))
                            {
                                Piece piece = BoardManager.pieceBoard[pos.x, pos.y];
                                if (piece != null)
                                {
                                    MarkPieceForRemoval(piece);
                                }
                            }
                            else if (BoardManager.IsInBoard(pos) && BoardManager.IsBlocked(pos))
                            {
                                Block block = BoardManager.blockBoard[pos.x, pos.y];
                                DecreaseLayerOfBlock(block, 1);
                            }
                        }
                        break;
                    }
                // oneTile boost removes randomly one tile, if for some reason it couldn't find not blocked tile after 1000 tries it fails
                case Piece.BoostType.oneTile:
                    {
                        Vector2Int highestValuePos = new Vector2Int(0, 0);
                        string highestValueType = "none";
                        Tile tile = null;
                        Piece piece = null;
                        Block block = null;

                        for (int x = 0; x < BoardManager.width; x++)
                        {
                            for (int y = 0; y < BoardManager.height; y++)
                            {
                                Vector2Int pos = new Vector2Int(x, y);

                                if (pos == boost.indexInPieceBoard || (otherBoost != null && pos == otherBoost.indexInPieceBoard))
                                    continue;

                                if (BoardManager.IsInBoard(pos))
                                {
                                    tile = BoardManager.tileBoard[pos.x, pos.y];
                                    piece = BoardManager.pieceBoard[pos.x, pos.y];
                                    block = BoardManager.blockBoard[pos.x, pos.y];

                                    Tile highestValueTile = BoardManager.tileBoard[highestValuePos.x, highestValuePos.y];
                                    Piece highestValuePiece = BoardManager.pieceBoard[highestValuePos.x, highestValuePos.y];
                                    Block highestValueBlock = BoardManager.blockBoard[highestValuePos.x, highestValuePos.y];

                                    // If current position we are looking at is polluted tile
                                    if (tile != null && tile.pollutionLayers > 0)
                                    {
                                        highestValueType = "tile";

                                        if (highestValueTile == null)
                                        {
                                            highestValuePos = pos;
                                            continue;
                                        }

                                        // If current highest value position is a polluted tile
                                        if (highestValueTile != null && highestValueTile.pollutionLayers > 0)
                                        {
                                            if (Random.value > 0.5f)
                                                highestValuePos = pos;
                                        }
                                        // Else it's either block or piece which both have lower priority
                                        else
                                            highestValuePos = pos;
                                    }
                                    // Else if not polluted tile and is blocked
                                    else if (block != null)
                                    {
                                        // If current highest value position is polluted tile, continue loop
                                        if (highestValueTile != null && highestValueTile.pollutionLayers > 0)
                                        {
                                            continue;
                                        }

                                        highestValueType = "block";
                                        if (highestValueBlock == null)
                                        {
                                            highestValuePos = pos;
                                            continue;
                                        }

                                        // If current position we are looking at is block, but not web
                                        if (block.GetType() != typeof(Web))
                                        {
                                            if (highestValueBlock.GetType() != typeof(Web))
                                            {
                                                if (Random.value > 0.5f)
                                                    highestValuePos = pos;
                                            }
                                            else
                                                highestValuePos = pos;
                                        }
                                        // If current position we are looking at is web
                                        else
                                        {
                                            if (highestValueBlock.GetType() == typeof(Web))
                                            {
                                                if (Random.value > 0.5f)
                                                    highestValuePos = pos;
                                            }
                                            else
                                                highestValuePos = pos;
                                        }
                                    }
                                    // Else look at piece at this pos
                                    else if (piece != null)
                                    {
                                        // If current highest value position is polluted tile, continue loop
                                        if (highestValueTile != null && highestValueTile.pollutionLayers > 0)
                                        {
                                            continue;
                                        }
                                        // If current highest value position is block, continue loop
                                        if (highestValueBlock != null)
                                        {
                                            continue;
                                        }

                                        highestValueType = "piece";
                                        if (highestValuePiece == null)
                                            highestValuePos = pos;

                                        if (highestValuePiece == null)
                                            continue;

                                        if (piece.color == Piece.PieceColor.inert)
                                        {
                                            if (highestValuePiece.color == Piece.PieceColor.inert)
                                            {
                                                if (Random.value > 0.5f)
                                                    highestValuePos = pos;
                                            }
                                            else
                                                highestValuePos = pos;
                                        }
                                        else if (piece.Boost != Piece.BoostType.none)
                                        {
                                            if (piece.Boost == Piece.BoostType.rainbow)
                                            {
                                                if (highestValuePiece.Boost == Piece.BoostType.rainbow)
                                                {
                                                    if (Random.value > 0.5f)
                                                        highestValuePos = pos;
                                                }
                                                else if (highestValuePiece.color != Piece.PieceColor.inert)
                                                    highestValuePos = pos;
                                            }
                                            else
                                            {
                                                if (highestValuePiece.color != Piece.PieceColor.inert)
                                                {
                                                    if (highestValuePiece.Boost != Piece.BoostType.none)
                                                    {
                                                        if (Random.value > 0.5f)
                                                            highestValuePos = pos;
                                                    }
                                                    else
                                                        highestValuePos = pos;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (highestValuePiece.Boost == Piece.BoostType.none && highestValuePiece.color != Piece.PieceColor.inert)
                                            {
                                                if (Random.value > 0.5f)
                                                    highestValuePos = pos;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        switch (highestValueType)
                        {
                            case "tile":
                                {
                                    piece = BoardManager.pieceBoard[highestValuePos.x, highestValuePos.y];
                                    MarkPieceForRemoval(piece);
                                    break;
                                }
                            case "piece":
                                {
                                    piece = BoardManager.pieceBoard[highestValuePos.x, highestValuePos.y];
                                    MarkPieceForRemoval(piece);
                                    break;
                                }
                            case "block":
                                {
                                    block = BoardManager.blockBoard[highestValuePos.x, highestValuePos.y];
                                    DecreaseLayerOfBlock(block, block.blockLayers);
                                    break;
                                }
                            default:
                                {
                                    Debug.LogError("OneTile boost couldn't find any target!");
                                    break;
                                }
                        }

                        if (otherBoost != null && otherBoost.boostType != Piece.BoostType.none)
                        {
                            otherBoost.indexInPieceBoard = highestValuePos;
                            boostTrigger.Add(otherBoost);
                        }
                        break;
                    }
            }
        }

        // Record boost used
        DataManager.instance.gameBattleResults.UpdateResults(boost.boostType);

        // Add boostPiece itself to markedToRemove list
        Piece boostPiece = BoardManager.pieceBoard[boost.indexInPieceBoard.x, boost.indexInPieceBoard.y];
        if (boostPiece != null)
        {
            // To make sure it's not triggered second time reset it's boost type
            boostPiece.Boost = Piece.BoostType.none;
            MarkPieceForRemoval(boostPiece);

            if (otherBoost != null)
            {
                Piece otherBoostPiece = BoardManager.pieceBoard[otherBoost.indexInPieceBoard.x, otherBoost.indexInPieceBoard.y];
                if (otherBoostPiece != null)
                {
                    otherBoostPiece.Boost = Piece.BoostType.none;
                    MarkPieceForRemoval(otherBoostPiece);
                }
            }
        }

        RemovePieces(true);
    }

    public void MarkPieceForRemoval(Piece piece)
    {
        if (!markedToRemove.Contains(piece))
        {
            pieceScore += piece.score;
            piecesRemoved++;
            markedToRemove.Add(piece);
        }
    }

    void DecreaseLayerOfBlock(Block block, int layers)
    {
        if (block.blockLayers > 0)
        {
            block.blockLayers -= layers;
        }

        // Destroy block
        if (block.blockLayers <= 0)
        {
            AddBreakScore(block.score, block.bonusRate);
            BoardManager.blockBoard[block.indexInPieceBoard.x, block.indexInPieceBoard.y] = null;
            Destroy(block.gameObject);
        }
    }

    // Moves all possible pieces down
    IEnumerator MovePiecesDown()
    {
        movingPieces = true;
        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 1; y < BoardManager.height; y++)
            {
                Piece currentPiece = BoardManager.pieceBoard[x, y];

                for (int i = 0; i < y; i++)
                {
                    if (BoardManager.IsInBoard(new Vector2Int(x, i)) && BoardManager.IsInBoard(new Vector2Int(x, y)))
                    {
                        if (!BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) && !BoardManager.IsBlockedOrWebbed(new Vector2Int(x, i)))
                        {
                            if (BoardManager.pieceBoard[x, i] == null && currentPiece != null)
                            {
                                BoardManager.pieceBoard[x, y] = null;
                                BoardManager.pieceBoard[x, i] = currentPiece;
                                currentPiece.indexInPieceBoard = new Vector2Int(x, i);

                                StartCoroutine(MoveAnimation(currentPiece.transform, currentPiece.transform.position, currentPiece.indexInPieceBoard, 0, moveAnimationDuration));
                                break;
                            }
                        }
                    }
                }
            }
        }
        yield return new WaitForSeconds(moveAnimationDuration);

        movingPieces = false;
    }

    // This function is slightly edited spawning methos in BoardManager's SetupBoard() function
    IEnumerator RefillBoard()
    {
        movingPieces = true;

        Piece.PieceColor[] previousLeft = new Piece.PieceColor[BoardManager.height];
        Piece.PieceColor previousBelow = Piece.PieceColor.inert;

        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Vector2 pos = new Vector2(x, BoardManager.height);

                if (!BoardManager.IsInBoard(new Vector2Int(x, y)) || BoardManager.IsBlocked(new Vector2Int(x, y)))
                    continue;

                // here we are making sure there are no matches after refilling
                List<Piece.PieceColor> possiblePieces = new List<Piece.PieceColor>();
                possiblePieces.AddRange(BoardManager.instance.spawnablePieces);
                possiblePieces.Remove(previousLeft[y]);
                possiblePieces.Remove(previousBelow);

                if (BoardManager.pieceBoard[x, y] == null)
                {
                    // this loop chooses randomly which piece to spawn, based on seed
                    int index = 0;
                    for (int i = 0; i < possiblePieces.Count; i++)
                    {
                        int randomValue = (int)(Random.value * possiblePieces.Count);
                        index = randomValue;
                    }

                    GameObject piece = Instantiate(BoardManager.instance.piecePrefab, pos, Quaternion.identity, transform);
                    Piece pieceScript = piece.GetComponent<Piece>();
                    pieceScript.color = possiblePieces[index];
                    pieceScript.indexInPieceBoard = new Vector2Int(x, y);
                    piece.name = pieceScript.color + "Piece";
                    BoardManager.pieceBoard[x, y] = pieceScript;

                    previousLeft[y] = possiblePieces[index];
                    previousBelow = possiblePieces[index];

                    StartCoroutine(MoveAnimation(pieceScript.transform, pos, pieceScript.indexInPieceBoard, 0, moveAnimationDuration));
                }
                else
                {
                    previousLeft[y] = BoardManager.pieceBoard[x, y].color;
                    previousBelow = BoardManager.pieceBoard[x, y].color;
                }
            }
        }
        yield return new WaitForSeconds(moveAnimationDuration);

        movingPieces = false;
    }

    // Check if player has available moves
    bool CheckForAvailableMoves()
    {
        for (int x = 0; x < BoardManager.width - 1; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                if (BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) || BoardManager.IsBlockedOrWebbed(new Vector2Int(x + 1, y)))
                    continue;
                if (!BoardManager.IsInBoard(new Vector2Int(x, y)) || !BoardManager.IsInBoard(new Vector2Int(x + 1, y)))
                    continue;

                // Temporarly swaps two pieces (without swaping their positions and doing any animations)
                // so that it can check if there is possible move that player could make
                TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);

                if (ResolveMatches(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]))
                {
                    TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);
                    return true;
                }
                else
                    TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);
            }
        }

        for (int y = 0; y < BoardManager.height - 1; y++)
        {
            for (int x = 0; x < BoardManager.width; x++)
            {
                if (BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) || BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y + 1)))
                    continue;
                if (!BoardManager.IsInBoard(new Vector2Int(x, y)) || !BoardManager.IsInBoard(new Vector2Int(x, y + 1)))
                    continue;

                TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
                if (ResolveMatches(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]))
                {
                    TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
                    return true;
                }
                else
                    TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
            }
        }

        Debug.Log("No available moves");
        return false;
    }

    // Reshuffle board by placing all pieces in a list and randomly putting them in available (not blocked) positions
    public IEnumerator ReshuffleBoard()
    {
        movingPieces = true;

        List<Piece> pieces = new List<Piece>();
        HashSet<Vector2Int> availablePositions = new HashSet<Vector2Int>();

        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                if (!BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) && BoardManager.IsInBoard(new Vector2Int(x, y)))
                {
                    pieces.Add(BoardManager.pieceBoard[x, y]);
                    availablePositions.Add(pieces[pieces.Count - 1].indexInPieceBoard);
                }
            }
        }

        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Vector2Int currentPos = new Vector2Int(x, y);
                if (availablePositions.Contains(currentPos))
                {
                    Piece piece = pieces[Random.Range(0, pieces.Count)];
                    pieces.Remove(piece);
                    availablePositions.Remove(currentPos);

                    piece.indexInPieceBoard = currentPos;
                    BoardManager.pieceBoard[x, y] = piece;

                    StartCoroutine(MoveAnimation(piece.transform, piece.transform.position, piece.indexInPieceBoard, 0, reshuffleAnimationDuration));
                }
            }
        }
        yield return new WaitForSeconds(reshuffleAnimationDuration);

        movingPieces = false;
    }

    #endregion

    #region Spell functions

    public void AddSpell(string target, string color, string type, string _object, int number, int layer)
    {
        if(type == "score")
        {
            spellScore += number;
            return;
        }
        if(type == "energy")
        {

            return;
        }

        string[] randomHostileObj = { "web", "inertPiece", "block", "pollution" };
        Vector2Int pos = new Vector2Int(Random.Range(0, BoardManager.width), Random.Range(0, BoardManager.height));
        HashSet<Vector2Int> usedPositions = new HashSet<Vector2Int>();

        if (_object == "random")
        {
            if (type == "piece")
            {
                Piece.PieceColor _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
                color = _color.ToString();
            }
            else if (type == "boost")
            {
                Piece.BoostType boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                Piece.PieceColor _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
                while (boost == Piece.BoostType.none || boost == Piece.BoostType.rainbow)
                    boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                _object = boost.ToString();
                color = _color.ToString();
            }
            else if (type == "hostileObj")
            {
                _object = randomHostileObj[Random.Range(0, randomHostileObj.Length)];
            }
        }

        for (int i = 0; i < number; i++)
        {
            while (usedPositions.Contains(pos))
            {
                pos = new Vector2Int(Random.Range(0, BoardManager.width), Random.Range(0, BoardManager.height));
            }
            usedPositions.Add(pos);

            if (type == "piece")
            {
                SpawnPiece(color, Piece.BoostType.none, pos, target);
            }
            else if (type == "boost")
            {
                SpawnPiece(color, Piece.StringToEnum<Piece.BoostType>(_object), pos, target);
            }
            else if (type == "hostileObj")
            {
                if (_object == "inertPiece")
                {
                    SpawnInertPiece(pos, target);
                }
                else if (_object == "web")
                {
                    SpawnWeb(pos, layer, target);
                }
                else if (_object == "block")
                {
                    SpawnBlock(pos, layer, target);
                }
                else if (_object == "pollution")
                {
                    PolluteTile(pos, layer);
                }
            }
        }
    }

    public void TriggerSpell(string target, string color, string type, string _object, int number, int layer)
    {
        Vector2Int pos = new Vector2Int(Random.Range(0, BoardManager.width), Random.Range(0, BoardManager.height));
        HashSet<Vector2Int> usedPositions = new HashSet<Vector2Int>();

        if (_object == "random")
        {
            if (type == "boost")
            {
                Piece.BoostType boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                Piece.PieceColor _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
                color = _color.ToString();
                while (boost == Piece.BoostType.none || boost == Piece.BoostType.rainbow)
                    boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                _object = boost.ToString();
            }
        }

        for (int i = 0; i < number; i++)
        {
            while (usedPositions.Contains(pos))
            {
                pos = new Vector2Int(Random.Range(0, BoardManager.width), Random.Range(0, BoardManager.height));
            }
            usedPositions.Add(pos);

            if (type == "boost")
            {
                SpawnPiece(color, Piece.StringToEnum<Piece.BoostType>(_object), pos, target);
                MarkPieceForRemoval(BoardManager.pieceBoard[pos.x, pos.y]);
            }
        }
    }

    public void RemoveSpell(string target, string color, string type, string _object, int number, int layer)
    {
        if(type == "score")
        {
            subSpellScore += number;
        }
        if(type == "energy")
        {
            spellController.RemoveEnergy(number);
        }

        string[] randomHostileObj = { "web", "inertPiece", "block", "pollution" };

        if (_object == "random")
        {
            if (type == "piece")
            {
                Piece.PieceColor _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
                color = _color.ToString();
            }
            else if (type == "boost")
            {
                Piece.BoostType boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                Piece.PieceColor _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
                color = _color.ToString();
                while (boost == Piece.BoostType.none || boost == Piece.BoostType.rainbow)
                    boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
                _object = boost.ToString();
            }
            else if (type == "hostileObj")
            {
                _object = randomHostileObj[Random.Range(0, randomHostileObj.Length)];
            }
        }

        for (int i = 0; i < number; i++)
        {
            if (type == "piece")
            {
                RemovePiece(color, Piece.BoostType.none, target);
            }
            else if (type == "boost")
            {
                RemovePiece(color, Piece.StringToEnum<Piece.BoostType>(_object), target);
            }
            else if (type == "hostileObj")
            {
                if (_object == "inertPiece")
                {
                    RemovePiece("inert", Piece.BoostType.none, target);
                }
                else if (_object == "web")
                {
                    RemoveWeb(layer);
                }
                else if (_object == "block")
                {
                    RemoveBlock(layer);
                }
                else if (_object == "pollution")
                {
                    RemovePollutionFromTile(layer);
                }
            }
        }
    }

    public void SpawnInertPiece(Vector2Int pos, string target)
    {
        SpawnPiece("inert", Piece.BoostType.none, pos, target);
    }

    public void SpawnBlock(Vector2Int pos, int layer, string target)
    {
        Block block = BoardManager.blockBoard[pos.x, pos.y];
        if (block != null)
        {
            block.blockLayers += layer;
        }
        else
        {
            Piece piece = BoardManager.pieceBoard[pos.x, pos.y];

            if (piece != null)
            {
                if(target == "enemy")
                {
                    subPieceScore += piece.score;
                }
                Destroy(piece.gameObject);
                BoardManager.pieceBoard[pos.x, pos.y] = null;
            }

            Vector2 posToSpawn = new Vector2(pos.x, BoardManager.height);
            GameObject spawnedBlock = Instantiate(BoardManager.instance.blockPrefab, posToSpawn, Quaternion.identity, transform);
            Block blockScript = spawnedBlock.GetComponent<Block>();
            blockScript.indexInPieceBoard = pos;
            BoardManager.blockBoard[pos.x, pos.y] = blockScript;
            blockScript.blockLayers = layer;
            StartCoroutine(MoveAnimation(blockScript.transform, posToSpawn, blockScript.indexInPieceBoard, 0, moveAnimationDuration));
        }
    }

    public void SpawnWeb(Vector2Int pos, int layer, string target)
    {
        Web web = (Web)BoardManager.blockBoard[pos.x, pos.y];
        if (web != null)
        {
            web.blockLayers += layer;
        }
        else
        {
            Piece piece = BoardManager.pieceBoard[pos.x, pos.y];

            if (piece != null)
            {
                if (target == "enemy")
                {
                    subPieceScore += piece.score;
                }
                Destroy(piece.gameObject);
                BoardManager.pieceBoard[pos.x, pos.y] = null;
            }

            Vector2 posToSpawn = new Vector2(pos.x, BoardManager.height);
            GameObject spawnedWeb = Instantiate(BoardManager.instance.webPrefab, posToSpawn, Quaternion.identity, transform);
            Block webScript = spawnedWeb.GetComponent<Web>();
            webScript.indexInPieceBoard = pos;
            BoardManager.blockBoard[pos.x, pos.y] = webScript;
            webScript.blockLayers = layer;
            StartCoroutine(MoveAnimation(webScript.transform, posToSpawn, webScript.indexInPieceBoard, 0, moveAnimationDuration));
        }
    }

    public void PolluteTile(Vector2Int pos, int layer)
    {
        Tile tile = BoardManager.tileBoard[pos.x, pos.y];
        tile.pollutionLayers += layer;
    }

    public void SpawnPiece(string color, Piece.BoostType boost, Vector2Int pos, string target)
    {
        if (BoardManager.pieceBoard[pos.x, pos.y] != null)
        {
            if (target == "enemy")
            {
                subPieceScore += BoardManager.pieceBoard[pos.x, pos.y].score;
            }
            Destroy(BoardManager.pieceBoard[pos.x, pos.y].gameObject);
        }
        Vector2 posToSpawn = new Vector2(pos.x, BoardManager.height);
        GameObject piece = Instantiate(BoardManager.instance.piecePrefab, posToSpawn, Quaternion.identity, transform);
        Piece pieceScript = piece.GetComponent<Piece>();
        pieceScript.color = Piece.StringToEnum<Piece.PieceColor>(color);
        pieceScript.Boost = boost;
        pieceScript.indexInPieceBoard = new Vector2Int(pos.x, pos.y);
        piece.name = pieceScript.color + "Piece";
        BoardManager.pieceBoard[pos.x, pos.y] = pieceScript;

        StartCoroutine(MoveAnimation(pieceScript.transform, posToSpawn, pieceScript.indexInPieceBoard, 0, moveAnimationDuration));
    }

    public void RemovePiece(string color, Piece.BoostType boost, string target)
    {
        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Piece piece = BoardManager.pieceBoard[x, y];
                if (piece != null && piece.color.ToString() == color && piece.Boost == boost)
                {
                    if(target == "enemy")
                    {
                        subPieceScore += piece.score;
                    }
                    Destroy(piece.gameObject);
                    BoardManager.pieceBoard[x, y] = null;
                    return;
                }
                else if (piece != null && piece.color == Piece.PieceColor.rainbow)
                {
                    if (target == "enemy")
                    {
                        subPieceScore += piece.score;
                    }
                    Destroy(piece.gameObject);
                    BoardManager.pieceBoard[x, y] = null;
                    return;
                }
            }
        }
    }

    public void RemoveBlock(int layer)
    {
        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Block block = BoardManager.blockBoard[x, y];
                if (block != null)
                {
                    block.blockLayers -= layer;
                    if (block.blockLayers <= 0)
                    {
                        Destroy(block.gameObject);
                        BoardManager.blockBoard[x, y] = null;
                    }
                    return;
                }
            }
        }
    }

    public void RemoveWeb(int layer)
    {
        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Web web = (Web)BoardManager.blockBoard[x, y];
                if (web != null)
                {
                    web.blockLayers -= layer;
                    if (web.blockLayers <= 0)
                    {
                        Destroy(web.gameObject);
                        BoardManager.blockBoard[x, y] = null;
                    }
                    return;
                }
            }
        }
    }

    public void RemovePollutionFromTile(int layer)
    {
        for (int x = 0; x < BoardManager.width; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                Tile tile = BoardManager.tileBoard[x, y];
                if (tile.pollutionLayers > 0)
                {
                    tile.pollutionLayers -= layer;
                    return;
                }
            }
        }
    }

    #endregion

    #region Boosts helper functions

    // Add boost pieces on game board
    void AddBoostPieces()
    {
        movingPieces = true;

        for (int i = 0; i < boostsToSpawn.Count; i++)
        {
            Vector2Int boostPosition = boostsToSpawn[i].indexInPieceBoard;
            int z = 0;
            while (!BoardManager.IsInBoard(boostPosition) || BoardManager.IsBlockedOrWebbed(boostPosition))
            {
                boostPosition.y--;
                if (boostPosition.y < 0)
                    boostPosition.y = BoardManager.height - 1;
                z++;
                if (z > BoardManager.height)
                    break;
            }
            if (z > BoardManager.height)
                break;

            if (BoardManager.pieceBoard[boostPosition.x, boostPosition.y] != null)
            {
                Destroy(BoardManager.pieceBoard[boostPosition.x, boostPosition.y].gameObject);
            }

            SpawnPiece(boostsToSpawn[i].color.ToString(), boostsToSpawn[i].boostType, boostPosition, "self");
        }

        boostsToSpawn.Clear();
    }

    // Determing boost pattern in given direction, returns number of pieces of the same color as hitPiece in given direction (number between 0-3)
    int DetermineBoostPattern(Piece hitPiece, Vector2Int direction)
    {
        int pattern = 0;
        for (int i = 1; i < 4; i++)
        {
            Vector2Int indexToCheck = hitPiece.indexInPieceBoard + direction * i;
            if (BoardManager.IsInBoard(indexToCheck) && !BoardManager.IsBlockedOrWebbed(indexToCheck))
            {
                Piece pieceToCheck = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];

                if (markedToRemove.Contains(pieceToCheck) && !usedHitList.Contains(indexToCheck) && pieceToCheck.color == hitPiece.color)
                {
                    pattern++;
                }
                else
                    break;
            }
            else
                break;
        }
        return pattern;
    }

    // Marks pieces as used when forming boost
    void MarkPiecesAsUsed(Piece hitPiece)
    {
        Vector2Int[] directions = { new Vector2Int(-1, 0), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(0, 1) };
        for (int i = 0; i < directions.Length; i++)
        {
            for (int j = 1; j < 3; j++)
            {
                Vector2Int indexToCheck = hitPiece.indexInPieceBoard + directions[i] * j;
                if (BoardManager.IsInBoard(indexToCheck) && !BoardManager.IsBlocked(indexToCheck))
                {
                    Piece pieceToCheck = BoardManager.pieceBoard[indexToCheck.x, indexToCheck.y];

                    if (pieceToCheck == null)
                        break;

                    if (!usedHitList.Contains(indexToCheck) && pieceToCheck.color == hitPiece.color)
                    {
                        usedHitList.Add(indexToCheck);
                    }
                    else
                        break;
                }
            }
        }
    }

    #endregion

    #region Other helper functions

    // Change pieces indexes in pieceBoard but don't change their locations
    public void TemporarySwap(Piece firstPiece, Piece secondPiece)
    {
        if (firstPiece == null || secondPiece == null)
            return;

        // Swap indexes in pieceBoard
        Vector2Int firstPieceIndex = firstPiece.indexInPieceBoard;
        firstPiece.indexInPieceBoard = secondPiece.indexInPieceBoard;
        secondPiece.indexInPieceBoard = firstPieceIndex;

        // Save pieces on correct indexes in piece board
        BoardManager.pieceBoard[firstPiece.indexInPieceBoard.x, firstPiece.indexInPieceBoard.y] = firstPiece;
        BoardManager.pieceBoard[secondPiece.indexInPieceBoard.x, secondPiece.indexInPieceBoard.y] = secondPiece;
    }

    // Function needed for AutoBoardAction() to stop refilling while pieces are moving
    bool IsMovingPieces()
    {
        return movingPieces;
    }

    // Coroutine that swaps positions of two pieces in swapAnimationDuration seconds
    IEnumerator MoveAnimation(Transform objectToSwap, Vector2 startPosition, Vector2 target, float time, float duration)
    {
        float t = time;
        t += Time.deltaTime / duration;
        objectToSwap.position = Vector2.Lerp(startPosition, target, t);
        yield return new WaitForEndOfFrame();
        if (objectToSwap == null)
            yield break;
        else if (Vector2.Distance(objectToSwap.transform.position, target) > 0.1f)
            StartCoroutine(MoveAnimation(objectToSwap, startPosition, target, t, duration));
        else
        {
            objectToSwap.transform.position = target;
            yield return null;
        }
    }

    #endregion

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
