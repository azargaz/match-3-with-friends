﻿using UnityEngine;
using System;

public class Piece : MonoBehaviour {

    public static T StringToEnum<T>(string str)
    {
        return (T)Enum.Parse(typeof(T), str);
    }

    public enum PieceColor
    {
        red,
        orange,
        yellow,
        green,
        purple,
        blue,
        rainbow,
        inert,
        none
    };
    public PieceColor color;

    public enum BoostType
    {
        none,
        rainbow,
        cross,
        around,
        column,
        row,
        oneTile
    };
    
    public BoostType Boost
    {
        get
        {
            return boost;
        }
        set
        {
            boost = value;
            UpdateProperties();
        }
    }

    [SerializeField]
    BoostType boost;

    public Vector2Int indexInPieceBoard;
    
    public int score;
    public float bonusRate;

    public int[] colorScores;
    public int[] boostScores;
    public Sprite[] sprites;
    public Sprite[] boostSprites;
    public SpriteRenderer mainSprite;
    public SpriteRenderer boostSprite;

    private void Start()
    {
        UpdateProperties();
    }
    
    void UpdateProperties()
    {
        mainSprite.sprite = sprites[(int)color];
        boostSprite.sprite = boostSprites[(int)Boost];
        score = colorScores[(int)color];
        if (Boost != BoostType.none)
            score = boostScores[(int)Boost];
    }
}
