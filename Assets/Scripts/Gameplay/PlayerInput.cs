﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using System;

public class PlayerInput : NetworkBehaviour
{
    // Assign this in the inspector
    [SerializeField]
    MatchController matchController;

    // Duration of swap animation
    [Range(0f, 1f)]
    public float swapAnimationDuration = 0.2f;
    float swapAnimationStopTime;

    // Duration of players turn
    [Range(0.1f, 5f)]
    public float playersTurnDuration = 2f;
    [HideInInspector]
    public float playersTurnStopTime;

    public int numberOfBlocksSent = 4;

    // Bool that tells wheter player took any action
    [HideInInspector]
    public bool playerMadeViableMove = false;

    // If busy don't read any player input
    [HideInInspector]
    public bool busy = false;

    // This is swap holder, that holds object you get after OnTouchBegin()
    [HideInInspector]
    public GameObject swapHolder;

    private void Start()
    {
        // Initiate players turn at the start of the game
        StartPlayersTurn();
    }

    private void FixedUpdate()
    {
        if (BoardManager.instance == null)
            return;

        if (!BoardManager.instance.gameStarted || (!isLocalPlayer && !BoardManager.instance.singleplayer))
            return;

        // If moving pieces or game is finished - set busy true to not read any inputs
        if (matchController.CurrentStep != Step.PlayerAction || BoardManager.instance.gameFinished)
            busy = true;

        // Count down players turn time
        if (playersTurnStopTime > 0 && !busy)
            playersTurnStopTime -= Time.deltaTime;

        // If player didn't take any action do autoswap
        if (!playerMadeViableMove && playersTurnStopTime <= 0 && !busy)
        {
            playerMadeViableMove = true;
            AutoSwap();
        }
    }

    private void Update()
    {
        if (BoardManager.instance == null)
            return;

        if (!BoardManager.instance.gameStarted || (!isLocalPlayer && !BoardManager.instance.singleplayer))
            return;

        // If busy or it's not players turn - don't read input
        if (busy || playersTurnStopTime <= 0)
            return;

        // Read touches
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                OnTouchBegan(Input.GetTouch(0));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                OnTouchEnded(Input.GetTouch(0));
            }
        }

        #region For testing purposes only
        // Pass mouse position as a Touch
        if (Input.GetMouseButtonDown(0))
        {
            OnTouchBegan(new Touch() { position = Input.mousePosition });
        }

        if (Input.GetMouseButtonUp(0))
        {
            OnTouchEnded(new Touch() { position = Input.mousePosition });
        }
        #endregion
    }

    public void StartPlayersTurn()
    {
        busy = false;
        playersTurnStopTime = playersTurnDuration;
        playerMadeViableMove = false;
    }

    #region Step 1: Player action

    void OnTouchBegan(Touch touch)
    {
        //Debug.Log("Touch Began.");

        // Get what we have touched by raycasting
        RaycastHit2D hit = new RaycastHit2D();
        hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);

        // Check if we hit something and it this something is a Piece
        if (hit && hit.transform.GetComponent<Piece>() != null)
        {
            // If swap holder is empty assign new piece to it
            if (swapHolder == null)
            {
                swapHolder = hit.transform.gameObject;
                //Debug.Log(swapHolder.name);
            }
        }
        else
            swapHolder = null;
    }

    void OnTouchEnded(Touch touch)
    {
        //Debug.Log("Touch Ended.");

        // Get what we have touched by raycasting
        RaycastHit2D hit = new RaycastHit2D();
        hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);

        // Check if we hit something and it this something is a Piece
        if (hit && hit.transform.GetComponent<Piece>() != null)
        {
            GameObject holder = hit.transform.gameObject;
            //Debug.Log(holder.name);

            // If both holders are not empty
            if (holder != null && swapHolder != null)
            {
                Piece holderPiece = holder.GetComponent<Piece>();
                Piece swapHolderPiece = swapHolder.GetComponent<Piece>();

                // If both pieces are the same piece return
                if (holderPiece == swapHolderPiece)
                    return;
                // Else swap pieces if they're swappable else clear swap holder and return
                else
                {
                    if (BoardManager.IsAdjacent(swapHolderPiece.indexInPieceBoard, holderPiece.indexInPieceBoard)
                        && !BoardManager.IsBlockedOrWebbed(swapHolderPiece.indexInPieceBoard)
                        && !BoardManager.IsBlockedOrWebbed(holderPiece.indexInPieceBoard))
                    {
                        StartCoroutine(SwapPieces(swapHolderPiece, holderPiece));
                    }
                    else
                    {
                        swapHolder = null;
                        return;
                    }
                }
            }
            // Else clear swap holder
            else
            {
                swapHolder = null;
                return;
            }
        }
        else
            swapHolder = null;
    }

    // Swap two pieces, this function basically calls other functions that do the work
    IEnumerator SwapPieces(Piece firstPiece, Piece secondPiece)
    {
        playerMadeViableMove = true;
        swapHolder = null;

        // Swap positions before Resolving Matches
        SwapPositionsAndIndexes(firstPiece, secondPiece);

        // This is to make sure player can't swap pieces during animation
        busy = true;
        yield return new WaitForSeconds(swapAnimationDuration);
        busy = false;

        // Process specialMatches like Rainbow + normal piece
        bool specialMatch = matchController.CheckForSpecialMatch(firstPiece, secondPiece);

        if (!specialMatch)
        {
            // Resolve Matches, if no match return pieces to their original positions
            if (!matchController.ResolveMatches(firstPiece, secondPiece))
            {
                SwapPositionsAndIndexes(firstPiece, secondPiece);

                //Debug.Log("Swap failed");
            }
        }

        // Go to step 2
        GoToNextStep(matchController.CurrentStep);
    }

    // Actually swaps two pieces
    void SwapPositionsAndIndexes(Piece firstPiece, Piece secondPiece)
    {
        // Swap indexes in pieceBoard
        Vector2Int firstPieceIndex = firstPiece.indexInPieceBoard;
        firstPiece.indexInPieceBoard = secondPiece.indexInPieceBoard;
        secondPiece.indexInPieceBoard = firstPieceIndex;

        // Save pieces on correct indexes in piece board
        BoardManager.pieceBoard[firstPiece.indexInPieceBoard.x, firstPiece.indexInPieceBoard.y] = firstPiece;
        BoardManager.pieceBoard[secondPiece.indexInPieceBoard.x, secondPiece.indexInPieceBoard.y] = secondPiece;

        // Swap using coroutines to make it look smooth
        StartCoroutine(SwapAnimation(firstPiece.transform, firstPiece.transform.position, firstPiece.indexInPieceBoard, 0));
        StartCoroutine(SwapAnimation(secondPiece.transform, secondPiece.transform.position, secondPiece.indexInPieceBoard, 0));
    }

    // Coroutine that swaps positions of two pieces in swapAnimationDuration seconds
    IEnumerator SwapAnimation(Transform objectToSwap, Vector2 startPosition, Vector2 target, float time)
    {
        float t = time;
        t += Time.deltaTime / swapAnimationDuration;
        objectToSwap.position = Vector2.Lerp(startPosition, target, t);
        yield return new WaitForEndOfFrame();
        if (objectToSwap == null)
            yield break;
        else if (Vector2.Distance(objectToSwap.transform.position, target) > 0.1f)
            StartCoroutine(SwapAnimation(objectToSwap, startPosition, target, t));
        else
        {
            objectToSwap.transform.position = target;
            yield return null;
        }
    }

    #endregion

    #region Spells

    // AutoSwap, is triggered when player did nothing and players turn is finished
    void AutoSwap()
    {
        Piece[] pieces = GetAvailableSwap();
        Piece firstPiece = pieces[0];
        Piece secondPiece = pieces[1];

        if (firstPiece != null && secondPiece != null)
        {
            StartCoroutine(SwapPieces(firstPiece, secondPiece));
        }
        else
        {
            AutoSwap();
        }
    }

    // Get two pieces that AutoSwap can use to make successful swap
    Piece[] GetAvailableSwap()
    {
        List<Vector2Int> availableHorizontalSwaps = new List<Vector2Int>();
        List<Vector2Int> availableVerticalSwaps = new List<Vector2Int>();

        for (int x = 0; x < BoardManager.width - 1; x++)
        {
            for (int y = 0; y < BoardManager.height; y++)
            {
                if (BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) || BoardManager.IsBlockedOrWebbed(new Vector2Int(x + 1, y)))
                    continue;
                if (!BoardManager.IsInBoard(new Vector2Int(x, y)) || !BoardManager.IsInBoard(new Vector2Int(x + 1, y)))
                    continue;

                // Temporarly swaps two pieces (without swaping their positions and doing any animations)
                // so that it can check if there is possible move that player could make
                matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);

                if (matchController.ResolveMatches(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]))
                {
                    matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);
                    availableHorizontalSwaps.Add(new Vector2Int(x, y));
                }
                else
                    matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x + 1, y]);
            }
        }

        for (int y = 0; y < BoardManager.height - 1; y++)
        {
            for (int x = 0; x < BoardManager.width; x++)
            {
                if (BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y)) || BoardManager.IsBlockedOrWebbed(new Vector2Int(x, y + 1)))
                    continue;
                if (!BoardManager.IsInBoard(new Vector2Int(x, y)) || !BoardManager.IsInBoard(new Vector2Int(x, y + 1)))
                    continue;

                matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
                if (matchController.ResolveMatches(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]))
                {
                    matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
                    availableVerticalSwaps.Add(new Vector2Int(x, y));
                }
                else
                    matchController.TemporarySwap(BoardManager.pieceBoard[x, y], BoardManager.pieceBoard[x, y + 1]);
            }
        }

        bool horizontal = UnityEngine.Random.value > 0.5f;

        if (availableHorizontalSwaps.Count <= 0)
            horizontal = false;
        else if (availableVerticalSwaps.Count <= 0)
            horizontal = true;
        else if (availableVerticalSwaps.Count <= 0 && availableHorizontalSwaps.Count <= 0)
        {
            matchController.ReshuffleBoard();
            return null;
        }

        Vector2Int randomIndex;
        if (horizontal)
            randomIndex = availableHorizontalSwaps[UnityEngine.Random.Range(0, availableHorizontalSwaps.Count)];
        else
            randomIndex = availableVerticalSwaps[UnityEngine.Random.Range(0, availableVerticalSwaps.Count)];

        Piece firstPiece = BoardManager.pieceBoard[randomIndex.x, randomIndex.y];
        Piece secondPiece = BoardManager.pieceBoard[randomIndex.x + (horizontal ? 1 : 0), randomIndex.y + (horizontal ? 0 : 1)];
        Piece[] returnedPieces = { firstPiece, secondPiece };
        return returnedPieces;
    }

    public void CastSpell(spellData.effect effect, bool castByPlayer)
    {
        if(castByPlayer && effect.type != "automatch")
        {
            GoToNextStep();
        }

        StartCoroutine(CastSpell(effect));
    }

    IEnumerator CastSpell(spellData.effect effect)
    {
        if (effect.type != "automatch")
            yield return new WaitUntil(IsStep3);
        if (effect.action == "standard")
        {
            StandardSpells(effect);
            yield break;
        }

        InfoSpell(effect);

        if (effect.target == "self")
        {
            if (effect.action == "add")
            {
                SelfAddSpell(effect);
                yield break;
            }
            else if (effect.action == "remove")
            {
                SelfRemoveSpell(effect);
                yield break;
            }
            else if (effect.action == "trigger")
            {
                SelfTriggerSpell(effect);
                yield break;
            }
        }
        else if (effect.target == "enemy")
        {
            if (effect.action == "add")
            {
                EnemyAddSpell(effect);
                yield break;
            }
            else if (effect.action == "remove")
            {
                EnemyRemoveSpell(effect);
                yield break;
            }
            else if (effect.action == "trigger")
            {
                EnemyTriggerSpell(effect);
                yield break;
            }
        }

        ErrorSpell(effect);
    }

    bool IsStep3()
    {
        return matchController.CurrentStep == Step.SpellsAndAutoBoardAction;
    }

    void ErrorSpell(spellData.effect effect)
    {
        string error = string.Format("Error! There is no spell effect with these properties: " +
            "target={0}, action={1}, type={2}, object={3}",
            effect.target, effect.action, effect.type, effect._object);
        Debug.LogError(error);
    }

    void InfoSpell(spellData.effect effect)
    {
        string info = string.Format("Info about spell being cast: " +
            "target={0}, action={1}, type={2}, object={3}",
            effect.target, effect.action, effect.type, effect._object);
        Debug.Log(info);
    }

    void StandardSpells(spellData.effect effect)
    {
        if (effect.type == "automatch")
        {
            AutoSwap();
        }
        else if (effect.type == "reshuffle")
        {
            StartCoroutine(matchController.ReshuffleBoard());
        }
        else
            ErrorSpell(effect);
    }

    void SelfAddSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "score":
                {
                    matchController.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
            case "energy":
                {
                    matchController.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
            case "piece":
                {
                    matchController.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
            case "boost":
                {
                    matchController.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
        }
    }
    
    void SelfRemoveSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "piece":
                {
                    matchController.RemoveSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
            case "boost":
                {
                    matchController.RemoveSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
            case "hostileObj":
                {
                    matchController.RemoveSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
        }
    }

    void SelfTriggerSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "boost":
                {
                    matchController.TriggerSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    break;
                }
        }
    }

    void EnemyAddSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "hostileObj":
                {
                    if (BoardManager.instance.singleplayer)
                    {
                        matchController.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
                    }
                    else
                    {
                        CmdAddSpell(BoardManager.instance.myId, effect);
                    }
                    break;
                }
        }
    }

    void EnemyRemoveSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "score":
                {
                    if (!BoardManager.instance.singleplayer)
                        CmdRemoveSpell(BoardManager.instance.myId, effect);
                    else
                        Opponent.instance.RemoveScore(effect.number);
                    break;
                }
            case "energy":
                {
                    if (!BoardManager.instance.singleplayer)
                        CmdRemoveSpell(BoardManager.instance.myId, effect);
                    else
                        Opponent.instance.RemoveEnergy(effect.number);
                    break;
                }
            case "piece":
                {
                    if (!BoardManager.instance.singleplayer)
                        CmdRemoveSpell(BoardManager.instance.myId, effect);
                    else
                        Debug.Log("Remove enemy pieces");
                    break;
                }
            case "boost":
                {
                    if (!BoardManager.instance.singleplayer)
                        CmdRemoveSpell(BoardManager.instance.myId, effect);
                    else
                        Debug.Log("Remove enemy boosts");
                    break;
                }
        }
    }

    void EnemyTriggerSpell(spellData.effect effect)
    {
        switch (effect.type)
        {
            default:
                {
                    ErrorSpell(effect);
                    break;
                }
            case "boost":
                {
                    Debug.Log("Spell effect not implmented");
                    break;
                }
        }
    }

    #endregion

    #region Network functions

    [Command]
    public void CmdAddSpell(int id, spellData.effect effect)
    {
        RpcAddSpell(id, effect);
    }
    [ClientRpc]
    void RpcAddSpell(int id, spellData.effect effect)
    {
        if (BoardManager.instance.myId != id)
        {
            MatchController.instance.AddSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
        }
    }

    [Command]
    public void CmdRemoveSpell(int id, spellData.effect effect)
    {
        RpcRemoveSpell(id, effect);
    }
    [ClientRpc]
    void RpcRemoveSpell(int id, spellData.effect effect)
    {
        if (BoardManager.instance.myId != id)
        {
            MatchController.instance.RemoveSpell(effect.target, effect.color, effect.type, effect._object, effect.number, effect.layer);
        }
    }

    [Command]
    public void CmdGoToNextStep(int id, Step currentStep)
    {
        Step goToStep = Step.PlayerAction;

        switch (currentStep)
        {
            case Step.PlayerAction:
                {
                    goToStep = Step.ResolveMatches;
                    break;
                }
            case Step.ResolveMatches:
                {
                    goToStep = Step.SpellsAndAutoBoardAction;
                    break;
                }
            case Step.SpellsAndAutoBoardAction:
                {
                    goToStep = Step.ResolveChainMatches;
                    break;
                }
            case Step.ResolveChainMatches:
                {
                    goToStep = Step.PlayerAction;
                    break;
                }
        }

        RpcGoToNextStep(id, goToStep);
    }
    [ClientRpc]
    void RpcGoToNextStep(int id, Step nextStep)
    {
        if(BoardManager.instance != null && BoardManager.instance.myId == id)
        {
            matchController.CurrentStep = nextStep;
        }
    }

    #endregion

    public void GoToNextStep()
    {
        GoToNextStep(matchController.CurrentStep);
    }

    public void GoToNextStep(Step currentStep)
    {
        if (!BoardManager.instance.singleplayer)
        {
            CmdGoToNextStep(BoardManager.instance.myId, matchController.CurrentStep);
            return;
        }

        Step goToStep = Step.PlayerAction;

        switch (currentStep)
        {
            case Step.PlayerAction:
                {
                    goToStep = Step.ResolveMatches;
                    break;
                }
            case Step.ResolveMatches:
                {
                    goToStep = Step.SpellsAndAutoBoardAction;
                    break;
                }
            case Step.SpellsAndAutoBoardAction:
                {
                    goToStep = Step.ResolveChainMatches;
                    break;
                }
            case Step.ResolveChainMatches:
                {
                    goToStep = Step.PlayerAction;
                    break;
                }
        }

        matchController.CurrentStep = goToStep;        
    }
}
