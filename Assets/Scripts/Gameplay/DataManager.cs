﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using Barebones.MasterServer;

#region Battle results classes

[System.Serializable]
public class PieceInfo
{
    public Piece.PieceColor color;
    public int piecesRemoved;
}

[System.Serializable]
public class BoostInfo
{
    public Piece.BoostType boostType;
    public int boostsUsed;
}

[System.Serializable]
public class SpellInfo
{
    public string spellType;
    public int spellsCast;
}

[System.Serializable]
public class BattleResults
{
    public int allPiecesRemoved;
    public int allBoostsUsed;
    public int allSpellsCast;
    public PieceInfo[] piecesOfColorRemoved;
    public BoostInfo[] boostsUsed;
    public SpellInfo[] spellsCast;

    public void UpdateResults(Piece.PieceColor color)
    {
        for (int i = 0; i < piecesOfColorRemoved.Length; i++)
        {
            if (color == piecesOfColorRemoved[i].color)
            {
                piecesOfColorRemoved[i].piecesRemoved++;
                allPiecesRemoved++;
            }
        }
    }

    public void UpdateResults(Piece.BoostType boostType)
    {
        for (int i = 0; i < boostsUsed.Length; i++)
        {
            if (boostType == boostsUsed[i].boostType)
            {
                boostsUsed[i].boostsUsed++;
                allBoostsUsed++;
            }
        }
    }

    public void UpdateResults(string spellType)
    {
        for (int i = 0; i < spellsCast.Length; i++)
        {
            if (spellType == spellsCast[i].spellType)
            {
                spellsCast[i].spellsCast++;
                allSpellsCast++;
            }
        }
    }
}

#endregion

public class DataManager : NetworkBehaviour
{
    [HideInInspector]
    public BattleResults gameBattleResults;

    [Header("BattleResults - don't change this unless you know what you're doing.")]
    [SerializeField]
    BattleResults battleResults;
    
    public int[] scores = new int[2];

    public int playerScore = 0;
    public int enemyScore = 0;

    [HideInInspector]
    public bool savedData = false;

    public static DataManager instance;

    //string gameDataFileName = "data.json";

    private void Awake()
    {
        instance = this;
        gameBattleResults = battleResults;
    }

    public void SaveBattleResults()
    {
        if (savedData)
            return;

        SaveBattleResultsToProfile();
        //string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);

        //if (File.Exists(filePath))
        //{
        //    // Get json file and read it as BattleResults class
        //    string dataAsJson = File.ReadAllText(filePath);
        //    BattleResults loadedData = JsonUtility.FromJson<BattleResults>(dataAsJson);

        //    // Add pieces removed to saved data
        //    loadedData.allPiecesRemoved += gameBattleResults.allPiecesRemoved;

        //    for (int i = 0; i < gameBattleResults.piecesOfColorRemoved.Length; i++)
        //    {
        //        for (int j = 0; j < loadedData.piecesOfColorRemoved.Length; j++)
        //        {
        //            if (gameBattleResults.piecesOfColorRemoved[i].color == loadedData.piecesOfColorRemoved[j].color)
        //            {
        //                gameBattleResults.piecesOfColorRemoved[i].piecesRemoved += loadedData.piecesOfColorRemoved[j].piecesRemoved;
        //            }
        //        }
        //    }
        //    // Add boosts used to saved data
        //    for (int i = 0; i < gameBattleResults.boostsUsed.Length; i++)
        //    {
        //        for (int j = 0; j < loadedData.boostsUsed.Length; j++)
        //        {
        //            if (gameBattleResults.boostsUsed[i].boostType == loadedData.boostsUsed[j].boostType)
        //            {
        //                gameBattleResults.boostsUsed[i].boostsUsed += loadedData.boostsUsed[j].boostsUsed;
        //            }
        //        }
        //    }
        //    // Add spelss cast to saved data
        //    for (int i = 0; i < gameBattleResults.spellsCast.Length; i++)
        //    {
        //        for (int j = 0; j < loadedData.spellsCast.Length; j++)
        //        {
        //            if (gameBattleResults.spellsCast[i].spellType == loadedData.spellsCast[j].spellType)
        //            {
        //                gameBattleResults.spellsCast[i].spellsCast += loadedData.spellsCast[j].spellsCast;
        //            }
        //        }
        //    }
        //}

        //string _dataAsJson = JsonUtility.ToJson(gameBattleResults);
        //File.WriteAllText(filePath, _dataAsJson);
    }

    void ListenToProfileChanges()
    {
        // Construct the profile
        var profile = new ObservableProfile()
        {
            new ObservableDictStringInt(MyProfileKeys.PiecesRemoved),
            new ObservableDictStringInt(MyProfileKeys.BoostsUsed),
            new ObservableDictStringInt(MyProfileKeys.SpellsCast),
            new ObservableInt(MyProfileKeys.Highscore),
            new ObservableInt(MyProfileKeys.OverallScore)
        };
        
        // Fill profile values
        Msf.Client.Profiles.GetProfileValues(profile, (successful, error) =>
        {
            if (!successful)
            {
                Logs.Error(error);
                return;
            }

            // Listen to property updates
            profile.PropertyUpdated += (code, property) =>
            {
                // Log a message, when property changes
                Logs.Info("Property changed:" + code + " - " + property.SerializeToString());
            };
        });
    }

    public void SaveBattleResultsToProfile()
    {
        savedData = true;
        if (BoardManager.instance.singleplayer)
        {
            Debug.LogWarning("Need to implement: save player data into profile in singleplayer mode.");
            return;
        }

        NetworkFunctions.instance.UpdateProfile(gameBattleResults, playerScore, Msf.Client.Auth.AccountInfo.Username);
    }

    public void LoadBattleResults()
    {
        //string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);

        //if (File.Exists(filePath))
        //{
        //    string dataAsJson = File.ReadAllText(filePath);
        //    BattleResults loadedData = JsonUtility.FromJson<BattleResults>(dataAsJson);
        //    gameBattleResults = loadedData;
        //}
        //else
        //{
        //    gameBattleResults = battleResults;
        //}
    }

    public void UpdateScore(int addedScore)
    {
        playerScore += addedScore;
        if(BoardManager.instance.myId != 0)
        {
            NetworkFunctions.instance.CmdUpdateScore(BoardManager.instance.myId - 1, playerScore);
        }
    }

    public void UpdateEnemyScore()
    {
        if (BoardManager.instance.myId == 2)
            enemyScore = scores[0];
        else
            enemyScore = scores[1];
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
