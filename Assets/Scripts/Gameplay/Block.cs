﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    public int blockLayers = 0;

    public int score;
    public float bonusRate = 0.2f;

    public Vector2Int indexInPieceBoard;
}
