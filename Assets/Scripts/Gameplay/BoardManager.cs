﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Barebones.Networking;
using Barebones.MasterServer;
using System.Collections;

public class BoardManager : NetworkBehaviour
{
    #region Variables

    public bool singleplayer = false;
    public bool randomBoard = true;

    // Size of gameboard, can only be assigned in code, if needed it can be changed so that you can type size in the inspector
    public static int width = 9;
    public static int height = 9;

    public float gameDuration = 180f;
    //[HideInInspector]
    [SyncVar]
    public float gameTimeLeft;
    [HideInInspector]
    public bool gameFinished
    {
        get {
            return _gameFinished;
        }
        set {
            _gameFinished = value;
            GameFinished();
        }
    }
    bool _gameFinished = false;

    public int winningCondition = 1000;
    [HideInInspector]
    public bool isWinner = false;
    [HideInInspector]
    public int winnerId = 0;
    [HideInInspector]
    public bool draw = false;

    // Count down before stat of the game
    public float timeBeforeStartOfTheGame = 3f;
    //[HideInInspector]
    [SyncVar]
    public float timeLeftToStart;
    [HideInInspector]
    public bool gameStarted = false;

    public float timeBetweenRandomEvents = 30f;
    float timeBeforeNextRandomEvent = 0;

    // For testing
    public bool generateNewSeed = true;
    public bool setupBoardCanSpawnMatches = false;
    //public bool spawnBlocksAndNullTiles = false;

    // 3 Layers of gameboard
    public static Tile[,] tileBoard;
    public static Piece[,] pieceBoard;
    public static Block[,] blockBoard;

    [Header("Prefabs")]
    // Assign all of these variables in the inspector
    public Piece.PieceColor[] spawnablePieces;
    public GameObject tilePrefab;
    public GameObject piecePrefab;
    public GameObject blockPrefab;
    public GameObject webPrefab;

    [Header("Other")]
    public int seed;

    [System.Serializable]
    public class RandomEvent
    {
        public float chanceToHappen;
        public spellData effect;
    }
    public RandomEvent[] randomEvents;

    public NetworkFunctions networkFunctions;
    public static BoardManager instance;
    StartGameplayComponents startGameplayComponents;

    [HideInInspector]
    public int myId = 0;

    #endregion

    #region Server side variables

    [HideInInspector]
    public bool gameReady;
    GameServer gameServer;
    [HideInInspector]
    public int playersReady = 0;

    float timeBetweenUpdates = 10f;
    float timeLeftToUpdate = 0f;

    #endregion

    public override void OnStartLocalPlayer()
    {
        #region Multiplayer
        if (instance == null)
        {
            gameObject.name = "BoardManager(Local)";
            instance = this;
            tileBoard = new Tile[width, height];
            pieceBoard = new Piece[width, height];
            blockBoard = new Block[width, height];
            startGameplayComponents = FindObjectOfType<StartGameplayComponents>();
            startGameplayComponents.GameSetup += SetupGame;
        }
        #endregion
    }

    void Awake()
    {
        #region Singleplayer
        if (singleplayer)
        {
            instance = this;

            tileBoard = new Tile[width, height];
            pieceBoard = new Piece[width, height];
            blockBoard = new Block[width, height];
            gameTimeLeft = gameDuration;
            timeLeftToStart = timeBeforeStartOfTheGame;
            timeBeforeNextRandomEvent = timeBetweenRandomEvents;

            startGameplayComponents = FindObjectOfType<StartGameplayComponents>();
            startGameplayComponents.GameSetup += SetupGame;
        }
        #endregion
    }

    void Start()
    {
        #region Server
        if (isServer)
        {
            gameServer = FindObjectOfType<GameServer>();
            if (instance == null)
                instance = this;
            gameTimeLeft = gameDuration;
            timeLeftToStart = timeBeforeStartOfTheGame;
            timeBeforeNextRandomEvent = timeBetweenRandomEvents;
        }
        #endregion
    }

    void SetupGame()
    {
        #region Multiplayer
        if (!singleplayer)
        {
            networkFunctions.CmdPlayerReady();

            myId = (int)netId.Value;
            if (generateNewSeed)
                networkFunctions.CmdGenerateSeed(myId);
            else
                SetupRandomBoard();
        }
        #endregion

        #region Singleplayer
        if (singleplayer)
        {
            Opponent opponent = FindObjectOfType<Opponent>();
            if (opponent != null)
            {
                opponent.StartGame();
            }

            if (randomBoard)
            {
                seed = (int)System.DateTime.Now.Ticks + Random.Range(0, 1000000);
                SetupRandomBoard();
            }
            else
            {
                SetupBoardFromMapData();
            }
        }
        #endregion
    }

    void FixedUpdate()
    {
        #region Multiplayer

        if (!singleplayer)
        {
            if (isServer)
            {
                if (timeLeftToUpdate <= 0 && !gameFinished)
                {
                    networkFunctions.RpcUpdateTimeLeft(gameTimeLeft, timeLeftToStart);
                    timeLeftToUpdate = timeBetweenUpdates;
                }
                else
                    timeLeftToUpdate -= Time.deltaTime;

                if (playersReady < 2)
                    return;

                if (!gameReady)
                {
                    gameReady = gameServer.gameServerState == GameServerState.GameStarted;

                    if (gameReady)
                        networkFunctions.RpcSetupFinished();
                    return;
                }

                if (!gameStarted)
                    timeLeftToStart -= Time.deltaTime;
                else if (!gameFinished && gameStarted)
                {
                    gameTimeLeft -= Time.deltaTime;
                    timeBeforeNextRandomEvent -= Time.deltaTime;

                    if (timeBeforeNextRandomEvent <= 0)
                    {
                        networkFunctions.RpcTriggerRandomEvent();
                        timeBeforeNextRandomEvent = timeBetweenRandomEvents;
                    }

                    if (gameTimeLeft <= 0 && MatchController.instance.CurrentStep == Step.PlayerAction)
                    {
                        gameFinished = true;
                        networkFunctions.RpcFinishGame();
                    }
                }

                if (timeLeftToStart <= 0)
                {
                    gameStarted = true;
                    networkFunctions.RpcStartGame();
                }
            }
            else
            {
                if (!gameReady)
                    return;

                if (!gameStarted && timeLeftToStart > 0)
                {
                    timeLeftToStart -= Time.deltaTime;
                    return;
                }

                if (gameTimeLeft > 0 && !gameFinished)
                    gameTimeLeft -= Time.deltaTime;
            }
        }

        #endregion

        #region Singleplayer
        if (singleplayer)
        {
            if (timeLeftToStart <= 0)
                gameStarted = true;

            if (!gameStarted)
                timeLeftToStart -= Time.deltaTime;
            else if (!gameFinished && gameStarted)
            {
                gameTimeLeft -= Time.deltaTime;
                timeBeforeNextRandomEvent -= Time.deltaTime;

                if (timeBeforeNextRandomEvent <= 0)
                {
                    StartCoroutine(TriggerRandomEvent());
                    timeBeforeNextRandomEvent = timeBetweenRandomEvents;
                }

                CheckWinningConditions();

                if (gameTimeLeft <= 0 && MatchController.instance.CurrentStep == Step.PlayerAction)
                {
                    gameFinished = true;
                }
            }
        }
        #endregion
    }

    // For multiplayer or practice
    public void SetupRandomBoard()
    {
        // Setup camera at the center of the board
        SetupCamera();

        // Use seed for Random functions
        Random.InitState(seed);

        // these two are necessary to check if we spawned piece in a match, which we don't want at the start of the game
        Piece.PieceColor[] previousLeft = new Piece.PieceColor[height];
        Piece.PieceColor previousBelow = Piece.PieceColor.inert;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                #region Spawning Tiles

                Vector2 pos = new Vector2(x, y);
                GameObject tile = Instantiate(tilePrefab, pos, Quaternion.identity, transform);
                Tile tileScript = tile.GetComponent<Tile>();
                tileScript.indexInPieceBoard = new Vector2Int(x, y);
                tileBoard[x, y] = tileScript;

                #endregion

                #region Spawning Pieces

                // here we are making sure there are no matches at the start of the game
                List<Piece.PieceColor> possiblePieces = new List<Piece.PieceColor>();
                possiblePieces.AddRange(spawnablePieces);

                if (!setupBoardCanSpawnMatches)
                {
                    possiblePieces.Remove(previousLeft[y]);
                    possiblePieces.Remove(previousBelow);
                }

                // this loop chooses randomly which piece to spawn, based on seed
                int index = 0;
                for (int i = 0; i < possiblePieces.Count; i++)
                {
                    int randomValue = (int)(Random.value * possiblePieces.Count);
                    index = randomValue;
                }
                GameObject piece = Instantiate(piecePrefab, pos, Quaternion.identity, transform);
                Piece pieceScript = piece.GetComponent<Piece>();
                pieceScript.color = possiblePieces[index];
                pieceScript.indexInPieceBoard = new Vector2Int(x, y);
                piece.name = pieceScript.color + "Piece";
                pieceBoard[x, y] = pieceScript;

                previousLeft[y] = possiblePieces[index];
                previousBelow = possiblePieces[index];
                #endregion
            }
        }
    }

    // For singleplayer
    public void SetupBoardFromMapData()
    {
        SetupCamera();
        SetupRandomEvents();
        SetupSpells();

        winningCondition = DataLoader.instance.chosenLevel.targetScore;

        levelMapData level = DataLoader.instance.chosenLevel.levelMapData;
        HashSet<Vector2Int> usedPositions = new HashSet<Vector2Int>();

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                #region Spawning Tiles

                Vector2 pos = new Vector2(x, y);
                GameObject tile = Instantiate(tilePrefab, pos, Quaternion.identity, transform);
                Tile tileScript = tile.GetComponent<Tile>();
                tileScript.indexInPieceBoard = new Vector2Int(x, y);
                tileBoard[x, y] = tileScript;

                #endregion
            }
        }

        for (int i = 0; i < level.positionedElements.Length; i++)
        {
            levelMapData.element elementToSpawn = level.positionedElements[i];
            for (int j = 0; j < level.positionedElements[i].posList.Length; j++)
            {
                Vector2Int pos = new Vector2Int(elementToSpawn.posList[j].xPos, elementToSpawn.posList[j].yPos);

                if (elementToSpawn.elementType == "piece" || elementToSpawn.elementType == "boost")
                {
                    SpawnPiece(elementToSpawn.name, elementToSpawn.posList[j].color,
                        elementToSpawn.elementType == "piece" ? "none" : elementToSpawn.boostType, pos);
                    usedPositions.Add(pos);
                }
                else if (elementToSpawn.elementType == "hostileObj")
                {
                    if (elementToSpawn.name == "block" || elementToSpawn.name == "web")
                    {
                        SpawnHostileObj(elementToSpawn.name, elementToSpawn.posList[j].layer, pos);
                    }
                    else if (elementToSpawn.name == "inertPiece")
                    {
                        SpawnPiece(elementToSpawn.name, "inert",
                        "none", pos);
                    }
                    usedPositions.Add(pos);
                }
                else if (elementToSpawn.elementType == "nullTile")
                {
                    Tile tile = tileBoard[pos.x, pos.y];
                    tile.pollutionLayers = -5;
                    usedPositions.Add(pos);
                }
            }
        }

        for (int i = 0; i < level.randomElements.Length; i++)
        {
            levelMapData.randomElement elementToSpawn = level.randomElements[i];

            Vector2Int randomPos = Vector2Int.zero;
            for (int j = 0; j < elementToSpawn.quantity; j++)
            {
                int countLoops = 0;
                bool spawn = true;

                while (usedPositions.Contains(randomPos))
                {
                    randomPos = new Vector2Int(Random.Range(0, width), Random.Range(0, height));
                    if (countLoops > width * height + 10)
                    {
                        spawn = false;
                        break;
                    }
                    countLoops++;
                }
                usedPositions.Add(randomPos);

                if (!spawn)
                    break;

                if (elementToSpawn.elementType == "piece" || elementToSpawn.elementType == "boost")
                {
                    SpawnPiece(elementToSpawn.name, elementToSpawn.color, elementToSpawn.boostType, randomPos);
                }
                else if (elementToSpawn.elementType == "hostileObj")
                {
                    if (elementToSpawn.name == "block" || elementToSpawn.name == "web")
                    {
                        SpawnHostileObj(elementToSpawn.name, elementToSpawn.layers, randomPos);
                    }
                    else if (elementToSpawn.name == "inertPiece")
                    {
                        SpawnPiece(elementToSpawn.name, "inert",
                        "none", randomPos);
                    }
                }
                else if (elementToSpawn.elementType == "nullTile")
                {
                    tileBoard[randomPos.x, randomPos.y].pollutionLayers = -5;
                }
            }
        }

        if (usedPositions.Count < width * height)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Vector2Int pos = new Vector2Int(x, y);
                    if (IsInBoard(pos) && !IsBlockedOrWebbed(pos) && pieceBoard[pos.x, pos.y] == null)
                    {
                        SpawnPiece("randomPiece", "random", "none", pos);
                    }
                }
            }
        }
    }

    #region Setup functions

    void SpawnPiece(string name, string color, string boost, Vector2Int pos)
    {
        if (!IsInBoard(pos))
            return;

        Piece.PieceColor _color;
        Piece.BoostType _boost;
        if (color == "random")
        {
            _color = (Piece.PieceColor)Random.Range(0, (int)Piece.PieceColor.rainbow);
        }
        else
            _color = Piece.StringToEnum<Piece.PieceColor>(color);

        if (boost == "random")
        {
            do
            {
                _boost = (Piece.BoostType)Random.Range(0, (int)Piece.BoostType.oneTile + 1);
            }
            while (_boost == Piece.BoostType.rainbow || _boost == Piece.BoostType.none);
        }
        else
            _boost = Piece.StringToEnum<Piece.BoostType>(boost);

        GameObject piece = Instantiate(piecePrefab, (Vector2)pos, Quaternion.identity, transform);
        Piece pieceScript = piece.GetComponent<Piece>();
        pieceScript.color = _color;
        pieceScript.Boost = _boost;
        pieceScript.indexInPieceBoard = pos;
        piece.name = name;
        pieceBoard[pos.x, pos.y] = pieceScript;
    }

    void SpawnHostileObj(string name, int layers, Vector2Int pos)
    {
        if (name != "block" && name != "web")
        {
            Debug.LogError("Error! Can't spawn hostileObj: " + name);
            return;
        }

        GameObject obj = Instantiate(name == "block" ? blockPrefab : webPrefab, (Vector2)pos, Quaternion.identity, transform);
        Block script = obj.GetComponent<Block>();
        script.blockLayers = layers;
        script.indexInPieceBoard = pos;
        blockBoard[pos.x, pos.y] = script;
    }

    void SetupRandomEvents()
    {
        timeBetweenRandomEvents = DataLoader.instance.chosenLevel.randomEventInterval;
        timeBeforeNextRandomEvent = timeBetweenRandomEvents;
        levelData.spell[] loadedRandomEvents = DataLoader.instance.chosenLevel.randomEventList;
        spellData[] spells = DataLoader.instance.spells;
        randomEvents = new RandomEvent[loadedRandomEvents.Length];

        int k = 0;
        for (int i = 0; i < loadedRandomEvents.Length; i++)
        {
            for (int j = 0; j < spells.Length; j++)
            {
                if (loadedRandomEvents[i].spellName == spells[j].name)
                {
                    RandomEvent newEvent = new RandomEvent()
                    {
                        chanceToHappen = loadedRandomEvents[i].spellChance,
                        effect = spells[j]
                    };
                    randomEvents[k] = newEvent;
                    k++;
                    break;
                }
            }
        }
    }

    void SetupSpells()
    {
        SpellController.instance.spells = new SpellController.Spell[DataLoader.instance.chosenMaterials.Count];
        for (int i = 0; i < DataLoader.instance.chosenMaterials.Count; i++)
        {
            materialData material = DataLoader.instance.chosenMaterials[i];
            spellData materialsSpell = DataLoader.instance.spells[material.spellAN];
            SpellController.Spell spell = new SpellController.Spell()
            {
                currentEnergy = 0,
                data = materialsSpell
            };
            SpellController.instance.spells[i] = spell;
        }
    }

    void SetupCamera()
    {
        Camera.main.transform.position = new Vector3(width * .5f - .5f, height * .5f - .5f, -100);
    }

    #endregion

    // Check if one of players won or if there is a draw
    public void CheckWinningConditions()
    {
        int playerScore = DataManager.instance.playerScore;
        if (!singleplayer)
        {
            int enemyScore = DataManager.instance.enemyScore;

            // If there is still time left, check if either of players won, end game and check who is winner of if there is draw
            if (!gameFinished)
            {
                if (playerScore >= winningCondition || enemyScore >= winningCondition)
                {
                    gameFinished = true;

                    if (playerScore > enemyScore)
                    {
                        isWinner = true;
                        winnerId = myId;
                    }
                    else if (playerScore == enemyScore)
                        draw = true;

                    networkFunctions.CmdSetWinner(winnerId, draw);
                }
            }
            // If game has finished without anyone reaching winning condition, check who is winner by comparing scores
            else
            {
                isWinner = playerScore > enemyScore;
                draw = playerScore == enemyScore;
                if (isWinner)
                    winnerId = myId;
                networkFunctions.CmdSetWinner(winnerId, draw);
            }
        }
        //else
        //{
        //    if (playerScore >= winningCondition)
        //    {
        //        isWinner = true;
        //        gameFinished = true;
        //    }
        //}
    }

    // Called after game is finished
    void GameFinished()
    {
        UIManager.ui.DisplayBattleResults();
        DataManager.instance.SaveBattleResults();
        DataLoader.instance.SetRewards(DataManager.instance.playerScore);
    }

    // Random event that happens every 30 seconds
    public IEnumerator TriggerRandomEvent()
    {
        if (randomEvents.Length <= 0)
            yield break;
        
        #region Choose randomly event

        RandomEvent eventToTrigger = randomEvents[0];
        float sumOfChances = 0;
        for (int i = 0; i < randomEvents.Length; i++)
        {
            sumOfChances += randomEvents[i].chanceToHappen;
        }
        float random = Random.Range(0f, sumOfChances);
        for (int i = randomEvents.Length - 1; i >= 0; i--)
        {
            if (random >= sumOfChances - randomEvents[i].chanceToHappen)
            {
                eventToTrigger = randomEvents[i];
                break;
            }
            else
                sumOfChances -= randomEvents[i].chanceToHappen;
        }

        #endregion

        // Cast spell of chosen random event
        SpellController.instance.RandomEventCastSpell(eventToTrigger.effect);
    }

    // For testing
    public void ClearBoard()
    {
        tileBoard = new Tile[width, height];
        pieceBoard = new Piece[width, height];
        blockBoard = new Block[width, height];

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    #region Helper static functions

    // Check if two pieces can be swapped
    public static bool IsAdjacent(Vector2Int firstPieceIndex, Vector2Int secondPieceIndex)
    {
        // Array of adjacent positions which we check in loop below
        Vector2Int[] adjacentPositions = { new Vector2Int(-1, 0), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(0, 1) };

        for (int i = 0; i < adjacentPositions.Length; i++)
        {
            Vector2Int checkedPosition = firstPieceIndex + adjacentPositions[i];

            if (IsInBoard(checkedPosition))
            {
                if (checkedPosition == secondPieceIndex)
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Check if there is block
    public static bool IsBlocked(Vector2Int pieceIndex)
    {
        if (!IsInBoard(pieceIndex))
            return true;
        if (blockBoard[pieceIndex.x, pieceIndex.y] != null && !IsWebbed(pieceIndex))
            return true;
        else
            return false;
    }

    public static bool IsWebbed(Vector2Int pieceIndex)
    {
        Block block = blockBoard[pieceIndex.x, pieceIndex.y];
        if (block == null)
            return false;
        else if (block.GetType() == typeof(Web))
            return true;

        return false;
    }

    public static bool IsBlockedOrWebbed(Vector2Int pieceIndex)
    {
        if (!IsInBoard(pieceIndex))
            return true;
        if (blockBoard[pieceIndex.x, pieceIndex.y])
            return true;
        else
            return false;
    }

    // Check if given position is inside board, or if tile is null
    public static bool IsInBoard(Vector2Int pieceIndex)
    {
        if (pieceIndex.x < 0 || pieceIndex.y < 0 || pieceIndex.x >= width || pieceIndex.y >= height)
            return false;
        else
        {
            if (tileBoard[pieceIndex.x, pieceIndex.y].pollutionLayers == -5)
                return false;
            else
                return true;
        }
    }

    #endregion

    private void OnDestroy()
    {
        if (instance == this)
            instance = null;
    }
}
