﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opponent : MonoBehaviour
{
    public opponentData opponent;
    public float enemyTurnDuaration = 2.5f;
    float enemyTurnTimeLeft = 0f;
    float currentRaiseRate = 0f;

    int currentEnergy;
    int opponentScore;

    bool gameStarted = false;

    public static Opponent instance;

    void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        opponent = DataLoader.instance.chosenOpponent;
        gameStarted = true;
        enemyTurnTimeLeft = enemyTurnDuaration;
    }

	void FixedUpdate()
    {
        if (!gameStarted)
            return;

        enemyTurnTimeLeft -= Time.deltaTime;
        if(enemyTurnTimeLeft <= 0)
        {
            enemyTurnTimeLeft = enemyTurnDuaration;
            NewTurn();
        }
	}

    public void NewTurn()
    {
        float currentScoreRate = opponent.scoreRate * (1f + currentRaiseRate);
        int scoreGenerated = Mathf.RoundToInt(Mathf.RoundToInt(currentScoreRate * (1f + Random.Range(-0.5f, 2f)) * 0.2f) / 0.2f);
        float currentEnergyRate = opponent.energyRate * (1f + currentRaiseRate);
        int energyGenerated = (int)currentEnergyRate;

        opponentScore += scoreGenerated;
        currentEnergy += energyGenerated;

        DataManager.instance.enemyScore = opponentScore;

        if(currentEnergy > opponent.spellCost)
        {
            StartCoroutine(CastSpell());
        }
    }

    bool IsStep4()
    {
        return MatchController.instance.CurrentStep == Step.ResolveChainMatches;
    }

    public IEnumerator CastSpell()
    {
        currentRaiseRate += opponent.raiseRate;

        int sumOfChances = 0;
        for (int i = 0; i < opponent.spellList.Length; i++)
        {
            sumOfChances += opponent.spellList[i].spellChance;
        }
        int currentChances = 0;
        int random = Random.Range(0, sumOfChances);
        int index = 0;
        for (int i = 0; i < opponent.spellList.Length; i++)
        {
            if(random >= currentChances && random < opponent.spellList[i].spellChance+currentChances)
            {
                index = opponent.spellList[i].spellAN;
                break;
            }
            currentChances += opponent.spellList[i].spellChance;
        }

        spellData chosenSpell = DataLoader.instance.spells[index];

        yield return new WaitUntil(IsStep4);

        Debug.LogWarning("Enemy spell incoming!");
        SpellController.instance.OpponentCastSpell(chosenSpell);
    }

    public void RemoveScore(int amount)
    {
        opponentScore -= amount;
    }

    public void RemoveEnergy(int amount)
    {
        currentEnergy -= amount;
    }
}
