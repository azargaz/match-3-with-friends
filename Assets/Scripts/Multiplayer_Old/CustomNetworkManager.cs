﻿using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager
{
    public UnetGameRoom GameRoom;

    void Awake()
    {
        if (GameRoom == null)
        {
            Debug.LogError("Game Room property is not set on NetworkManager");
            return;
        }

        // Subscribe to events
        GameRoom.PlayerJoined += OnPlayerJoined;
        GameRoom.PlayerLeft += OnPlayerLeft;
    }
    
    /// Regular Unet method, which gets called when client disconnects from game server
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);

        // Don't forget to notify the room that a player disconnected
        GameRoom.ClientDisconnected(conn);
    }

    /// Invoked, when client provides a successful access token and enters the room
    private void OnPlayerJoined(UnetMsfPlayer player)
    {
        Debug.Log("Player joined");

        //add the connection to the game server to make sure the player can update on the client and the gameserver
        NetworkServer.AddPlayerForConnection(player.Connection, null, 0);

        return;
    }


    private void OnPlayerLeft(UnetMsfPlayer player)
    {

    }
}