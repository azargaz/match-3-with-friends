﻿using Barebones.MasterServer;
using Barebones.Networking;
using UnityEngine;
using UnityEngine.UI;

public class DisplayConnectionStatus : MonoBehaviour
{
    private IClientSocket _connection;
    public Text text;

    private void Start()
    {
        text = gameObject.GetComponent<Text>();
        // Get current connecion
        _connection = GetConnection();
        // When status of conneciton changes call UpdateStatusView
        _connection.StatusChanged += UpdateStatusView;
        UpdateStatusView(_connection.Status);
    }

    protected void OnDestroy()
    {
        _connection.StatusChanged -= UpdateStatusView;
    }

    protected IClientSocket GetConnection()
    {
        return Msf.Connection;
    }

    protected void UpdateStatusView(ConnectionStatus status)
    {
        switch (status)
        {
            case ConnectionStatus.Connected:
                text.text = "Connected";
                break;
            case ConnectionStatus.Disconnected:
                text.text = "Offline";
                break;
            case ConnectionStatus.Connecting:
                text.text = "Connecting";
                break;
            default:
                text.text = "Unknown";
                break;
        }
    }
}
