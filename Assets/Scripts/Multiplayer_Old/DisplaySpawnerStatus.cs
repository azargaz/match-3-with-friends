﻿using Barebones.MasterServer;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySpawnerStatus : MonoBehaviour
{
    public Text text;

    private void Start()
    {
        text = gameObject.GetComponent<Text>();
        Msf.Server.Spawners.SpawnerRegistered += OnSpawnerRegistered;
    }

    private void OnSpawnerRegistered(SpawnerController obj)
    {
        text.text = "Started Spawner";
    }

    void OnDestroy()
    {
        Msf.Server.Spawners.SpawnerRegistered -= OnSpawnerRegistered;
    }
}
