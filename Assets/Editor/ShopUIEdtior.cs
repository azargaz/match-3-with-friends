﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Purchasing;
using UnityEditor.Purchasing;
using System.Collections.Generic;

[CustomEditor(typeof(ShopUI))]
[CanEditMultipleObjects]
public class ShopUIEdtior : Editor
{
    private List<string> m_ValidIDs = new List<string>();
    private List<SerializedProperty> m_ProductIDProperty;
    private const string kNoProduct = "<None>";
    
    public override void OnInspectorGUI()
    {
        m_ProductIDProperty = new List<SerializedProperty>();

        var iapItems = serializedObject.FindProperty("iAPItems");
        for (int i = 0; i < iapItems.arraySize; i++)
        {
            var item = iapItems.GetArrayElementAtIndex(i).FindPropertyRelative("productId");
            m_ProductIDProperty.Add(item);
        }

        for (int i = 0; i < m_ProductIDProperty.Count; i++)
        {
            var shopUI = (ShopUI)target;

            serializedObject.Update();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("IAP Item "+ i + " Product ID :", "Select a product from the IAP catalog"));            

            var catalog = ProductCatalog.LoadDefaultCatalog();

            m_ValidIDs.Clear();
            m_ValidIDs.Add(kNoProduct);
            foreach (var product in catalog.allProducts)
            {
                m_ValidIDs.Add(product.id);
            }

            int currentIndex = string.IsNullOrEmpty(shopUI.iAPItems[i].productId) ? 0 : m_ValidIDs.IndexOf(shopUI.iAPItems[i].productId);
            int newIndex = EditorGUILayout.Popup(currentIndex, m_ValidIDs.ToArray());
            if (newIndex > 0 && newIndex < m_ValidIDs.Count)
            {
                m_ProductIDProperty[i].stringValue = m_ValidIDs[newIndex];
            }
            else
            {
                m_ProductIDProperty[i].stringValue = "none";
            }

            EditorGUILayout.EndHorizontal();
            serializedObject.ApplyModifiedProperties();
        }

        if (GUILayout.Button("IAP Catalog..."))
        {
            ProductCatalogEditor.ShowWindow();
        }

        base.OnInspectorGUI();
    }
}