﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SingleplayerBattles))]
public class SingleplayerBattlesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SingleplayerBattles myScript = (SingleplayerBattles)target;
        if (GUILayout.Button("Setup battles"))
        {
            myScript.SetupBattles();
        }
    }
}