﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Instead of editing this script, I would recommend to write your own
/// (or copy and change it). Otherwise, your changes will be overwriten when you
/// update project :)
/// </summary>
public class QuickBuild
{
    /// <summary>
    /// Have in mind that if you change it, it might take "a while" 
    /// for the editor to pick up changes 
    /// </summary>
    public static string QuickSetupRoot = "Assets/Scenes";

    public static BuildTarget TargetPlatform = BuildTarget.StandaloneWindows;
    public static BuildTarget ServerTargetPlatform = BuildTarget.StandaloneLinux64;

    /// <summary>
    /// Build with "Development" flag, so that we can see the console if something 
    /// goes wrong
    /// </summary>
    public static BuildOptions BuildOptions = BuildOptions.Development;
    public static BuildOptions ServerBuildOptions = BuildOptions.EnableHeadlessMode;

    public static string PrevPath = null;

    public static BuildPlayerOptions clientBuildPlayerOptions = new BuildPlayerOptions()
    {
        options = BuildOptions,
        target = TargetPlatform
    };

    public static BuildPlayerOptions serverBuildPlayerOptions = new BuildPlayerOptions()
    {
        options = ServerBuildOptions,
        target = ServerTargetPlatform
    };
    /// <summary>
    /// Creates a build for master server and spawner
    /// </summary>
    /// <param name="path"></param>
    public static void BuildMasterAndClient(string path)
    {
        BuildMaster(path);
        BuildClient(path);
    }

    /// <summary>
    /// Creates a build for master server and spawner
    /// </summary>
    /// <param name="path"></param>
    public static void BuildMaster(string path)
    {
        var _scenes = new[]
        {
            QuickSetupRoot+ "/MainMenu.unity",
            QuickSetupRoot+ "/BattleScene.unity"
        };

        serverBuildPlayerOptions.locationPathName = path + "/match3server/match3withfriends.x86_64";
        serverBuildPlayerOptions.scenes = _scenes;

        BuildPipeline.BuildPlayer(serverBuildPlayerOptions);
    }
    
    /// <summary>
     /// Creates a build for master server and spawner
     /// </summary>
     /// <param name="path"></param>
    public static void BuildClient(string path)
    {
        var _scenes = new[]
        {
            QuickSetupRoot+ "/MainMenu.unity",
            QuickSetupRoot+ "/BattleScene.unity"
        };

        clientBuildPlayerOptions.locationPathName = path + "/WindowsClient/match3withfriends.exe";
        clientBuildPlayerOptions.scenes = _scenes;

        BuildPipeline.BuildPlayer(clientBuildPlayerOptions);
    }
    
    /// <summary>
    /// Creates a build for master server and spawner
    /// </summary>
    /// <param name="path"></param>
    public static void BuildTestMaster(string path)
    {
        var _scenes = new[]
        {
            QuickSetupRoot+ "/MainMenu.unity",
            QuickSetupRoot+ "/BattleScene.unity"
        };

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        buildPlayerOptions.options = BuildOptions.None;
        buildPlayerOptions.target = BuildTarget.StandaloneWindows;
        buildPlayerOptions.locationPathName = path + "/Test/match3withfriends.exe";
        buildPlayerOptions.scenes = _scenes;
        
        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }

    #region Editor Menu

    [MenuItem("Tools/CustomMsf/Build Server + Client", false, 11)]
    public static void BuildMasterAndClientMenu()
    {
        var path = GetPath();
        if (!string.IsNullOrEmpty(path))
        {
            BuildMasterAndClient(path);
        }
    }

    [MenuItem("Tools/CustomMsf/Build Linux Server", false, 11)]
    public static void BuildMasterMenu()
    {
        var path = GetPath();
        if (!string.IsNullOrEmpty(path))
        {
            BuildMaster(path);
        }
    }

    [MenuItem("Tools/CustomMsf/Build Windows Client", false, 11)]
    public static void BuildClientMenu()
    {
        var path = GetPath();
        if (!string.IsNullOrEmpty(path))
        {
            BuildClient(path);
        }
    }

    [MenuItem("Tools/CustomMsf/Build Test Server", false, 11)]
    public static void Test()
    {
        var path = GetPath();
        if (!string.IsNullOrEmpty(path))
        {
            BuildTestMaster(path);
        }
    }

    #endregion

    public static string GetPath()
    {
        string path = Application.dataPath.Replace("/Assets", "/Builds");
        return path;
    }
}