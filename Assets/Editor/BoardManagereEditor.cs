﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BoardManager))]
public class BoardManagereEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BoardManager myScript = (BoardManager)target;
        if(GUILayout.Button("Setup Board"))
        {
            myScript.ClearBoard();
            myScript.SetupRandomBoard();
        }
    }
}
