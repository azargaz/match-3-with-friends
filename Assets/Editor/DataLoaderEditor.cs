﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DataLoader))]
public class DataLoaderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DataLoader myScript = (DataLoader)target;
        SingleplayerBattles otherScript = FindObjectOfType<SingleplayerBattles>();
        if (GUILayout.Button("Load data"))
        {
            myScript.LoadData();
            if (otherScript != null)
                otherScript.SetupBattles();
            else
                Debug.LogError("Couldn't find SingleplayerBattles script. Maybe it's inactive in hierarchy?");
        }
        if (GUILayout.Button("Clear data"))
        {
            myScript.ClearData();
        }
    }
}
